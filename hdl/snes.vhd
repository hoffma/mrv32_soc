library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity snes is
    Generic (
        CLK_IN_HZ : natural
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;
        -- R L X A right left down up start select Y B
        btns : out std_logic_vector(11 downto 0);

        dclk : out std_logic;
        dlatch : out std_logic;
        sdat : in std_logic;

        data_valid : out std_logic
    );
end snes;

architecture behave of snes is
    constant DATA_CLK_HZ : natural := 83000;
    constant CLK_DIV_CNT : natural := (CLK_IN_HZ/DATA_CLK_HZ)/2;

    signal div_cnt : unsigned(31 downto 0) := (others => '0');
    signal data_clk_i : std_logic;

    type state_t is (ST_START, ST_LATCH1, ST_LATCH2, ST_DATA, ST_REST);
    signal curr_state : state_t;
    signal dclk_en : std_logic;
    signal btns_i, btns_o : std_logic_vector(11 downto 0);
begin
    dclk <= data_clk_i when dclk_en = '1' else '1';
    btns <= not btns_o;

    process(clk)
    begin
        if rising_edge(clk) then
            if div_cnt < (CLK_DIV_CNT-1) then
                div_cnt <= div_cnt + 1;
            else
                div_cnt <= (others => '0');
                data_clk_i <= not data_clk_i;
            end if;

            if rst = '1' then
                div_cnt <= (others => '0');
                data_clk_i <= '0';
            end if;
        end if;
    end process;

    process(data_clk_i, rst)
        variable bit_cnt : integer range 0 to 16;
    begin
        if rst = '1' then
            curr_state <= ST_LATCH1;
            data_valid <= '0';
            bit_cnt := 0;
            btns_i <= (others => '0');
            btns_o <= (others => '0');
            dclk_en <= '0';
            dlatch <= '0';
        elsif rising_edge(data_clk_i) then
            data_valid <= '0';
            dclk_en <= '0';
            dlatch <= '0';
            case curr_state is
                when ST_START =>
                    dlatch <= '1';
                    bit_cnt := 0;
                    btns_i <= (others => '0');
                    curr_state <= ST_LATCH1;
                when ST_LATCH1 =>
                    dlatch <= '1';
                    bit_cnt := 0;
                    btns_i <= (others => '0');
                    curr_state <= ST_LATCH2;
                when ST_LATCH2 =>
                    bit_cnt := 0;
                    btns_i <= (others => '0');
                    dclk_en <= '1';
                    curr_state <= ST_DATA;
                when ST_DATA =>
                    dclk_en <= '1';
                    btns_i <= sdat & btns_i(11 downto 1);
                    bit_cnt := bit_cnt + 1;

                    if bit_cnt = 12 then
                        curr_state <= ST_REST;
                    end if;
                when ST_REST =>
                    bit_cnt := bit_cnt + 1;
                    dclk_en <= '1';
                    if bit_cnt = 16 then
                        data_valid <= '1';
                        btns_o <= btns_i;
                        dclk_en <= '0';
                        curr_state <= ST_START;
                    end if;
            end case;
        end if;
    end process;
end behave;
