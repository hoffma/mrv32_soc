library ieee;
use ieee.std_logic_1164.all;

library std;
use std.textio.all;

library work;
use work.misc.all;
use work.types.all;
use work.soc_types.all;
use work.devices.all;
use work.mem_init.all;

entity soc is
    Port (
        clk : in std_logic;
        rst : in std_logic;
        led : out std_logic_vector(7 downto 0);
        uart0_rxd : in std_logic;
        uart0_txd : out std_logic;

        irq : in std_logic;

        mat_clk : in std_logic;
        panel_clk : out std_logic;
        panel_oe : out std_logic;
        panel_lat : out std_logic;
        panel_row : out std_logic_vector(4 downto 0);
        panel_rgb1 : out std_logic_vector(2 downto 0);
        panel_rgb2 : out std_logic_vector(2 downto 0);
        
        dclk : out std_logic;
        dlatch : out std_logic;
        sdat : in std_logic
    );
end soc;

architecture behave of soc is
    signal r_cpu : cpu_simple;
    signal tim_interrupt : std_logic;
    signal mtime_wen : std_logic;

    signal boot_rom_dev : dev_interface_t;
    signal boot_ram_dev : dev_interface_t;
    signal sram_dev : dev_interface_t;
    signal led_dev : dev_interface_t;
    signal uart0_dev : dev_interface_t;
    signal mtime_dev : dev_interface_t;
    signal mat_dev : dev_interface_t;
    signal controller_dev : dev_interface_t;

    signal soc_cs_lines : std_logic_vector(CSG_ENTRIES'length-1 downto 0);

    signal boot_rom_sel : std_logic;
    signal boot_ram_sel : std_logic;
    signal sram_sel : std_logic;
    signal mtime_sel : std_logic;
    signal led_sel : std_logic;
    signal uart0_sel : std_logic;
    signal mat_sel : std_logic;
    signal controller_sel : std_logic;
begin
    boot_rom_sel <= soc_cs_lines(0);
    boot_ram_sel <= soc_cs_lines(1);
    sram_sel <= soc_cs_lines(2);
    mtime_sel <= soc_cs_lines(3);
    led_sel <= soc_cs_lines(4);
    uart0_sel <= soc_cs_lines(5);
    mat_sel <= soc_cs_lines(6);
    controller_sel <= soc_cs_lines(7);

    boot_rom_dev.we <= r_cpu.wen;
    boot_rom_dev.addr <= r_cpu.addr;
    boot_rom_dev.din <= r_cpu.wdata;

    boot_ram_dev.we <= r_cpu.wen;
    boot_ram_dev.addr <= r_cpu.addr;
    boot_ram_dev.din <= r_cpu.wdata;

    sram_dev.we <= r_cpu.wen;
    sram_dev.addr <= r_cpu.addr;
    sram_dev.din <= r_cpu.wdata;

    mtime_dev.we <= r_cpu.wen;
    mtime_dev.addr <= r_cpu.addr;
    mtime_dev.din <= r_cpu.wdata;

    led_dev.we <= r_cpu.wen;
    led_dev.addr <= r_cpu.addr;
    led_dev.din <= r_cpu.wdata;

    uart0_dev.we <= r_cpu.wen;
    uart0_dev.addr <= r_cpu.addr;
    uart0_dev.din <= r_cpu.wdata;

    mat_dev.we <= r_cpu.wen;
    mat_dev.addr <= r_cpu.addr;
    mat_dev.din <= r_cpu.wdata;

    controller_dev.we <= r_cpu.wen;
    controller_dev.addr <= r_cpu.addr;
    controller_dev.din <= r_cpu.wdata;

    bus_mux_p : process(all)
    begin
        r_cpu.valid <= '0';
        boot_rom_dev.en <= '0';
        boot_ram_dev.en <= '0';
        sram_dev.en <= '0';
        mtime_dev.en <= '0';
        led_dev.en <= '0';
        uart0_dev.en <= '0';
        mat_dev.en <= '0';
        controller_dev.en <= '0';

        r_cpu.rdata <= (others => 'U');

        if ?? boot_rom_sel then
            boot_rom_dev.en <= r_cpu.strb;
            r_cpu.valid <= boot_rom_dev.ack;
            r_cpu.rdata <= boot_rom_dev.dout;
        elsif ?? boot_ram_sel then
            boot_ram_dev.en <= r_cpu.strb;
            r_cpu.valid <= boot_ram_dev.ack;
            r_cpu.rdata <= boot_ram_dev.dout;
        elsif ?? sram_sel then
            sram_dev.en <= r_cpu.strb;
            r_cpu.valid <= sram_dev.ack;
            r_cpu.rdata <= sram_dev.dout;
        elsif ?? mtime_sel then
            mtime_dev.en <= r_cpu.strb;
            r_cpu.valid <= mtime_dev.ack;
            r_cpu.rdata <= mtime_dev.dout;
        elsif ?? led_sel then
            led_dev.en <= r_cpu.strb;
            r_cpu.valid <= led_dev.ack;
            r_cpu.rdata <= led_dev.dout;
        elsif ?? uart0_sel then
            uart0_dev.en <= r_cpu.strb;
            r_cpu.valid <= uart0_dev.ack;
            r_cpu.rdata <= uart0_dev.dout;
        elsif ?? mat_sel then
            mat_dev.en <= r_cpu.strb;
            r_cpu.valid <= mat_dev.ack;
            r_cpu.rdata <= mat_dev.dout;
        elsif ?? controller_sel then
            controller_dev.en <= r_cpu.strb;
            r_cpu.valid <= controller_dev.ack;
            r_cpu.rdata <= controller_dev.dout;
        else
            r_cpu.valid <= '1'; -- dont let the CPU stall
        end if;
    end process;

    mtime_dev.ack <= mtime_sel when rising_edge(clk);

    inst_dbus_csg : entity work.csg
    generic map (
        BUS_DEVICES => CSG_ENTRIES
    ) port map (
        addr => r_cpu.addr,
        cs_signal => soc_cs_lines
    );

--    irq_p : process(clk)
--    begin
--        if rising_edge(clk) then
--            if rst = '1' then
--                prev_irq <= '0';
--                irq <= '0';
--            else
--                prev_irq <= irq_line;
--                irq <= '0';

--                if (prev_irq = '0') and (irq_line = '1') then
--                    irq <= '1';
--                end if;
--            end if;
--        end if;
--    end process;

    inst_mrv32 : entity work.mrv32
    -- inst_mrv32 : mrv32_0
    port map (
        clk => clk,
        rst => rst,
        strb => r_cpu.strb,
        addr => r_cpu.addr,
        rdata => r_cpu.rdata,
        wen => r_cpu.wen,
        wdata => r_cpu.wdata,
        valid => r_cpu.valid,

        int_tim => tim_interrupt,
        int_ext => '0'
    );

    inst_boot_rom : entity work.rom_device(behave)
    generic map (
        ROM_INIT => ROM_INIT_DATA
    ) port map (
        clk => clk,
        rst => rst,

        en => boot_rom_dev.en,
        we => "0000",
        addr => boot_rom_dev.addr,
        din => boot_rom_dev.din,
        dout => boot_rom_dev.dout,
        ack => boot_rom_dev.ack
    );

    inst_boot_ram: entity work.sram_device
    generic map (
        MEM_SIZE_BYTES => BOOT_RAM_SIZE_BYTES
    ) port map (
        clk => clk,
        rst => rst,

        en => boot_ram_dev.en,
        we => boot_ram_dev.we,
        addr => boot_ram_dev.addr,
        din => boot_ram_dev.din,
        dout => boot_ram_dev.dout,
        ack => boot_ram_dev.ack
    );

    inst_sram : entity work.sram_device
    generic map (
        MEM_SIZE_BYTES => SRAM_SIZE_BYTES
    ) port map (
        clk => clk,
        rst => rst,

        en => sram_dev.en,
        we => sram_dev.we,
        addr => sram_dev.addr,
        din => sram_dev.din,
        dout => sram_dev.dout,
        ack => sram_dev.ack
    );

    mtime_wen <= '1' when mtime_dev.we /= "0000" else '0';
    inst_mtime : entity work.mtime
    port map (
        clk => clk,
        rst => rst,
        en => mtime_dev.en,
        wen => mtime_wen,
        addr => mtime_dev.addr,
        din => mtime_dev.din,
        dout => mtime_dev.dout,
        int => tim_interrupt
    );

    inst_led : entity work.led_device
    port map (
        clk => clk,
        rst => rst,

        en => led_dev.en,
        we => led_dev.we,
        addr => led_dev.addr,
        din => led_dev.din,
        dout => led_dev.dout,
        ack => led_dev.ack,

        led_o => led
    );

    inst_uart0: entity work.uart_device
    port map (
        clk => clk,
        rst => rst,

        en => uart0_dev.en,
        we => uart0_dev.we,
        addr => uart0_dev.addr,
        din => uart0_dev.din,
        dout => uart0_dev.dout,
        ack => uart0_dev.ack,

        txd => uart0_txd,
        rxd => uart0_rxd
    );

    inst_mat: entity work.matrix_device
    port map (
        clk => clk,
        rst => rst,
        en => mat_dev.en,
        we => mat_dev.we,
        addr => mat_dev.addr,
        din => mat_dev.din,
        dout => mat_dev.dout,
        ack => mat_dev.ack,

        mat_clk => mat_clk,
        pxl_clk => panel_clk,
        oe => panel_oe,
        lat => panel_lat,
        row => panel_row,
        rgb1 => panel_rgb1,
        rgb2 => panel_rgb2
    );
    
    inst_controller: entity work.snes_device
    generic map (
        CLK_IN_HZ => 100_000_000
    ) port map (
        clk => clk,
        rst => rst,
        en => controller_dev.en,
        we => controller_dev.we,
        addr => controller_dev.addr,
        din => controller_dev.din,
        dout => controller_dev.dout,
        ack => controller_dev.ack,
        dclk => dclk,
        dlatch => dlatch,
        sdat => sdat
    );
end behave;
