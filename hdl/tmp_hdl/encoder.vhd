library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity encoder is
    Port (
        clk : in std_logic;

        rclk : in std_logic;
        di : in std_logic;
        bi : in std_logic;
        -- count between 0 <= n < ticks
        ticks : in std_logic_vector(15 downto 0);

        cnt_in : in std_logic_vector(15 downto 0);
        cnt_we : in std_logic;
        btn : out std_logic;
        cnt : out std_logic_vector(15 downto 0)
    );
end encoder;

architecture behave of encoder is
    constant MAX_TICKS : unsigned(15 downto 0) := x"0064";
    signal cnt_u : unsigned(15 downto 0);

begin
    cnt <= std_logic_vector(cnt_u);
    btn <= bi; -- WTF???


    process(rclk)
    begin
        if rising_edge(rclk) then
            if di = '1' then
                if cnt_u > 0 then
                    cnt_u <= cnt_u - 1;
                end if;
            else
                if cnt_u < unsigned(ticks)-1 then
                    cnt_u <= cnt_u + 1;
                end if;
            end if;

            if cnt_we = '1' then
                if unsigned(cnt_in) >= cnt_u then
                    cnt_u <= unsigned(ticks)-1;
                else
                    cnt_u <= unsigned(cnt_in);
                end if;
            end if;
        end if;
    end process;
end behave;
