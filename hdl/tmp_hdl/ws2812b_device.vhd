library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ws2812b_device is
    Generic (
        CLK_T_NS : natural
    );
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        wen     : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic;

        led_out : out std_logic
    );
end ws2812b_device;

architecture behave of ws2812b_device is
    signal color : std_logic_vector(23 downto 0);
    signal pxl_clk : std_logic;
    signal ws_done : std_logic;
    signal ws_dout : std_logic;
    signal pxl_addr : std_logic_vector(15 downto 0);
    signal nxt_pxl_addr : std_logic_vector(15 downto 0);

    constant LED_RAM_DEPTH : natural := 16#1000#;
    type pxl_ram_t is array(natural range<>) of std_logic_vector(23 downto 0);
    signal pxl_data : pxl_ram_t(0 to LED_RAM_DEPTH-1) := (others => (others => '0'));
    signal pxl_cnt_reg : std_logic_vector(31 downto 0) := (others => '0');
begin
    led_out <= ws_dout;

    process(clk)
        variable pxl_addr_i : integer;
        variable tmp_addr : std_logic_vector(15 downto 0);
        variable addr_i_half: std_logic_vector(15 downto 0);
    begin

        if rising_edge(clk) then
            dout <= (others => 'U');
            ack <= '0';
            addr_i_half := addr(15 downto 0);
            pxl_addr_i := to_integer(unsigned(pxl_addr(15 downto 0)));

            if en = '1' then
                tmp_addr := "00" & addr(15 downto 2);
                
                if addr_i_half = x"4000" then
                    pxl_cnt_reg <= din;
                else
                    dout <= x"00" & pxl_data(to_integer(unsigned(tmp_addr)));
                    if wen /= "0000" then
                        pxl_data(to_integer(unsigned(tmp_addr))) <= din(23 downto 0);
                    end if;
                end if;
                
                ack <= '1';
            end if;

            color <= pxl_data(pxl_addr_i);

            if rst = '1' then
                color <= (others => '0');
                ack <= '0';
                dout <= (others => 'U');
                pxl_cnt_reg <= (others => '0');
            end if;
        end if;
    end process;

    inst_ws2812b : entity work.ws2812b
    generic map (
        CLK_T_NS => CLK_T_NS
    ) port map (
        clk => clk,
        color => color,
        pixel => pxl_addr,
        pxl_clk => pxl_clk,
        nxt_pxl => nxt_pxl_addr,
        done => ws_done,
        pxl_cnt => pxl_cnt_reg,
        dout => ws_dout
    );


end behave;
