library ieee;
use ieee.std_logic_1164.all;

entity sw_device is
    Port (
        clk : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic;
        -- hardware connection
        sw_i    : in std_logic_vector(3 downto 0)
    );
end sw_device;

architecture behave of sw_device is
    signal dout_i : std_logic_vector(31 downto 0);
    signal sw_reg : std_logic_vector(3 downto 0);
    signal ack_i : std_logic;
begin
    ack <= ack_i;
    dout <= dout_i;

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                sw_reg <= (others => '0');
            else
                sw_reg <= sw_i;
                ack_i <= '0';

                if en = '1' then
                    if we /= "0000" then
                        null; /* do nothing for the moment */
                    end if;
                    dout_i <= x"0000_000" & sw_reg;
                    ack_i <= '1';
                end if;
            end if;
        end if;
    end process;
end behave;
