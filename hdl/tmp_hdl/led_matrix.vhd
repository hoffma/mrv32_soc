library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.led_data.all;

entity led_matrix is
    Port (
        clk : in STD_LOGIC;
        rst : in STD_LOGIC;
        addr : out STD_LOGIC_VECTOR (4 downto 0);
        rgb1 : out STD_LOGIC_VECTOR (2 downto 0);
        rgb2 : out STD_LOGIC_VECTOR (2 downto 0);
        lat : out STD_LOGIC;
        pxl_clk : out STD_LOGIC;
        oe : out STD_LOGIC;
        
        bus_strb : in std_logic;
        bus_wen : in std_logic_vector(1 downto 0);
        bus_addr : in std_logic_vector(31 downto 0);
        bus_din : in std_logic_vector(15 downto 0)
       );
end led_matrix;

architecture behave of led_matrix is
    constant ROWS_HALF : natural := (PANEL_HEIGHT/2);
    constant PIXEL_X : natural := (PANEL_WIDTH*PANEL_CNT);
    type state_t is (ST_START, ST_FETCH_DATA, ST_SET_RGB, ST_CLK, ST_ROW_DONE, ST_ROW_INCR);
    signal curr_state : state_t;
    
    signal col_cnt : integer;
    signal row_cnt : integer;
    
    signal mem_en : std_logic;
    signal mem_addr : std_logic_vector(31 downto 0) := (others => '0');
    signal mem_dout : std_logic_vector(15 downto 0) := (others => '0');
    
    signal rgb1_data, rgb2_data : std_logic_vector(2 downto 0);
begin
    addr <= std_logic_vector(to_unsigned(row_cnt, addr'length));

    
    rgb1_data <= mem_dout(2 downto 0);
    rgb2_data <= mem_dout(10 downto 8); 
                       
    addr_comp : process(all)
        variable tmp_x : integer;
        variable tmp_y : integer;
    begin
        tmp_x := col_cnt;
        tmp_y := row_cnt;
        
        mem_addr <= std_logic_vector(to_unsigned(tmp_y * (PIXEL_X) + tmp_x, mem_addr'length));
    end process;
    
    process(clk)
        variable rgb1_addr : integer;
        variable rgb2_addr : integer;
    begin
        if rising_edge(clk) then
            lat <= '0';
            oe <= '1';
            pxl_clk <= '0';
            mem_en <= '1';
            
            case curr_state is
                when ST_START =>
                    col_cnt <= PIXEL_X-1;
                    row_cnt <= 0;
                    curr_state <= ST_FETCH_DATA;
                when ST_FETCH_DATA =>
                    curr_state <= ST_SET_RGB;
                when ST_SET_RGB =>
                    rgb1 <= rgb1_data;
                    rgb2 <= rgb2_data;
                    curr_state <= ST_CLK;
                when ST_CLK =>
                    pxl_clk <= '1';
                    
                    if col_cnt > 0 then
                        col_cnt <= col_cnt - 1;
                        curr_state <= ST_FETCH_DATA;              
                    else
                        lat <= '1';
                        oe <= '0';
                        curr_state <= ST_ROW_DONE;
                    end if;
                when ST_ROW_DONE =>
                    -- lat <= '1';
                    -- oe <= '0';
                    curr_state <= ST_ROW_INCR;
                when ST_ROW_INCR =>
                    col_cnt <= PIXEL_X-1;
                    if row_cnt < (ROWS_HALF-1) then
                        row_cnt <= row_cnt + 1;
                    else
                        row_cnt <= 0;
                        col_cnt <= PIXEL_X-1;
                    end if;
                    curr_state <= ST_FETCH_DATA;
            end case;
        
            if rst = '1' then
                mem_en <= '0';
                rgb1 <= (others => '0');
                rgb2 <= (others => '0');
                col_cnt <= PIXEL_X-1;
                row_cnt <= 0;
                lat <= '0';
                oe <= '1';
                pxl_clk <= '0';
                curr_state <= ST_START;
            end if;
        end if;
    end process;
    
    inst_ledram : entity work.led_mem
    generic map (
        MEM_SIZE_BYTES => 16#10000#
    ) port map (
        clka => clk,
        ena => bus_strb,
        wena => bus_wen,
        addra => bus_addr,
        dina => bus_din,
        
        clkb => clk,
        enb => mem_en,
        addrb => mem_addr,
        doutb => mem_dout
    );
end behave;
