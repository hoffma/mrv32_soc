library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ws2812b is
    Generic (
        -- STRIP_LEN : natural;
        CLK_T_NS : natural
    );
    Port (
        clk : in std_logic;
        color : in std_logic_vector(23 downto 0);
        pixel : out std_logic_vector(15 downto 0);
        pxl_clk : out std_logic;
        nxt_pxl : out std_logic_vector(15 downto 0);
        done : out std_logic;
        pxl_cnt : in std_logic_vector(31 downto 0);
        -- physical output pin to the strip
        dout  : out std_logic
    );
end ws2812b;

architecture behave of ws2812b is
    -- NOTE: there are new timings: 
    -- https://tomverbeure.github.io/2019/10/13/WS2812B_Reset_Old_and_New.html
    constant BPP : natural := 24; -- bits per pixel
    constant CYCLE_DURATION_NS : natural := 1100;
    constant BIT_CYCLES : natural := CYCLE_DURATION_NS/CLK_T_NS;
    constant RESET_CYCLES : natural := 300000/CLK_T_NS; -- 300us reset time
    constant H0_CYCLES : natural := 400/CLK_T_NS; -- 400 ns 0 HIGH
    constant H1_CYCLES : natural := 850/CLK_T_NS; -- 850 ns 1 HIGH

    type state_t is (ST_RESET, ST_TRANSMIT, ST_POST, ST_PRE_POST, ST_NEXT_PXL, ST_LOAD_CLR);
    signal curr_state : state_t := ST_RESET;

    signal cycle_count : unsigned(31 downto 0) := (others => '0'); -- have counter big enough
    signal bit_count : unsigned(6 downto 0) := (others => '0');
    signal led_count : unsigned(15 downto 0) := (others => '0');
    signal curr_data : std_logic_vector(23 downto 0);
    signal curr_bit : std_Logic;
    signal o_dout : std_logic;
    signal load_clr_o : std_logic;

    signal STRIP_LEN : natural;
begin
    STRIP_LEN <= to_integer(unsigned(pxl_cnt));
    dout <= o_dout;
    pixel <= std_logic_vector(led_count);
    nxt_pxl <= std_logic_vector(led_count + 1) when (led_count < (STRIP_LEN-1)) else
                  (others => '0');

    process(clk)
    begin
        if rising_edge(clk) then
            cycle_count <= cycle_count + 1;
            done <= '0';
            pxl_clk <= '0';
            
            case curr_state is
                when ST_RESET =>
                    o_dout <= '0';
                    led_count <= (others => '0');

                    if cycle_count >= RESET_CYCLES then
                        cycle_count <= (others => '0');
                        bit_count <= (others => '0');
                        curr_data <= (others => '0');
                        curr_bit <= '1';
                        curr_state <= ST_PRE_POST;
                        load_clr_o <= '1';
                        -- curr_state <= ST_POST;
                    end if;
                -- when ST_RESET

                when ST_TRANSMIT =>
                    o_dout <= '0';
                    if curr_bit = '1' and cycle_count <= H1_CYCLES then
                        o_dout <= '1';
                    elsif curr_bit = '0' and cycle_count <= H0_CYCLES then
                        o_dout <= '1';
                    elsif cycle_count >= BIT_CYCLES then
                        curr_state <= ST_POST;
                        cycle_count <= (others => '0');
                    end if;
                -- when ST_TRANSMIT

                when ST_PRE_POST =>
                        curr_data <= color;
                        curr_state <= ST_POST;

                when ST_POST =>
                    -- I don't know what the fuck is going on here and why I
                    -- had to put bit_count >= 23 on the first and > 23 on the
                    -- LED before reset. If I did both the same it had bugs on
                    -- the last or first LED of the strip
                    if bit_count >= 23 and led_count < (STRIP_LEN-1) then
                        bit_count <= (others => '0');
                        curr_state <= ST_NEXT_PXL;
                        led_count <= led_count + 1;
                        pxl_clk <= '1';
                        o_dout <= '1';
                    elsif bit_count > 23 and led_count >= (STRIP_LEN-1) then
                        curr_state <= ST_RESET;
                        curr_bit <= '0';
                        done <= '1';
                        cycle_count <= (others => '0');
                    else
                        curr_state <= ST_TRANSMIT;
                        curr_bit <= curr_data(to_integer(23 - bit_count));
                        bit_count <= bit_count + 1;
                        o_dout <= '1';
                    end if;
                -- when ST_POST

                when ST_NEXT_PXL =>
                    curr_state <= ST_LOAD_CLR;

                when ST_LOAD_CLR =>
                    curr_data <= color;
                    curr_state <= ST_TRANSMIT;
                -- when ST_LOAD_CLR

                when others =>
                    curr_state <= ST_RESET;
                    cycle_count <= (others => '0');
                    bit_count <= (others => '0');
                -- when others
            end case;
        end if;
    end process;
end behave;


