library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.misc.all;

entity led_mem is
    Generic (
        MEM_SIZE_BYTES : natural
    );   
    Port (
        clka : in STD_LOGIC;
        ena : in STD_LOGIC;
        wena : in STD_LOGIC_VECTOR (1 downto 0);
        addra : in STD_LOGIC_VECTOR (31 downto 0);
        dina : in STD_LOGIC_VECTOR (15 downto 0);
        
        clkb : in STD_LOGIC;
        enb : in STD_LOGIC;
        addrb : in STD_LOGIC_VECTOR (31 downto 0);
        doutb : out STD_LOGIC_VECTOR (15 downto 0)
    );
end led_mem;

architecture Behavioral of led_mem is
    constant BYTES_PER_WORD : natural := 2;
    constant MEM_SIZE_WORDS : natural := (MEM_SIZE_BYTES/BYTES_PER_WORD);
    signal addra_i : integer;
    signal addrb_i : integer;
    
    shared variable led_ram : mem16_t (0 to (MEM_SIZE_BYTES/BYTES_PER_WORD)-1) := (others => (others => '0'));
begin
    addra_i <= to_integer(unsigned(addra));
    addrb_i <= to_integer(unsigned(addrb));
    
    a_port : process(clka)
    begin
        if rising_edge(clka) then
            if ena = '1' then
                if wena(0) = '1' then
                    led_ram(addra_i)(7 downto 0) := dina(7 downto 0);
                end if;
                if wena(1) = '1' then
                    led_ram(addra_i)(15 downto 8) := dina(15 downto 8);
                end if;
            end if;
        end if;
    end process;
    
    b_port : process(clkb)
    begin
        if rising_edge(clkb) then
            doutb <= (others => 'U');
            if enb = '1' then
                doutb <= led_ram(addrb_i);
            end if;
        end if;
    end process;
end Behavioral;
