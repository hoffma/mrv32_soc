library ieee;
use ieee.std_logic_1164.all;

entity encoder_device is
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic;
        -- hardware connection
        rclk_i  : in std_logic;
        di_i    : in std_logic;
        btn_i   : in std_logic;
        enc_o   : out std_logic_vector(15 downto 0)
    );
end encoder_device;

architecture behave of encoder_device is
    signal encoder_out : std_logic_vector(15 downto 0) := (others => '0');
    signal encoder_in : std_logic_vector(15 downto 0);
    signal encoder_we : std_logic;
    signal cnt_limit : std_logic_vector(15 downto 0) := x"0064";
    signal btn_val : std_logic;
    signal rst_val : std_logic;

    signal dout_i : std_logic_vector(31 downto 0);
    signal ack_i : std_logic;

    signal rclk_db, di_db, btn_db : std_logic; -- debounced signals
begin
    dout <= dout_i;
    ack <= ack_i;

    process(clk)
        variable nibble : std_logic_vector(7 downto 0);
    begin
        if rising_edge(clk) then
            if rst = '1' then
                encoder_in <= (others => '0');
                encoder_we <= '0';
                cnt_limit <= (others => '0');
                btn_val <= '0';
            else
                dout_i <= (others => 'U');
                ack_i <= '0';
                rst_val <= '0';
                nibble := addr(7 downto 0);
                encoder_in <= (others => '0');
                encoder_we <= '0';

                if en = '1' then
                    dout_i <= (others => '0');
                    case nibble is
                        when x"00" =>
                            if we(0) = '1' then
                                encoder_in(7 downto 0) <= din(7 downto 0);
                                encoder_we <= '1';
                            end if;
                            if we(1) = '1' then
                                encoder_in(15 downto 8) <= din(15 downto 8);
                                encoder_we <= '1';
                            end if;
                            dout_i(15 downto 0) <= encoder_out;
                        when x"04" =>
                            if we(0) = '1' then
                                cnt_limit(7 downto 0) <= din(7 downto 0);
                            end if;
                            if we(1) = '1' then
                                cnt_limit(15 downto 8) <= din(15 downto 8);
                            end if;
                            dout_i(15 downto 0) <= cnt_limit;
                        when others =>
                            dout_i <= (others => '0');
                    end case;

                    ack_i <= '1';
                end if;
            end if;
        end if;
    end process;

    -- TODO: Test this. Should be debounced now
    inst_rclk_debounce : entity work.debounce
    generic map (
        CLK_T_NS => 10,
        DEBOUNCE_T_NS => 10000
    ) port map (
        clk => clk,
        rst => rst,
        din => rclk_i,
        dout => rclk_db
    );

    inst_di_debounce : entity work.debounce
    generic map (
        CLK_T_NS => 10,
        DEBOUNCE_T_NS => 10000
    ) port map (
        clk => clk,
        rst => rst,
        din => di_i,
        dout => di_db
    );

    inst_btn_debounce : entity work.debounce
    generic map (
        CLK_T_NS => 10,
        DEBOUNCE_T_NS => 10000
    ) port map (
        clk => clk,
        rst => rst,
        din => btn_i,
        dout => btn_db
    );

    inst_enc : entity work.encoder
    port map (
        clk => clk,
        rclk => rclk_db,
        di => di_db,
        bi => btn_db,
        ticks => cnt_limit,
        cnt_in => encoder_in,
        cnt_we => encoder_we,
        btn => btn_val,
        cnt => encoder_out
    );
    enc_o <= encoder_out;
end behave;
