library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.misc.all;

entity matrix_device is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           we : in STD_LOGIC_VECTOR (3 downto 0);
           addr : in STD_LOGIC_VECTOR (31 downto 0);
           din : in STD_LOGIC_VECTOR (31 downto 0);
           dout : out STD_LOGIC_VECTOR (31 downto 0);
           ack : out STD_LOGIC;
           -- hardware signals
           mat_clk : in STD_LOGIC;
           pxl_clk : out STD_LOGIC;
           oe : out STD_LOGIC;
           lat : out STD_LOGIC;
           row : out STD_LOGIC_VECTOR (4 downto 0);
           rgb1 : out STD_LOGIC_VECTOR (2 downto 0);
           rgb2 : out STD_LOGIC_VECTOR (2 downto 0));
end matrix_device;

architecture Behavioral of matrix_device is
    -- type mem16_t is array(natural range<>) of std_logic_vector(15 downto 0);
    -- shared variable mem_data : mem16_t(0 to 4095) := (others => (others => '0'));
    constant MEM_DEPTH : natural := 16#8000#;
    signal mem_data : mem16_t(0 to (MEM_DEPTH-1)) := (others => (others => '0'));
    
    signal addr_i : std_logic_vector(31 downto 0);
    signal mat_mem_strb : std_logic;
    signal mat_mem_ack : std_logic;
    signal mat_mem_dout : std_logic_vector(15 downto 0);
    signal mat_mem_addr : std_logic_vector(31 downto 0);
    
   
begin

    addr_i <= x"00" & "00" & addr(23 downto 2); -- cut upper 8 bits and shift 2 right
    
    process(clk)
        variable addr_v : integer;
    begin
        if rising_edge(clk) then
            ack <= '0';
            addr_v := to_integer(unsigned(addr_i));
            if en = '1' then
                ack <= '1';
                if we(0) = '1' then
                    mem_data(addr_v)(7 downto 0) <= din(7 downto 0);
                end if;
                if we(1) = '1' then
                    mem_data(addr_v)(15 downto 8) <= din(15 downto 8);
                end if;
                dout <= x"0000" & mem_data(addr_v);
            end if;
        end if;
    end process;
    
    -- TODO: check if address is in bounds
    process(mat_clk)
        variable addr_v : integer;
    begin
        if rising_edge(mat_clk) then
            addr_v := to_integer(unsigned(mat_mem_addr));
            mat_mem_ack <= '0';
            mat_mem_dout <= (others => '0');
            if mat_mem_strb = '1' then
                mat_mem_ack <= mat_mem_strb;
                mat_mem_dout <= mem_data(addr_v);
            end if;
        end if;
    end process;
    
    inst_mat: entity work.matrix
    port map (
        clk => mat_clk,
        rst => rst,
        pxl_clk => pxl_clk,
        oe => oe,
        lat => lat,
        row => row,
        rgb1 => rgb1,
        rgb2 => rgb2,
        
        mem_strb => mat_mem_strb,
        mem_addr => mat_mem_addr,
        mem_dout => mat_mem_dout,
        mem_ack => mat_mem_ack
    );
end Behavioral;
