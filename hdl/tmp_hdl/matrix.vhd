library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity matrix is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           pxl_clk : out STD_LOGIC;
           oe : out STD_LOGIC;
           lat : out STD_LOGIC;
           row : out STD_LOGIC_VECTOR (4 downto 0);
           rgb1 : out STD_LOGIC_VECTOR (2 downto 0);
           rgb2 : out STD_LOGIC_VECTOR (2 downto 0);
           
           mem_strb : out std_logic;
           mem_addr : out std_logic_vector(31 downto 0);
           mem_dout : in std_logic_vector(15 downto 0);
           mem_ack  : in std_logic);
end matrix;

architecture Behavioral of matrix is
    constant MAT_WIDTH : natural := 64;
    constant MAT_HEIGHT : natural := 32;
    constant MAT_CNT : natural := 12;
    constant ROWS_HALF : natural := (MAT_HEIGHT/2);
    constant PIXEL_X : natural := (MAT_WIDTH*MAT_CNT);
    constant PIXEL_CNT_MAX : natural := (MAT_WIDTH*MAT_HEIGHT*MAT_CNT);
    
    type state_t is (ST_START, ST_FETCH, ST_SET, ST_CLK, ST_NEXT, ST_ROW_DONE);
    signal curr_state : state_t;
    
    signal col_cnt : integer;
    signal row_cnt : integer;
begin
    process(clk)
        variable tmp_x : integer;
        variable tmp_y : integer;
        variable tmp_addr : unsigned(31 downto 0);
    begin
        if rising_edge(clk) then
            mem_addr <= (others => '0');
            mem_strb <= '0';
            pxl_clk <= '0';
            lat <= '0';
            oe <= '1';
            
            tmp_x := col_cnt;
            tmp_y := row_cnt;
            tmp_addr := to_unsigned(tmp_y * PIXEL_X + tmp_x, tmp_addr'length);
            
            case curr_state is
                when ST_START =>
                    mem_strb <= '1';
                    mem_addr <= std_logic_vector(tmp_addr);
                    row_cnt <= 0;
                    row <= (others => '0');
                    col_cnt <= PIXEL_X-1;
                    curr_state <= ST_FETCH;
                when ST_FETCH =>
                    curr_state <= ST_SET;
                when ST_SET =>
                    rgb1 <= mem_dout(2 downto 0);
                    rgb2 <= mem_dout(10 downto 8);
                    pxl_clk <= '1';
                    curr_state <= ST_CLK;
                when ST_CLK =>
                    if col_cnt = 0 then
                        curr_state <= ST_ROW_DONE;
                        row <= std_logic_vector(to_unsigned(row_cnt, row'length));
                        lat <= '1';
                        oe <= '0';
                    else
                        col_cnt <= col_cnt - 1;
                        curr_state <= ST_NEXT;
                    end if;
                when ST_NEXT =>
                    mem_strb <= '1';
                    mem_addr <= std_logic_vector(tmp_addr);
                    curr_state <= ST_FETCH;
                when ST_ROW_DONE =>
                    row_cnt <= row_cnt + 1;
                    col_cnt <= PIXEL_X-1;
                    if row_cnt >= (ROWS_HALF-1) then
                        row_cnt <= 0;
                    end if;
                    mem_strb <= '1';
                    mem_addr <= std_logic_vector(tmp_addr);
                    curr_state <= ST_NEXT;
            end case;
            
            if rst = '1' then
                curr_state <= ST_START;
                row_cnt <= 0;
                col_cnt <= PIXEL_X-1;
                lat <= '0';
                oe <= '0';
                pxl_clk <= '0';
                mem_strb <= '0';
                mem_addr <= (others => '0');
            end if;
        end if;
    end process;
end Behavioral;
