library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce is
    Generic (
        CLK_T_NS : natural := 10;
        DEBOUNCE_T_NS : natural := 10000
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;
        din : in std_logic;
        dout : out std_logic
    );
end debounce;

architecture behave of debounce is
    constant DEBOUNCE_MAX_CNT : natural := (DEBOUNCE_T_NS / CLK_T_NS / 2);

    signal debounce_cnt : unsigned(31 downto 0);

    type stage_reg_t is array(0 to 4) of std_logic;

    signal stages : stage_reg_t;
begin
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                debounce_cnt <= (others => '0');
                stages <= (others => '0');
            else
                if debounce_cnt < (DEBOUNCE_MAX_CNT-1) then
                    debounce_cnt <= debounce_cnt + 1;
                else
                    debounce_cnt <= (others => '0');
                    for I in 1 to 4 loop
                        stages(I) <= stages(I-1);
                    end loop;
                    stages(0) <= din;
                end if;
            end if;
        end if;
    end process;

    gen_out : process(all)
        variable tmp : std_logic;
    begin
        tmp := '1';
        for I in 0 to 4 loop
            tmp := tmp and stages(I);
        end loop;

        dout <= tmp;
    end process;
end behave;
