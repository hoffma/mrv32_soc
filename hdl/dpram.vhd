library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.misc.all;

entity dpram is
    Generic (
        MEM_SIZE_BYTES : natural; -- Byte
        MEM_FILE : string
        -- MEM_INIT : mem32_t
    );
    Port (
        clk : in std_logic;
        
        en_a : in std_logic;
        addr_a : in std_logic_vector(31 downto 0);
        dout_a : out std_logic_vector(31 downto 0);
        ack_a : out std_logic;

        en_b : in std_logic;
        wren_b : in std_logic_vector(3 downto 0);
        addr_b : in std_logic_vector(31 downto 0);
        din_b : in std_logic_vector(31 downto 0);
        dout_b : out std_logic_vector(31 downto 0);
        ack_b : out std_logic
    );
end dpram;

architecture Behavioral of dpram is    
    signal addr_a_i : integer;
    signal addr_b_i : integer;

    constant MEM_SIZE_WORDS : natural := (MEM_SIZE_BYTES/4);

    impure function mem32_init_f(f_name: string; mem_size: natural) return mem32_t is
        file text_f : text open read_mode is f_name;
        variable text_line_v : line;
        variable mem_v : mem32_t(0 to (MEM_SIZE_BYTES/4)-1);
        variable idx_v : natural;
        variable word_v : bit_vector(31 downto 0);
    begin
        mem_v := (others => (others => '0'));
        idx_v := 0;

        while (endfile(text_f) = false) and (idx_v < mem_size) loop
            readline(text_f, text_line_v);
            hread(text_line_v, word_v);
            mem_v(idx_v) := to_stdlogicvector(word_v);
            idx_v := idx_v + 1;
        end loop;

        return mem_v;
    end function mem32_init_f;

    signal mem_ram : mem32_t(0 to (MEM_SIZE_WORDS-1)) := mem32_init_f(MEM_FILE, MEM_SIZE_BYTES);
    
begin

    -- cut address to actual width and word align
    addr_a_i <= to_integer(unsigned(addr_a(log2_ceil_f((MEM_SIZE_BYTES/4))+1 downto 2)));

    a_port : process(clk)
    begin
        if rising_edge(clk) then
            ack_a <= '0';
            if en_a = '1' then
                  dout_a <= mem_ram(addr_a_i);
                  ack_a <= '1';
            end if;
        end if;
    end process;

    addr_b_i <= to_integer(unsigned(addr_b(log2_ceil_f((MEM_SIZE_BYTES/4))+1 downto 2)));

    b_port : process(clk)
    begin
        if rising_edge(clk) then
            ack_b <= '0';
            if en_b = '1' then
                if wren_b(0) = '1' then
                    mem_ram(addr_b_i)(7 downto 0) <= din_b(7 downto 0);
                end if;
                if wren_b(1) = '1' then
                    mem_ram(addr_b_i)(15 downto 8) <= din_b(15 downto 8);
                end if;
                if wren_b(2) = '1' then
                    mem_ram(addr_b_i)(23 downto 16) <= din_b(23 downto 16);
                end if;
                if wren_b(3) = '1' then
                    mem_ram(addr_b_i)(31 downto 24) <= din_b(31 downto 24);
                end if;

                dout_b <= mem_ram(addr_b_i);
                ack_b <= '1';
            end if;
        end if;
    end process;
end Behavioral;
