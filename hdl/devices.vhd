library ieee;
use ieee.std_logic_1164.all;

library work;
use work.soc_types.all;

package DEVICES is

    constant CSG_MASK_256 : mask_t := x"ffff_ff";
    constant CSG_MASK_512 : mask_t := x"ffff_fe";
    constant CSG_MASK_16K : mask_t := x"ffff_c0";
    constant CSG_MASK_32K : mask_t := x"ffff_80";
    constant CSG_MASK_64K : mask_t := x"ffff_00";
    constant CSG_MASK_128K : mask_t := x"fffe_00";

    -- dummy entry
    constant CSG_DEFAULT_OFF : device_t := ('0', x"0000_00", CSG_MASK_256);
    -- actual available devices
    constant CSG_BOOT_ROM_DEV : device_t := ('1', x"8000_00", CSG_MASK_16K);
    constant CSG_BOOT_RAM_DEV : device_t := ('1', x"8000_40", CSG_MASK_16K);
    constant CSG_SRAM_DEV : device_t := ('1', x"8100_00", CSG_MASK_64K);

    constant CSG_MTIME : device_t := ('1', x"ffff_f0", CSG_MASK_256);

    constant CSG_LED_DEV : device_t := ('1', x"0100_00", CSG_MASK_256);
    constant CSG_UART0_DEV : device_t := ('1', x"0110_00", CSG_MASK_256);
    constant CSG_MAT_DEV : device_t := ('1', x"0120_00", CSG_MASK_64K);
    constant CSG_CONTROLLER_DEV : device_t := ('1', x"0111_00", CSG_MASK_256);


    constant CSG_ENTRIES : csg_devices := (
        CSG_BOOT_ROM_DEV,
        CSG_BOOT_RAM_DEV,
        CSG_SRAM_DEV,
        CSG_MTIME,
        CSG_LED_DEV,
        CSG_UART0_DEV,
        CSG_MAT_DEV,
        CSG_CONTROLLER_DEV,
        -- maybe I need this for wrong address access
        CSG_DEFAULT_OFF
    );
end package DEVICES;

package body DEVICES is
end package body DEVICES;
