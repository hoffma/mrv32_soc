library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package led_data is
    constant COLOR_BIT : natural := 8;
    constant PANEL_WIDTH : natural := 64;
    constant PANEL_HEIGHT : natural := 64;
    constant PANEL_CNT : natural := 1;
end package led_data;

package body led_data is
end package body led_data;