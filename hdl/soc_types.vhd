library ieee;
use ieee.std_logic_1164.all;

package SOC_TYPES is
    type dev_interface_t is record
        en      : std_logic;
        we      : std_logic_vector(3 downto 0);
        addr    : std_logic_vector(31 downto 0);
        din     : std_logic_vector(31 downto 0);
        dout    : std_logic_vector(31 downto 0);
        ack     : std_logic;
    end record dev_interface_t;

    subtype mask_t is std_logic_vector(23 downto 0);

    type device_t is record
        enable_cs : std_logic;
        base_addr : std_logic_vector(23 downto 0);
        addr_mask : mask_t;
    end record device_t;

    type csg_devices is array(natural range<>) of device_t;
end package SOC_TYPES;

package body SOC_TYPES is
end package body SOC_TYPES;
