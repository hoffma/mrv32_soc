library ieee;
use ieee.std_logic_1164.all;

entity top is
    Port (
        clk_25mhz : in std_logic;
        btn : in std_logic_vector(3 downto 0);
        led : out std_logic_vector(7 downto 0);
        ftdi_rxd : out std_logic;
        ftdi_txd : in std_logic;

        wifi_gpio0 : out std_logic
    );
end entity top;

architecture behave of top is
    component ecp5pll is
        generic
        (
        in_hz        : natural;
        out0_hz      : natural;
        out0_deg     : natural; -- keep 0
        out0_tol_hz  : natural -- Hz tolerance
        );
        port
        (
        clk_i        : in  std_logic;
        clk_o        : out std_logic_vector(3 downto 0);
        reset        : in  std_logic := '0';
        locked       : out std_logic
        );
    end component ecp5pll;
    signal rst : std_logic;

    signal clocks : std_logic_vector(3 downto 0);
    signal locked : std_logic;
begin
    wifi_gpio0 <= '1';


    process(clk_25mhz)
    begin
        if rising_edge(clk_25mhz) then
            rst <= not btn(0);
        end if;
    end process;

    inst_soc : entity work.soc(behave)
    port map (
        clk => clocks(0),
        rst => rst,
        led => led,
        uart0_rxd => ftdi_txd,
        uart0_txd => ftdi_rxd,

        irq => '0'
    );

    inst_pll : ecp5pll
    generic map (
        in_hz => 25000000,
        out0_hz => 50000000,
        out0_deg => 0,
        out0_tol_hz => 0
    ) port map (
        clk_i => clk_25mhz,
        clk_o => clocks,
        reset => rst,
        locked => locked
    );
end behave;
