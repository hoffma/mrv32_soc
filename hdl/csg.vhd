library ieee;
use ieee.std_logic_1164.all;

library work;
use work.devices.all;
use work.soc_types.all;

entity csg is
    Generic (
        BUS_DEVICES : csg_devices
    );
    Port (
        addr : in std_logic_vector(31 downto 0);
        cs_signal : out std_logic_vector(BUS_DEVICES'length-1 downto 0)
    );
end csg;

architecture behave of csg is
begin

    process(all)
        variable tmp_bit : std_logic;
        variable tmp_dev : device_t;
        variable addr_in : mask_t;
        variable raw_cs : std_logic_vector(BUS_DEVICES'length-1 downto 0);
    begin
        cs_signal <= (others => '0');
        
        for i in 0 to BUS_DEVICES'length-1 loop
            tmp_bit := '1';
            tmp_dev := BUS_DEVICES(i);

            addr_in := (addr(31 downto 8) and tmp_dev.addr_mask);
            for j in 0 to tmp_dev.addr_mask'length-1 loop
                tmp_bit := tmp_bit and (tmp_dev.base_addr(j) xnor addr_in(j));
            end loop;
            raw_cs(i) := tmp_bit and tmp_dev.enable_cs;
        end loop;

        cs_signal <= raw_cs;
    end process;
end behave;
