library ieee;
use ieee.std_logic_1164.all;

package MISC is
    constant ROM_SIZE_BYTES : natural :=  16#4000#;
    constant BOOT_RAM_SIZE_BYTES : natural :=  16#4000#;
    constant SRAM_SIZE_BYTES : natural :=  16#10000#;
    type mem32_t is array(natural range<>) of std_logic_vector(31 downto 0);
    type mem16_t is array(natural range<>) of std_logic_vector(15 downto 0);
    type mem8_t is array(natural range<>) of std_logic_vector(7 downto 0);

    function log2_ceil_f(val : natural) return natural;
end package MISC;

package body MISC is
    function log2_ceil_f(val : natural) return natural is
        variable i : natural;
        variable tmp : natural;
    begin
        i := 0;
        tmp := 1;
        while tmp < val loop
            tmp := tmp * 2;
            i := i + 1;
        end loop;

        return i;
    end function log2_ceil_f;
end package body MISC;
