library ieee;
use ieee.std_logic_1164.all;

entity led_device is
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic;
        -- hardware connection
        led_o   : out std_logic_vector(7 downto 0)
    );
end led_device;

architecture behave of led_device is
    signal led_reg : std_logic_vector(7 downto 0);
    signal dout_i : std_logic_vector(31 downto 0);
    signal ack_i : std_logic;
begin
    led_o <= led_reg;

    dout <= dout_i;
    ack <= ack_i;

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                led_reg <= (others => '0');
            else
                dout_i <= (others => 'U');
                ack_i <= '0';

                if en = '1' then
                    if we(0) = '1' then
                        led_reg <= din(7 downto 0);
                    end if;
                    dout_i <= x"0000_00" & led_reg;
                    ack_i <= '1';
                end if;
            end if;
        end if;
    end process;
end behave;
