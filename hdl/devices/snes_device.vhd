library ieee;
use ieee.std_logic_1164.all;

entity snes_device is
    Generic (
        CLK_IN_HZ : natural
    );
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic;
        -- hardware connection
        dclk : out std_logic;
        dlatch : out std_logic;
        sdat : in std_logic
    );
end snes_device;

architecture behave of snes_device is
    signal dout_i : std_logic_vector(31 downto 0);
    signal ack_i : std_logic;

    signal btns : std_logic_vector(11 downto 0);
    signal snes_valid : std_logic;
begin

    dout <= dout_i;
    ack <= ack_i;

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                dout_i <= (others => '0');
            else
                dout_i <= (others => 'U');
                ack_i <= '0';

                if en = '1' then
                    -- no reads
                    ack_i <= '1';
                    dout_i(31 downto 12) <= (others => '0');
                    dout_i(11 downto 0) <= btns;
                end if;
            end if;
        end if;
    end process;
    inst_snes: entity work.snes
    generic map (
        CLK_IN_HZ => CLK_IN_HZ
    ) port map (
        clk => clk,
        rst => rst,
        btns => btns,
        dclk => dclk,
        dlatch => dlatch,
        sdat => sdat,
        data_valid => snes_valid
    );
end behave;
