library ieee;
use ieee.std_logic_1164.all;

entity uart_device is
  Port (
    clk   : in std_logic;
    rst   : in std_logic;

    en    : in std_logic;
    we    : in std_logic_vector(3 downto 0);
    addr  : in std_logic_vector(31 downto 0);
    din   : in std_logic_vector(31 downto 0);
    dout  : out std_logic_vector(31 downto 0);
    ack   : out std_logic;
    -- hardware output pins
    txd   : out std_logic;
    rxd   : in std_logic
  );
end uart_device;

architecture behave of uart_device is
  constant BASE_NIBBLE_TX : std_logic_vector(7 downto 0) := x"00";
  constant BASE_NIBBLE_RX : std_logic_vector(7 downto 0) := x"04";
  constant BASE_NIBBLE_BRD : std_logic_vector(7 downto 0) := x"08";
  constant BASE_NIBBLE_RX_NB : std_logic_vector(7 downto 0) := x"0C";
  constant BASE_NIBBLE_RX_RDY : std_logic_vector(7 downto 0) := x"10";

  signal dout_i : std_logic_vector(31 downto 0);
  signal ack_i : std_logic;
  signal uart_brd : std_logic_vector(31 downto 0);
  
  type tx_state_t is (ST_WAIT, ST_SEND, ST_END);
  signal tx_state : tx_state_t;
  
  -- rx
  signal rx_rdy : std_logic;
  signal rx_reg : std_logic_vector(7 downto 0);
  signal rx_dout : std_logic_vector(7 downto 0);
  signal rx_valid : std_logic;
  -- tx
  signal tx_busy : std_logic;
  signal tx_we : std_logic;
  signal tx_ack : std_logic;
  signal tx_din : std_logic_vector(7 downto 0);
begin
  dout <= dout_i;
  ack <= ack_i;

  tx_din <= din(7 downto 0);
  

  process(clk)
    variable addr_i_nibble : std_logic_vector(7 downto 0);
  begin
    
    if rising_edge(clk) then
        if rst = '1' then
            tx_we <= '0';
            rx_valid <= '0';
            rx_reg <= (others => '0');
            tx_state <= ST_WAIT;
         else
            dout_i <= (others => 'U');
            ack_i <= '0';
            tx_we <= '0';
            addr_i_nibble := addr(7 downto 0);
            
            -- save the received data in a temporary register
            -- this gets overwritten (for now) if the register gets not read in time
            -- maybe some sort of FIFO-Buffer would be nice here
            if rx_rdy = '1' then
                rx_valid <= '1';
                rx_reg <= rx_dout;
            end if;
            
            if en = '1' then
                case addr_i_nibble is
                    when BASE_NIBBLE_TX =>
                        if tx_state = ST_WAIT then
                            if we /= "0000" then
                                tx_we <= '1';
                                tx_state <= ST_SEND;
                            end if;
                        elsif tx_state = ST_SEND then
                            if tx_ack = '1' then
                                tx_state <= ST_END;
                                ack_i <= '1';
                            end if;
                        elsif tx_state = ST_END then
                            tx_state <= ST_WAIT;
                        else
                            ack_i <= '1';
                            tx_state <= ST_WAIT;
                        end if;
                    -- /BASE_NIBBLE_TX
                    
                    when BASE_NIBBLE_RX =>
                        if rx_valid = '1' then
                            rx_valid <= '0';
                            dout_i <= x"0000_00" & rx_reg;
                            rx_reg <= (others => '0');
                            ack_i <= '1';
                        end if;
                    -- /BASE_NIBBLE_RX
                    
                    when BASE_NIBBLE_BRD =>
                        if we(0) = '1' then
                            uart_brd(7 downto 0) <= din(7 downto 0);
                        end if;
                        if we(1) = '1' then
                            uart_brd(15 downto 8) <= din(15 downto 8);
                        end if;
                        if we(2) = '1' then
                            uart_brd(23 downto 16) <= din(23 downto 16);
                        end if;
                        if we(3) = '1' then
                            uart_brd(31 downto 24) <= din(31 downto 24);
                        end if;
                        
                        dout_i <= uart_brd;
                        ack_i <= '1';
                    -- /BASE_NIBBLE_BRD
                    
                    -- non-blocking RX
                    when BASE_NIBBLE_RX_NB =>
                        dout_i <= x"0000_00" & rx_reg;
                        rx_reg <= (others => '0');
                        rx_valid <= '0';
                        ack_i <= '1';
                    -- /BASE_NIBBLE_RX_NB
                    
                    -- ready flag
                    when BASE_NIBBLE_RX_RDY =>
                        dout_i <= (others => '0');
                        dout_i(0) <= rx_valid;
                        ack_i <= '1';
                    -- /BASE_NIBBLE_RX_RDY
                    
                    when others =>
                        -- do nothing on wrong address, no error handling yet
                        ack_i <= '1';
                        dout_i <= (others => '0');
                end case;
            end if;
         end if;
      
    end if;
  end process;

  inst_tx : entity work.UART_TX
  port map (
    i_CLK => clk,
    i_TX_DV => tx_we,
    i_TX_Byte => tx_din,
    i_BRD => uart_brd,
    o_TX_Active => tx_busy,
    o_TX_Serial => txd,
    o_TX_Done => tx_ack
  );

  inst_rx : entity work.UART_RX
  port map (
    i_CLK => clk,
    i_RX_Serial => rxd,
    i_BRD => uart_brd,
    o_RX_DV => rx_rdy,
    o_RX_Byte => rx_dout
  );
end behave;
