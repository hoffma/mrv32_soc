library ieee;
use ieee.std_logic_1164.all;

entity sram_device is
    Generic (
        MEM_SIZE_BYTES : natural
    );
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic
    );
end sram_device;

architecture behave of sram_device is
    signal ack_i : std_logic;
    signal dout_i : std_logic_vector(31 downto 0);
begin
    dout <= dout_i;
    ack <= ack_i;

    inst_sram : entity work.sram
    generic map (
        MEM_SIZE_BYTES => MEM_SIZE_BYTES
    ) port map (
        clk => clk,
        
        en => en,
        wen => we,
        addr => addr,
        din => din,
        dout => dout_i,
        ack => ack_i
    );
end behave;
