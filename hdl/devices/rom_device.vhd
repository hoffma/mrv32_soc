library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.misc.all;

entity rom_device is
    Generic (
        ROM_INIT : mem32_t
    );
    Port (
        clk     : in std_logic;
        rst     : in std_logic;

        en      : in std_logic;
        we      : in std_logic_vector(3 downto 0);
        addr    : in std_logic_vector(31 downto 0);
        din     : in std_logic_vector(31 downto 0);
        dout    : out std_logic_vector(31 downto 0);
        ack     : out std_logic
    );
end rom_device;

architecture bram_behave of rom_device is
    signal ack_i : std_logic;
    signal dout_i : std_logic_vector(31 downto 0);
    
    COMPONENT blk_mem_gen_0
      PORT (
        clka : IN STD_LOGIC;
        rsta : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        addra : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        rsta_busy : OUT STD_LOGIC 
      );
    END COMPONENT;
begin
    dout <= dout_i;
    ack <= ack_i;
    
    inst_rom: blk_mem_gen_0
    port map (
        clka => clk,
        rsta => rst,
        ena => en,
        addra => addr,
        douta => dout_i,
        rsta_busy => open
    );
    
    process(clk)
    begin
        if rising_edge(clk) then
            ack_i <= '0';

            if en = '1' then
                ack_i <= '1';
            end if;
        end if;
    end process;
end bram_behave;

architecture behave of rom_device is
    signal ack_i : std_logic;
    signal dout_i : std_logic_vector(31 downto 0);
    signal addr_i : integer;
    
    signal mem_rom : mem32_t(0 to (ROM_SIZE_BYTES/4)-1) := ROM_INIT;
begin
    dout <= dout_i;
    ack <= ack_i;
    addr_i <= to_integer(unsigned(addr(log2_ceil_f((ROM_SIZE_BYTES/4))+1 downto 2)));

    process(clk)
    begin
        if rising_edge(clk) then
            dout_i <= (others => 'U');
            ack_i <= '0';

            if en = '1' then
                dout_i <= mem_rom(addr_i);
                ack_i <= '1';
            end if;
        end if;
    end process;

end behave;
