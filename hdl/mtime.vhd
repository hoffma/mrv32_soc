library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

-- TODO:
-- add enable
-- count up with T when enabled
entity mtime is
    Port (
        clk  : in std_logic;
        rst  : in std_logic;
        en   : in std_logic;
        wen  : in std_logic;
        addr : in std_logic_vector(31 downto 0);
        din  : in std_logic_vector(31 downto 0);
        dout : out std_logic_vector(31 downto 0);

        int  : out std_logic
    );
end mtime;

architecture behave of mtime is
    signal mtime_reg : unsigned(63 downto 0) := (others => '0');
    signal mtimecmp_reg : unsigned(63 downto 0) := (others => '0');
    signal mtime_en : std_logic_vector(31 downto 0) := (others => '0');
    signal mtime_ctrl : std_logic_vector(31 downto 0) := (others => '0');

    signal prescaler_reg : unsigned(31 downto 0);
    signal prescale_cnt : unsigned(31 downto 0);

    signal addr_i : std_logic_vector(11 downto 0);

    constant MTIME_REG_C    : std_logic_vector(11 downto 0) := x"000";
    constant MTIME_REGH_C   : std_logic_vector(11 downto 0) := x"004";
    constant MTIME_CMP_C    : std_logic_vector(11 downto 0) := x"008";
    constant MTIME_CMPH_C   : std_logic_vector(11 downto 0) := x"00c";
    constant MTIME_PSC_C    : std_logic_vector(11 downto 0) := x"010";
    constant MTIME_EN_C     : std_logic_vector(11 downto 0) := x"014";
    constant MTIME_CTRL_C   : std_logic_vector(11 downto 0) := x"018";

    constant MTIME_INT_EN_C : std_logic_vector(7 downto 0) := x"01";
    constant MTIME_FREERUN_C : std_logic_vector(7 downto 0) := x"02";
    -- constant MTIME_FREERUN_C : std_logic_vector(7 downto 0) := x"04";

    signal is_interrupt : std_logic;
    signal int_enabled : std_logic;
    signal is_freerun : std_logic;
    signal ctrl_byte : std_logic_vector(7 downto 0);
    
    signal dout_i : std_logic_vector(31 downto 0);
begin
    addr_i <= addr(11 downto 0);
    dout <= dout_i;

--    with addr_i select
--        dout <= mtime_ctrl                                   when MTIME_CTRL_C,
--                mtime_en                                     when MTIME_EN_C,
--                std_logic_vector(prescaler_reg)              when MTIME_PSC_C,
--                std_logic_vector(mtimecmp_reg(63 downto 32)) when MTIME_CMPH_C,
--                std_logic_vector(mtimecmp_reg(31 downto 0))  when MTIME_CMP_C,
--                std_logic_vector(mtime_reg(63 downto 32))    when MTIME_REGH_C,
--                std_logic_vector(mtime_reg(31 downto 0))     when MTIME_REG_C,
--                (others => '0') when others;
                
    rd : process(clk)
    begin
        if rising_edge(clk) then
            dout_i <= (others => '0');
            if en = '1' then
                case addr_i is
                    when MTIME_CTRL_C =>
                        dout_i <= mtime_ctrl;
                    when MTIME_EN_C =>
                        dout_i <= mtime_en;
                    when MTIME_PSC_C =>
                        dout_i <= std_logic_vector(prescaler_reg);
                    when MTIME_CMPH_C =>
                        dout_i <= std_logic_vector(mtimecmp_reg(63 downto 32));
                    when MTIME_CMP_C =>
                        dout_i <= std_logic_vector(mtimecmp_reg(31 downto 0));
                    when MTIME_REGH_C =>
                        dout_i <= std_logic_vector(mtime_reg(63 downto 32));
                    when MTIME_REG_C =>
                        dout_i <= std_logic_vector(mtime_reg(31 downto 0));
                    when others =>
                        dout_i <= (others => '0');
                end case;
            end if;
        end if;
    end process;

    is_freerun <= '1' when (ctrl_byte and MTIME_FREERUN_C) = MTIME_FREERUN_C else '0';
    int_enabled <= '1' when (ctrl_byte and MTIME_INT_EN_C) = MTIME_INT_EN_C else '0';
    is_interrupt <= '1' when (mtime_en(0) = '1') and (int_enabled = '1') and (mtime_reg >= mtimecmp_reg) else '0';
    ctrl_byte <= mtime_ctrl(7 downto 0);

    wr : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                mtime_en        <= (others => '0');
                -- mtime_reg       <= (others => '0');
                -- mtimecmp_reg    <= (others => '0');
                -- mtime_ctrl      <= (others => '0');

                -- prescale_cnt    <= (others => '0');
                -- prescaler_reg   <= (others => '0');
            else
                if mtime_en(0) = '1' then
                    prescale_cnt <= prescale_cnt + 1;
                    if prescale_cnt >= prescaler_reg then
                        if (is_interrupt and is_freerun) = '1' then
                            mtime_reg <= (others => '0');
                        else
                            mtime_reg <= mtime_reg + 1;
                        end if;
                        prescale_cnt <= (others => '0');
                    end if;
                else
                    prescale_cnt <= (others => '0');
                end if;

                if en = '1' and wen = '1' then
                    case addr_i is
                        when MTIME_REG_C =>
                            mtime_reg(31 downto 0) <= unsigned(din);
                        when MTIME_REGH_C =>
                            mtime_reg(63 downto 32) <= unsigned(din);
                        when MTIME_CMP_C =>
                            mtimecmp_reg(31 downto 0) <= unsigned(din);
                        when MTIME_CMPH_C =>
                            mtimecmp_reg(63 downto 32) <= unsigned(din);
                        when MTIME_PSC_C =>
                            prescaler_reg <= unsigned(din);
                        when MTIME_EN_C =>
                            mtime_en <= din;
                        when MTIME_CTRL_C =>
                            mtime_ctrl <= din;
                        when others =>
                            NULL;
                    end case;
                end if;
            end if;

            int <= is_interrupt;
        end if;
    end process;
end behave;
