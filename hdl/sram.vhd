library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.misc.all;

entity sram is
    Generic (
        MEM_SIZE_BYTES : natural
    );
    Port (
        clk  : in std_logic;
        en   : in std_logic;
        wen  : in std_logic_vector(3 downto 0);
        addr : in std_logic_vector(31 downto 0);
        din  : in std_logic_vector(31 downto 0);
        dout : out std_logic_vector(31 downto 0);
        ack  : out std_logic
    );
end sram;

architecture behave of sram is
    signal addr_i : integer;
    signal mem_ram : mem32_t(0 to (MEM_SIZE_BYTES/4)-1) := (others => (others => '0'));
begin
    addr_i <= to_integer(unsigned(addr(log2_ceil_f((MEM_SIZE_BYTES/4))+1 downto 2)));

    process(clk)
    begin
        if rising_edge(clk) then
            dout <= (others => 'U');
            ack <= '0';

            if en = '1' then
                if wen(0) = '1' then
                    mem_ram(addr_i)(7 downto 0) <= din(7 downto 0);
                end if;
                if wen(1) = '1' then
                    mem_ram(addr_i)(15 downto 8) <= din(15 downto 8);
                end if;
                if wen(2) = '1' then
                    mem_ram(addr_i)(23 downto 16) <= din(23 downto 16);
                end if;
                if wen(3) = '1' then
                    mem_ram(addr_i)(31 downto 24) <= din(31 downto 24);
                end if;

                dout <= mem_ram(addr_i);
                ack <= '1';
            end if;
        end if;
    end process;
end behave;
