library ieee;
use ieee.std_logic_1164.all;

entity arty_top_tb is
end arty_top_tb;

architecture behave of arty_top_tb is
    signal clk, rst : std_logic := '0';
    signal led : std_logic_vector(3 downto 0);
    signal uart_rx, uart_tx : std_logic;
    signal ck_io : std_logic_vector(11 downto 2);
    signal ck_a : std_logic_vector(4 downto 0);
begin
    clk <= not clk after 20 ns;
    rst <= '1', '0' after 200 ns;

    inst_top : entity work.arty_top
    port map (
        CLK100MHZ => clk,
        btn0 => rst,
        led => led,
        ck_io => ck_io,
        ck_a => ck_a,
        uart_txd_in => uart_tx,
        uart_rxd_out => uart_rx
    );
end behave;
