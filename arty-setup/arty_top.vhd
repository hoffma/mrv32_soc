library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity arty_top is
    Port (
        CLK100MHZ : in STD_LOGIC;
        btn0 : in STD_LOGIC;
        ck_io13 : in std_logic;
        led : out STD_LOGIC_VECTOR (3 downto 0);
        ja0 : out std_logic; -- txd
        ja1 : in std_logic; -- rxd
        jc0 : out std_logic; -- clk
        jc1 : out std_logic; -- latch
        jc2 : in std_logic; -- data
        -- uart_txd_in : in std_logic;
        -- uart_rxd_out : out std_logic;
        sw : in STD_LOGIC_VECTOR(3 downto 0);
        ck_io : out STD_LOGIC_VECTOR(11 downto 2);
        ck_a : out std_logic_vector(4 downto 0)
    );
end arty_top;

architecture Behavioral of arty_top is
    component clk_wiz_0
    port
    (
        clk_out1          : out    std_logic;
        clk_out2          : out    std_logic;
        clk_in1           : in     std_logic
    );
    end component;

    signal clk_soc, mat_clk : std_logic;
    signal rst : std_logic := '1';
    signal led_out : std_logic_vector(7 downto 0);

signal panel_rgb1 : std_logic_vector(2 downto 0);
signal panel_rgb2 : std_logic_vector(2 downto 0);
signal panel_clk : std_logic;
signal panel_oe : std_logic;
signal panel_lat : std_logic;
signal panel_row : std_logic_vector(4 downto 0);

signal rst_cnt : unsigned(15 downto 0) := x"ffff";
signal cnt_nz : std_logic;

signal dclk, dlatch, sdat : std_logic;
begin

    led <= led_out(3 downto 0);
    -- rst <= btn0 when rising_edge(CLK100MHZ);
    ck_io(4 downto 2) <= panel_rgb1;
    ck_io(7 downto 5) <= panel_rgb2;
    ck_io(8) <= panel_clk;
    ck_io(9) <= not panel_oe;
    ck_io(10) <= panel_lat;
    ck_a <= panel_row;
    jc0 <= dclk;
    jc1 <= dlatch;
    sdat <= jc2;
    
    -- some reset circuit????
    process(CLK100MHZ)
    begin
        if rising_edge(CLK100MHZ) then
            cnt_nz <= '0';
            if rst_cnt > 0 then
                cnt_nz <= '1';
                rst_cnt <= rst_cnt - 1;
            end if;
            
            if btn0 = '1' then
                rst_cnt <= x"ffff";
                cnt_nz <= '1';
            end if;
            
            rst <= ck_io13 or btn0 or cnt_nz;
        end if;
    end process;

    inst_clk : clk_wiz_0
    port map (
        clk_in1 => CLK100MHZ,
        clk_out1 => clk_soc,
        clk_out2 => mat_clk
    );

    inst_soc : entity work.soc
    port map (
        clk => clk_soc,
        rst => rst,
        led => led_out,
        uart0_rxd => ja1,
        uart0_txd => ja0,
        irq => '0',

        mat_clk => mat_clk,
        panel_clk => panel_clk,
        panel_oe => panel_oe,
        panel_lat => panel_lat,
        panel_row => panel_row,
        panel_rgb1 => panel_rgb1,
        panel_rgb2 => panel_rgb2,
        
        dclk => dclk,
        dlatch => dlatch,
        sdat => sdat
    );
end Behavioral;
