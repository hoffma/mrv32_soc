#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <fcntl.h>    // open()
#include <sys/stat.h> // fstat()
#include <sys/termios.h>
#include <termios.h>
#include <unistd.h> // close()

// #define BAUD_RATE (B1000000)
#define BAUD_RATE (B19200)
// #define BAUD_RATE (B115200)
#define DEFAULT_DEV "/dev/ttyUSB1"
#define SRAM_BASE_ADDR (0x0)
#define DATA_FAIL 0xACAB

struct ser_packet_t {
    uint16_t preamble;
    uint32_t word;
    uint16_t checksum;
};

int uart_open(char *device);
int uart_close(int tty_fd);
uint8_t uart_get();
uint8_t uart_write(uint8_t b);
uint8_t uart_write_word(uint32_t w);
void mem_write(uint32_t w);
void dump_mem(uint32_t *start_addr, uint32_t *end_addr);

uint32_t *BIN_DATA;
uint32_t *write_addr;
int uart_fd = 0;

void usage() {
  fprintf(stderr, "### Usage:\n");
  fprintf(stderr, "prog_load <binary-file> [tty-device]\n");
  fprintf(stderr, "\t Default baud rate is 115200.\n");
  fprintf(stderr, "\t [tty-device] - Serial Port. Default /dev/ttyUSB1\n");
}

uint16_t fletcher16(uint32_t *data_start, uint32_t cnt)
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    uint32_t *data = data_start;
    uint32_t index;

    for (index = 0; index < cnt; index += 4) {
        uint32_t word = *data;
        for (uint32_t i = 0; i < 4; ++i) {
            uint16_t tmp = (word >> (i*8))&0xff;
            sum1 = (sum1 + tmp) % 255;
            sum2 = (sum2 + sum1) % 255;
        }
        data++;
    }

    return (sum2 << 8) | sum1;
}

int main(int argc, char **argv) {
    char *serial_port = DEFAULT_DEV;
    char *bin_file = "main.bin";

    BIN_DATA = malloc(4096*sizeof(uint32_t));

    if (argc < 2) {
        fprintf(stderr, "Binary file not supplied!\n");
        usage();
        exit(EXIT_FAILURE);
    } 
    if (argc >= 2) {
        serial_port = argv[1];
    }
    if (argc >= 3) {
        bin_file = argv[2];
    }

    printf("Serial port: '%s'\n", serial_port);
    uart_fd = uart_open(serial_port);
    printf("uart_fd: %d\n", uart_fd);

    int bin_fd;
    uint32_t bin_size = 0;
    struct stat file_info;

    bin_fd = open(bin_file, O_RDONLY);
    fstat(bin_fd, &file_info);
    bin_size = file_info.st_size;
    printf("Binary file size: %d\n", bin_size);

    auto cnt = read(bin_fd, BIN_DATA, bin_size);
    printf("Read %lu bytes from binary\n", cnt);
    close(bin_fd);

    uint16_t chksum_calc = fletcher16(BIN_DATA, cnt);
    printf("calculated checksum: 0x%04x\n", chksum_calc);

    uint32_t recv_b = 0;
    const uint32_t data_b = 0xaa55aa55;

    tcdrain(uart_fd);
    do {
        auto n = write(uart_fd, &data_b, sizeof(data_b));
        printf("Wrote %lu bytes to uart, 0x%02x\n", n, data_b);
        n = read(uart_fd, &recv_b, sizeof(recv_b));
        printf("Received bytes: 0x%02x\n", recv_b);
    } while (recv_b != data_b);

    printf("Sending bin size\n");
    auto write_cnt = write(uart_fd, &bin_size, sizeof(bin_size));
    printf("Wrote %lu bytes to uart: 0x%08x\n", write_cnt, bin_size);
    uint32_t recv_sz = 0;
    auto n = read(uart_fd, &recv_sz, sizeof(recv_sz));
    printf("Received %lu bytes: 0x%08x\n", n, recv_sz);
    tcdrain(uart_fd);

    printf("Sending checksum.\n");
    write_cnt = write(uart_fd, &chksum_calc, sizeof(chksum_calc));
    printf("Wrote %lu bytes to uart: 0x%08x\n", write_cnt, chksum_calc);
    uint16_t recv_chk = 0;
    n = read(uart_fd, &recv_chk, sizeof(recv_chk));
    printf("Received %lu bytes: 0x%08x\n", n, recv_chk);
    tcdrain(uart_fd);

    write_cnt = write(uart_fd, BIN_DATA, cnt);
    printf("Wrote %lu bytes to uart\n", write_cnt);
    // tcdrain(uart_fd);

    // n = cnt;
    // uint32_t *bin_ptr = BIN_DATA;
    // // printf("start n: %lu\n", n);
    // printf("Sending payload...\n");
    // while(n > 0) {
    //     uint32_t dout = *bin_ptr;
    //     uint32_t din = 0;

    //     write_cnt = write(uart_fd, bin_ptr, 4);
    //     printf("Wrote %lu bytes to uart\n", write_cnt);
    //     n -= write_cnt;
    //     auto read_cnt = read(uart_fd, &din, 4);
    //     if (din != dout) {
    //         printf("ERROR! sent: 0x%08x, recvd: 0x%08x\n", dout, din);
    //         exit(EXIT_FAILURE);
    //     }
    //     if (n > 0) bin_ptr++;
    // }

    uint16_t chksum_in = 0;
    auto read_cnt = read(uart_fd, &chksum_in, sizeof(chksum_in));
    if (chksum_calc != chksum_in) {
        fprintf(stderr, "CHKESUM DOES NOT MATCH. Expected: 0x%04x, received: 0x%04x\n", chksum_calc, chksum_in);
    }
    printf("Received checksum: 0x%04x\n", chksum_in);

    free(BIN_DATA);
    uart_close(uart_fd);
    return 0;
}

void dump_mem(uint32_t *start_addr, uint32_t *end_addr) {
    uint32_t *start = start_addr;
    while (start < end_addr) {
        printf("0x%08x\n", *start);
        start++;
    }
}

uint8_t uart_get() {
    uint8_t b = 0;
    uint32_t n = 0;
    if ((n = read(uart_fd, &b, 1)) > 0) {
        // printf("read byte: 0x%02x\n", b);
        return b;
    }

    return 0; // fail
}

uint8_t uart_write_word(uint32_t w) {
    for (auto i = 0; i < 4; ++i) {
        uint8_t tmp = (w >> (i*8))&0xff;
        if (uart_write(tmp) == 0) {
            return 0; // fail
        }
    }
    return 1;
}

uint8_t uart_write(uint8_t b) {
    uint32_t n = 0;
    if ((n = write(uart_fd, &b, 1)) > 0) {
        return b;
    }
    return 0;
}

void mem_write(uint32_t w) {
    *write_addr = w;
    write_addr++;
}

int uart_open(char *device) {
  int tty_fd;
  struct termios termios_attr;

  if ((tty_fd = open(device, O_RDWR)) < 0) {
    perror("Erorr opening device ");
    exit(EXIT_FAILURE);
  }

  if (tcgetattr(tty_fd, &termios_attr) < 0) {
    perror("Error on tcgetattr");
    exit(EXIT_FAILURE);
  }

  cfsetospeed(&termios_attr, BAUD_RATE);
  cfsetispeed(&termios_attr, BAUD_RATE); /* input baud rate same as output baud */
  // cfmakeraw(&termios_attr);       /* raw mode. nothing additional */
  /* set the parameter immediately */
  if (tcsetattr(tty_fd, TCSANOW, &termios_attr) < 0) {
    perror("Error on tcsetattr ");
    exit(EXIT_FAILURE);
  }

  printf("cflag: %u\n", termios_attr.c_cflag);
  printf("cc: %s\n", termios_attr.c_cc);
  printf("oflag: %u\n", termios_attr.c_oflag);
  printf("ospeed: %u\n", termios_attr.c_ospeed);

  printf("tty_fd: %d\n", tty_fd);

  return tty_fd;
}

int uart_close(int fd) { return close(fd); }
