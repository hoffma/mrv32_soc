#include "prog_loader.h"

uint16_t loader_load(
        uint8_t (recv_byte)(void),
        void (write_mem)(uint32_t)
) {
    uint32_t recvd_len = 0;
    uint16_t recvd_checksum = 0;

    // get length
    for (int i = 0; i < 4; i++) {
        uint32_t b = recv_byte();
        recvd_len |= (b << (i*8));
    }

    // get checksum
    for (int i = 0; i < 2; i++) {
        uint32_t b = recv_byte();
        recvd_checksum |= (b << (i*8));
    }

    while (recvd_len > 0) {
        uint32_t tmp_word = 0;
        for (int i = 0; i < 4; i++) {
            uint8_t b = recv_byte();
            tmp_word |= (b << (i*8));
            recvd_len--;
        }
        write_mem(tmp_word);
    }
    return recvd_checksum;
}
