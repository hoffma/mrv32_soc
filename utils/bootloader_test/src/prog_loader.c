#include "prog_loader.h"

uint32_t get_word(uint8_t (recv_byte)(void));
uint16_t get_half_word(uint8_t (recv_byte)(void));

uint32_t get_word(uint8_t (recv_byte)(void)) {
    uint32_t tmp_word = 0;
    for (uint32_t i = 0; i < 4; ++i) {
        uint8_t b = recv_byte();
        tmp_word |= (uint32_t)(b << (i*8));
    }
    return tmp_word;
}

uint16_t get_half_word(uint8_t (recv_byte)(void)) {
    uint16_t tmp_word = 0;
    for (uint32_t i = 0; i < 2; ++i) {
        uint8_t b = recv_byte();
        tmp_word |= (uint16_t)(b << (i*8));
    }
    return tmp_word;
}

uint16_t loader_load(
        uint8_t (recv_byte)(void),
        void (write_mem)(uint32_t)
) {
    // uint32_t recvd_len = 0;
    // uint16_t recvd_checksum = 0;

    // get length
    uint32_t recvd_len = get_word(recv_byte);
    uint16_t recvd_checksum = get_half_word(recv_byte);

    while (recvd_len > 0) {
        uint32_t tmp_word = 0;
        uint32_t w = get_word(recv_byte);
        write_mem(w);
        recvd_len = recvd_len - 4;
    }
    return recvd_checksum;
}
