#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <fcntl.h>    // open()
#include <sys/stat.h> // fstat()
#include <sys/termios.h>
#include <termios.h>
#include <unistd.h> // close()

#include "prog_loader.h"

#define BAUD_RATE (B115200)
#define DEFAULT_DEV "/dev/ttyUSB1"
#define SRAM_BASE_ADDR (0x0)
#define DATA_FAIL 0xACAB

// struct ser_packet_t {
//     uint16_t preamble;
//     uint32_t word;
//     uint16_t checksum;
// };

int uart_open(char *device);
int uart_close(int tty_fd);
uint8_t uart_get();
uint8_t uart_write(uint8_t b);
void mem_write(uint32_t w);
void dump_mem(uint32_t *start_addr, uint32_t *end_addr);

uint32_t MEM_DATA[4096];
uint32_t *write_addr;
int uart_fd = 0;

uint16_t fletcher16(uint32_t *data_start, uint32_t *data_end)
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    uint32_t *data = data_start;
    uint32_t index;

    while (data < data_end) {
        uint32_t word = *data;
        for (int i = 0; i < 4; ++i) {
            uint16_t tmp = (word >> (i*8))&0xff;
            sum1 = (sum1 + tmp) % 255;
            sum2 = (sum2 + sum1) % 255;
        }
        data++;
    }

    return (sum2 << 8) | sum1;
}

int main(int argc, char **argv) {
    char *serial_port = DEFAULT_DEV;

    if (argc < 2) {
        fprintf(stderr, "Serial port not supplied!\n");
        exit(EXIT_FAILURE);
    } else if (argc >= 2) {
        serial_port = argv[1];
    }

    printf("Serial port: '%s'\n", serial_port);
    uart_fd = uart_open(serial_port);
    printf("uart_fd: %d\n", uart_fd);

    write_addr = &MEM_DATA[0];

    uint8_t recv = 0;
    do {
        recv = uart_get();
        uart_write(recv);
    } while (recv != 0xaa);

    uint16_t chksum_in = loader_load(uart_get, mem_write);
    printf("Received checksum: 0x%04x\n", chksum_in);

    uint16_t chksum_calc = fletcher16(&MEM_DATA[0], write_addr);
    printf("calculated checksum: 0x%04x\n", chksum_calc);

    uart_write(chksum_calc&0xff);
    uart_write((chksum_calc >> 8)&0xff);

    // dump_mem(&MEM_DATA[0], write_addr);

    uart_close(uart_fd);
    return 0;
}

void dump_mem(uint32_t *start_addr, uint32_t *end_addr) {
    uint32_t *start = start_addr;
    while (start < end_addr) {
        printf("0x%08x\n", *start);
        start++;
    }
}

uint8_t uart_get() {
    uint8_t b = 0;
    uint32_t n = 0;
    if ((n = read(uart_fd, &b, 1)) > 0) {
        // printf("read byte: 0x%02x\n", b);
        return b;
    }

    return 0; // fail
}

uint8_t uart_write(uint8_t b) {
    uint32_t n = 0;
    if ((n = write(uart_fd, &b, 1)) > 0) {
        return b;
    }
    return 0;
}

void mem_write(uint32_t w) {
    *write_addr = w;
    write_addr++;
}

int uart_open(char *device) {
  int tty_fd;
  struct termios termios_attr;

  if ((tty_fd = open(device, O_RDWR)) < 0) {
    perror("Erorr opening device ");
    exit(EXIT_FAILURE);
  }

  if (tcgetattr(tty_fd, &termios_attr) < 0) {
    perror("Error on tcgetattr");
    exit(EXIT_FAILURE);
  }

  cfsetospeed(&termios_attr, BAUD_RATE);
  cfsetispeed(&termios_attr, B0); /* input baud rate same as output baud */
  cfmakeraw(&termios_attr);       /* raw mode. nothing additional */
  /* set the parameter immediately */
  if (tcsetattr(tty_fd, TCSANOW, &termios_attr) < 0) {
    perror("Error on tcsetattr ");
    exit(EXIT_FAILURE);
  }

  printf("cflag: %u\n", termios_attr.c_cflag);
  printf("cc: %s\n", termios_attr.c_cc);
  printf("oflag: %u\n", termios_attr.c_oflag);
  printf("ospeed: %u\n", termios_attr.c_ospeed);

  printf("tty_fd: %d\n", tty_fd);

  return tty_fd;
}

int uart_close(int fd) { return close(fd); }
