
TARGET=template
APP_ELF = main.elf
APP_BIN = main.bin

MEM_PLACEHOLDER=mem_placeholder.vhd

MARCH ?= rv32i_zicsr
MABI ?= ilp32

# bootloader should only use rv32i instructions anyways
RISCV_PREFIX = riscv32-unknown-elf-
CC = 		$(RISCV_PREFIX)gcc
CXX = 		$(RISCV_PREFIX)g++
OBJDUMP = 	$(RISCV_PREFIX)objdump
OBJCOPY = 	$(RISCV_PREFIX)objcopy
SIZE = 		$(RISCV_PREFIX)size
ELF2HEX = 	$(RISCV_PREFIX)elf2hex

SRCDIR=src
OBJDIR=obj

APP_HEX = firmware.hex
APP_SRC = $(wildcard $(SRCDIR)/*.c)
START_SRC = ../crt0.S

APP_OBJ = $(START_SRC:%.S=$(OBJDIR)/%.o)
APP_OBJ += $(APP_SRC:$(SRCDIR)/%.c=$(OBJDIR)/%.o) 


LD_SCRIPT=../link.ld

EFFORT = -Os
STD = --std=c2x
CC_WARNS = -Wl,-Bstatic,--strip-debug
CC_FLAGS = $(EFFORT) $(STD) $(CC_WARNS) -ffreestanding -nostdlib
LD_FLAGS =
LIBS=-lgcc

OBJ = $(APP_OBJ)

all: $(APP_HEX) $(APP_BIN)


$(APP_HEX): $(OBJDIR) $(APP_ELF)
	$(SIZE) $(APP_ELF)
	$(ELF2HEX) --bit-width 32 --input $(APP_ELF) > $(APP_HEX)

$(OBJDIR):
	@echo $(OBJ)
	mkdir -p $(OBJDIR)

$(OBJDIR)/%.o: %.S
	$(CC) -mabi=$(MABI) -march=$(MARCH) -c $(CC_FLAGS) $< -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) -mabi=$(MABI) -march=$(MARCH) -c $(CC_FLAGS) $(LIB_INC) $< -o $@

$(APP_ELF): $(OBJ)
	$(CC) -mabi=$(MABI) -march=$(MARCH) -T$(LD_SCRIPT) $(CC_FLAGS) $^ -o $@ $(LIBS)

$(APP_BIN): $(APP_ELF)
	@$(OBJCOPY) -O binary $< /dev/stdout > $@

clean:
	rm -rf $(OBJDIR)
	rm -f $(APP_ELF)
	rm -f $(APP_BIN)
	rm -f $(APP_HEX)
