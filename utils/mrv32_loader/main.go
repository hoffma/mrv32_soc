package main

import (
	"encoding/binary"
	"fmt"
	"os"

	"github.com/tarm/serial"
)

func Fletcher16(data []byte) uint16 {
	var sum1 uint16 = 0
	var sum2 uint16 = 0

	for i := 0; i < len(data); i++ {
		sum1 = (sum1 + uint16(data[i])) % 255
		sum2 = (sum2 + sum1) % 255
	}

	return (sum2 << 8) | sum1
}

func printBuf(buf []byte) {
	for i := range buf {
		fmt.Printf(" %02x", buf[i])
	}
	fmt.Printf("\n")
}

func help() {
	fmt.Println("Usage:")
	fmt.Printf("%s <serialPort> <bin-file>\n", os.Args[0])
	fmt.Println("Baudrate is set to 19200")
}

func main() {
	// c := &serial.Config{Name: "/dev/pts/7", Baud: 115200}
	if len(os.Args) < 3 {
		help()
		panic("Not enough arguments")
	}

	serialPort := os.Args[1]
	fname := os.Args[2]
	fmt.Println("Serial Port: ", serialPort, ", fname: ", fname)
	c := &serial.Config{Name: serialPort, Baud: 19200}

	fstat, err := os.Stat(fname)
	if err != nil {
		panic("file does not exist")
	}

	nbytes := fstat.Size()
	// TODO: check if too big here
	var prog_in []byte
	if (nbytes % 4) != 0 {
		pad_bytes := 4 - (nbytes % 4)
		nbytes += pad_bytes
		prog_in = make([]byte, nbytes)
	} else {
		prog_in = make([]byte, nbytes)
	}

	fmt.Println("fname: ", fname)
	file, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file", err)
		return
	}

	_, err = file.Read(prog_in)
	if err != nil {
		panic("Error reading file")
	}

	// fmt.Println("file bytes: ", nbytes)
	fmt.Println("payload bytes: ", len(prog_in))

	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}

	recv := uint32(0)
	expected := uint32(0xaa55aa55)
	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, uint32(expected))
	for recv != expected {
		n, _ := s.Write(buf)
		fmt.Println("writing dummy byte: ", buf)
		fmt.Println("n bytes written:", n)
		rbuf := make([]byte, 4)
		n, err = s.Read(rbuf)
		if err != nil {
			panic(err)
		}
		recv = binary.LittleEndian.Uint32(rbuf)
		fmt.Printf("received: 0x%08x\n", recv)
	}

	buf = make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, uint32(nbytes))
	n, err := s.Write(buf)
	fmt.Println("len: ", nbytes, ", filesize: ", fstat.Size())
	fmt.Println("n bytes written:", n)
	// printBuf(buf)
	rbuf := make([]byte, 4)
	n, err = s.Read(rbuf)
	if err != nil {
		panic(err)
	}
	rsize := binary.LittleEndian.Uint32(rbuf)
	fmt.Printf("Sent size: %d, received size: %d\n", nbytes, rsize)
	if rsize != uint32(nbytes) {
		panic(fmt.Errorf("Sent size: %d, received size: %d\n", nbytes, rsize))
	}

	buf = make([]byte, 2)
	sum := Fletcher16(prog_in)
	binary.LittleEndian.PutUint16(buf, sum)
	n, err = s.Write(buf)
	fmt.Println("n bytes written:", n)
	rbuf = make([]byte, 2)
	n, err = s.Read(rbuf)
	if err != nil {
		panic(err)
	}
	rchksum := binary.LittleEndian.Uint16(rbuf)
	fmt.Printf("Sent checksum: 0x%04x, received checksum: 0x%04x\n", rchksum, sum)
	if rchksum != sum {
		panic(fmt.Errorf("Sent checksum: %d, received checksum: %d\n", rchksum, sum))
	}

	// TODO: Fix sending here

	fmt.Println("writing program ...")
	n, err = s.Write(prog_in)
	fmt.Println("n bytes written:", n)
	fmt.Printf("fletcher sum: 0x%04x\n", sum)

	buf = make([]byte, 2)
	n, err = s.Read(buf)
	if err != nil {
		panic(err)
	}
	recv_sum := binary.LittleEndian.Uint16(buf)
	fmt.Printf("received calculated checksum: 0x%04x\n", recv_sum)
}
