package main

import (
	"encoding/binary"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/tarm/serial"
)

func Fletcher16(data []byte) uint16 {
	var sum1 uint16 = 0
	var sum2 uint16 = 0

	for i := 0; i < len(data); i++ {
		sum1 = (sum1 + uint16(data[i])) % 255
		sum2 = (sum2 + sum1) % 255
	}

	return (sum2 << 8) | sum1
}

func printBuf(buf []byte) {
	for i := range buf {
		fmt.Printf(" %02x", buf[i])
	}
	fmt.Printf("\n")
}

func main() {
	// teststr := "HELLO, WORLD!"

	// c := &serial.Config{Name: "/dev/pts/7", Baud: 115200}
	if len(os.Args) < 2 {
		panic("Not enough arguments")
	}

	serialPort := os.Args[1]
	fmt.Println("Serial Port: ", serialPort)
	c := &serial.Config{Name: serialPort, Baud: 19200, StopBits: 1, ReadTimeout: time.Millisecond * 500}

	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}

	// for j := 0; j < 1000; j++ {
	// 	for i := 0; i < len(teststr); i++ {
	// 		buf := make([]byte, 1)
	// 		buf[0] = teststr[i]
	// 		n, _ := s.Write(buf)
	// 		fmt.Println("writing: ", buf, "n bytes written:", n)
	// 		rbuf := make([]byte, 1)
	// 		n, err = s.Read(rbuf)
	// 		if err != nil {
	// 			panic(err)
	// 		}
	// 		fmt.Println("received: ", rbuf)
	// 		if buf[0] != rbuf[0] {
	// 			panic(fmt.Errorf("Sent: %s, received: %s\n", buf, rbuf))
	// 		}
	// 	}
	// }
	for i := 0; i < 2000; i++ {
		buf := make([]byte, 4)
		num := rand.Uint32()
		binary.LittleEndian.PutUint32(buf, uint32(num))
		n, err := s.Write(buf)
		fmt.Println("buf: ", buf)
		fmt.Println("num: ", num, ", n bytes written:", n)
		if err != nil {
			panic(err)
		}
		rbuf := make([]byte, 4)
		n, err = s.Read(rbuf)
		if err != nil {
			panic(err)
		}
		recv_num := binary.LittleEndian.Uint32(rbuf)
		if num != recv_num {
			panic(fmt.Errorf("Sent: %d, received: %d\n", num, recv_num))
		}
		fmt.Println("received: ", rbuf)
	}
}
