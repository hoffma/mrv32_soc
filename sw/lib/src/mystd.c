#include "mystd.h"

struct uart_t * UART0;
struct uart_t * UART1;
struct led_t * LED0;
struct mtime_t * MTIME0;

void std_init() {
    LED0 =  (struct led_t *) LED_BASE;
    UART0 =  (struct uart_t *) UART0_BASE;
    UART1 =  (struct uart_t *) UART1_BASE;
    MTIME0 =  (struct mtime_t *) MTIME_BASE;
}

void sei() {
    __asm__ volatile("li t1, 128\n\t"
            "csrw mie, t1\n\t"
            "li t1, 8\n\t"
            "csrw mstatus, t1\n\t");
}

void cli() {
    __asm__ volatile("li t1, 128\n\t"
            "csrc mie, t1\n\t"
            "li t1, 8\n\t"
            "csrc mstatus, t1\n\t");
}

