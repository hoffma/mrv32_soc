#pragma once

#include <stdint.h>

#ifndef F_CPU
#define F_CPU (100000000)
#warning "Defined F_CPU at 100MHz"
#endif

#define MTIME_BASE  (0xfffff000)
#define LED_BASE    (0x01000000)
#define UART0_BASE  (0x01100000)
#define UART1_BASE  (0x01100100)
#define MAT_BASE    (0x01200000)

struct uart_t {
    uint32_t tx;
    uint32_t rx;
    uint32_t brd;
    uint32_t rx_nb;
    uint32_t rx_valid;
};

struct led_t {
    uint32_t val;
};

struct mtime_t {
    uint64_t cnt;
    uint64_t cmp;
    uint32_t psc;
    uint32_t en;
    uint32_t ctrl;
};

extern struct uart_t * UART0;
extern struct uart_t * UART1;
extern struct led_t * LED0;
extern struct mtime_t * MTIME0;

extern void ISR();

void std_init();
void sei();
void cli();

