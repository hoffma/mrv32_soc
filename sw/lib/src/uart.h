#pragma once

#include "mystd.h"

struct uart_t;

void uart_set_brd(struct uart_t *uart_dev, uint32_t brd);
void uart_send_byte(struct uart_t *uart_dev, uint8_t b);
[[nodiscard]] uint8_t uart_recv_byte(struct uart_t *uart_dev);
void uart_putchar(struct uart_t *uart_dev, char c);
void uart_puts(struct uart_t *uart_dev, char *s);
uint8_t uart_recv_nb(struct uart_t *uart_dev);
uint8_t uart_rx_valid(struct uart_t *uart_dev);
