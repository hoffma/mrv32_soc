#include "uart.h"

void uart_set_brd(struct uart_t *uart_dev, uint32_t brd) {
    uart_dev->brd = brd;
} 

void uart_send_byte(struct uart_t *uart_dev, uint8_t b) {
    uart_dev->tx = b;
}

uint8_t uart_recv_byte(struct uart_t *uart_dev) {
    return uart_dev->rx;
}

void uart_putchar(struct uart_t *uart_dev, char c) {
    if (c == '\n')
        uart_putchar(uart_dev, '\r');
    uart_dev->tx = c;
}

void uart_puts(struct uart_t *uart_dev, char *s) {
    while (*s) {
        uart_putchar(uart_dev, *s);
        s++;
    }
}

[[nodiscard]] char uart_getchar(struct uart_t *uart_dev) {
    return uart_dev->rx;
}

[[nodiscard]] uint8_t uart_recv_nb(struct uart_t *uart_dev) {
    return uart_dev->rx_nb;
}

[[nodiscard]] uint8_t uart_rx_valid(struct uart_t *uart_dev) {
    return uart_dev->rx_valid;
}
