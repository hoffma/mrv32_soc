#include "led_matrix.h"

#define MATRIX_START ((volatile uint32_t *) 0x01200000)

static uint32_t MAT_W = 64;
static uint32_t MAT_H = 32;
static uint32_t MAT_cnt = 2;
static uint32_t MAT_rows = 1;
static uint32_t MAT_cols = 0;
static uint32_t MAT_pxl_per_row= 0;
static uint32_t MAT_pxl_per_col = 0;
static uint32_t MAT_pxl_row_chain = 0;

static int get_addr_offset(uint32_t x, uint32_t y, uint32_t *off_o, bool *is_top) {
    if ((x < 0) || (y < 0)) return -1;
    if (y >= (MAT_rows*MAT_H)) return -1;
    if (x >= (MAT_cols*MAT_W)) return -1;

    // y = (MAT_cols*MAT_H) - 1 - y;
    // x = (MAT_rows*MAT_W - 1) - x;

    int addr_offset = 0;
    int start_addr = 0;
    // get the panel row number
    int tmp_y = (y/MAT_H);
    bool inverted = (tmp_y&1) == 1;
    int rel_y = inverted ? (MAT_H - y%MAT_H - 1) : (y%MAT_H);

    if (rel_y >= (MAT_H/2)) {
        rel_y -= (MAT_H/2);
        *is_top = false;
    }  else {
        *is_top = true;
    }
    start_addr = MAT_pxl_row_chain*rel_y + tmp_y*MAT_pxl_per_row;

    if (inverted) {
        start_addr += (MAT_pxl_per_row - x - 1);
    } else {
        start_addr += x;
    }

    *off_o = start_addr;
    return 0;
}

static int get_addr(uint32_t x, uint32_t y, uint32_t *off_o, bool *is_top) {
    if ((x < 0) || (y < 0)) return -1;
    if (y >= (MAT_rows*MAT_H)) return -1;
    if (x >= (MAT_cols*MAT_W)) return -1;

    x = MAT_pxl_per_row - x - 1;
    y = MAT_pxl_per_col - y - 1;

    int tmp_offset = 0;
    int start_addr = 0;

    int tmp_y = (y/MAT_H);
    bool inverted = (tmp_y&1) == 1;
    int rel_y = inverted ? (MAT_H - y%MAT_H - 1) : (y%MAT_H);

    if (rel_y >= (MAT_H/2)) {
        rel_y -= (MAT_H/2);
        *is_top = false;
    }  else {
        *is_top = true;
    }
    start_addr = MAT_pxl_row_chain*rel_y + tmp_y*MAT_pxl_per_row;
    if (inverted) {
        start_addr += x;
    } else {
        start_addr += (MAT_pxl_per_row - x - 1);
    }

    // start_addr = (y*MAT_pxl_per_row);
    // start_addr += (MAT_pxl_per_row - 1) - x;
    *off_o = start_addr;

    return 0;
}

void matrix_init(
    uint32_t pnl_w,
    uint32_t pnl_h,
    uint32_t pnl_cnt,
    uint32_t mat_rows
) {
    MAT_W = pnl_w;
    MAT_H = pnl_h;
    MAT_cnt = pnl_cnt;
    MAT_rows = mat_rows;
    MAT_cols = MAT_cnt/MAT_rows;
    MAT_pxl_per_row = MAT_W*MAT_cols;
    MAT_pxl_per_col = MAT_H*MAT_rows;
    MAT_pxl_row_chain = MAT_cnt*MAT_W;
}

void matrix_clear() {
    const int pxl_cnt = (MAT_W*MAT_H*MAT_cnt)/2;
    for(auto i = 0; i < pxl_cnt; ++i) {
        *(MATRIX_START+i) = 0x0;
    }
}

void matrix_fill(uint8_t clr) {
    const int pxl_cnt = (MAT_W*MAT_H*MAT_cnt)/2;
    for(auto i = 0; i < pxl_cnt; ++i) {
        *(MATRIX_START+i) = (clr << 8) | clr;
    }
}

int matrix_pixel_xy(
    uint32_t x,
    uint32_t y,
    uint8_t clr
) {
    uint32_t offset = 0;
    bool is_top = true;
    auto res = get_addr(x, y, &offset, &is_top);
    if (res == 0) {
        if (is_top) {
            *(MATRIX_START + offset) &= ~(0xff); 
            *(MATRIX_START + offset) |= clr; 
        } else {
            *(MATRIX_START + offset) &= ~(0xff00); 
            *(MATRIX_START + offset) |= (clr << 8); 
        }
    }
    return res;
}

void matrix_set(uint16_t n, uint16_t val) {
    *(MATRIX_START + n) = val&0xffff; 
}
