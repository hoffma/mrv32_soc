#pragma once

#include <stdint.h>

void matrix_init(uint32_t pnl_w, uint32_t pnl_h, uint32_t mat_cnt, uint32_t mat_rows);
void matrix_clear();
void matrix_fill(uint8_t clr);
int matrix_pixel_xy(uint32_t x, uint32_t y, uint8_t clr);
void matrix_set(uint16_t n, uint16_t val);
