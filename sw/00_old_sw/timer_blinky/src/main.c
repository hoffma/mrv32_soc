#include <stdint.h>

extern void ISR(void);
extern int main(void);

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__ volatile("li t1, 128\n\t"
            "csrw mie, t1\n\t"
            "li t1, 8\n\t"
            "csrw mstatus, t1\n\t");
}

void ISR(void) {
    uint32_t tmp = LED_PORT;
    tmp++;
    LED_PORT = tmp;
}

int main() {
    LED_PORT = 0x01;

    MTIME_EN = 0x0; /* disable timer */
    MTIME_REG = 0;
    MTIME_REGH = 0;

    MTIME_CMP = 100;
    // MTIME_CMP = 5;
    MTIME_CMPH = 0;
    MTIME_PSC = 24999;
    // MTIME_PSC = 4;

    sei();
    MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x1; /* enable the timer */

  while (1) {
      /* do nothing */
  }
}
