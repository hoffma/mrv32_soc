#include <stdint.h>

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

void short_delay() {
  for (volatile uint32_t i = 0; i < 10; i++)
    ;
}


void delay() {
  for (volatile uint32_t i = 0; i < 10000; i++)
    short_delay();
}

int main() {
    LED_PORT = 0;

    while(1) {
        LED_PORT++;
        delay();
    }
}
