#include <stdint.h>

extern void ISR(void);
extern int main(void);

// 25 MHz
#define F_CPU (25000000)
#define BAUD_RATE (115200)

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define UART_BASE (0x01100000)
#define UART_TX_PORT (*(volatile uint32_t *)(UART_BASE + 0x00))
#define UART_RX_PORT (*(volatile uint32_t *)(UART_BASE + 0x04))
#define UART_BRD_PORT (*(volatile uint32_t *)(UART_BASE + 0x08))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__("li t1, 128\n\t"
            "csrw mie, t1\n\t"
            "li t1, 8\n\t"
            "csrw mstatus, t1\n\t");
}

void uart_set_brd(uint32_t brd);
void uart_putchar(char c);
void uart_putstr(char *s);

volatile uint8_t tick = 0;

void ISR(void) {
    LED_PORT++;
    tick = 1;
}

void delay() {
    for (volatile int i = 0; i < 100000; i++)
        ;
}


void uart_set_brd(uint32_t brd) {
    UART_BRD_PORT = brd;
}

void uart_putchar(char c) {
    if (c == '\n')
        uart_putchar('\r');
    UART_TX_PORT = c;
}

void uart_putstr(char *s) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
}

int main() {

    LED_PORT = 0x0;
    uart_set_brd(F_CPU/BAUD_RATE);

    MTIME_REG = 0;
    MTIME_REGH = 0;
    MTIME_CMP = 1000;
    MTIME_CMPH = 0;
    MTIME_PSC = 24999;
    sei();
    MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x1; /* enable the timer */

    while (1) {
        if (tick) {
            uart_putstr("Hello!\n");
            tick = 0;
        }
    }
}
