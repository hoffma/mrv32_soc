#pragma once

#include <stdint.h>

#define PANEL_W (64)
#define PANEL_H (32)
#define PANEL_CNT (4)
#define PANEL_CNT_X (2)

#define PIXEL_CNT (PANEL_W*PANEL_H*PANEL_CNT)
#define PIXEL_CNT_HALF (PIXEL_CNT >> 1)

#define MAT_BASE    (0x90000000)
#define MAT_TOP_REG (*(volatile uint32_t *) (MAT_BASE + 0x00))
#define MAT_BOT_REG (*(volatile uint32_t *) (MAT_BASE + 0x04))
#define MAT_REG     (*(volatile uint32_t *) (MAT_BASE + 0x08))

uint16_t coordToPixel(uint16_t x, uint16_t y);
