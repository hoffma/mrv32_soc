#include <stdint.h>

typedef uint32_t u32;

extern void ISR(void);
extern int main(void);

#define MAT_WIDTH (64)
#define MAT_HEIGHT (32)
#define MAT_CHAIN (1)
#define PIXEL_COUNT (MAT_WIDTH*MAT_HEIGHT*MAT_CHAIN)

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__("li a1, 128\n\t"
            "csrw mie, a1\n\t"
            "li a1, 8\n\t"
            "csrw mstatus, a1\n\t");
}

void ISR(void) {
    uint32_t tmp = LED_PORT;
    tmp++;
    LED_PORT = tmp;
}

void setPixel(uint16_t n, uint16_t clr) {
    uint32_t data = 0;

    data |= (n << 16); // set address
    data |= ((clr&0x7) << 8) | (clr&0x7); // set color
    MAT_REG = data;
}

void setPixelTop(uint16_t n, uint16_t clr) {
    uint32_t data = 0;

    data |= (n << 16); // set address
    data |= (clr&0x7); // set color
    MAT_TOP_REG = data;
}

void setPixelBot(uint16_t n, uint16_t clr) {
    uint32_t data = 0;
    data |= (clr&0x7); // set color
    data <<= 8;
    data |= (n << 16); // set address
    MAT_BOT_REG = data;
}

void setPixelXY(uint16_t x, uint16_t y, uint16_t clr) {
    const uint32_t panel_half = (MAT_HEIGHT >> 1);
    const uint32_t count_half = (PIXEL_COUNT >> 1);
    uint32_t pixel_n = y*MAT_WIDTH + x;
    if (y < panel_half) {
        setPixelTop(pixel_n, clr);
    } else {
        setPixelBot((pixel_n - count_half), clr);
    }
}

int main() {

    LED_PORT = 0x2;

    for (uint16_t i = 0; i < MAT_HEIGHT; i++) {
        setPixelXY(i, i, 0x01);
        setPixelXY(i+MAT_HEIGHT, i, 0x02);
    }

    for (uint16_t i = 0; i < MAT_WIDTH; i++) {
        // y = mx+b
        uint16_t x = (MAT_WIDTH-1) - i;
        uint16_t y = (MAT_HEIGHT - 1) - (x >> 1) + 0;
        setPixelXY(x, y, 0x3);
    }

    MTIME_REG = 0;
    MTIME_REGH = 0;
    MTIME_CMP = 500;
    // MTIME_CMP = 5;
    MTIME_CMPH = 0;
    MTIME_PSC = 24999;
    // MTIME_PSC = 2;
    sei();
    MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x1; /* enable the timer */

  while (1) {
      /* do nothing */
  }
}
