#include "matrix.h"

uint16_t coordToPixel(uint16_t x, uint16_t y) {
    uint16_t x_idx = x/PANEL_W;
    uint16_t y_idx = y/PANEL_H;

    uint16_t pnl_x = x % PANEL_W;
    uint16_t pnl_y = y % PANEL_H;

    uint16_t panel_n = y_idx*PANEL_CNT_X + x_idx;
    uint8_t isUpsideDown = panel_n&0x1;
    /*
     * 1) get Panel number
     * 2) check if panel is upside down
     * 3) figure out if coordinate is in top or bottom half of the panel
     * 4) return pixel address
     */

    return 0;
}
