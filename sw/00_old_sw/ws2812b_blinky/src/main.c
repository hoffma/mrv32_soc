#include <stdint.h>

typedef uint32_t u32;

extern void ISR(void);
extern int main(void);

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define WS_BASE     (0x90000000)
#define WS_REG      (*(volatile uint32_t *) (WS_BASE + 0x00))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__("li a1, 128\n\t"
            "csrw mie, a1\n\t"
            "li a1, 8\n\t"
            "csrw mstatus, a1\n\t");
}

void ISR(void) {
    uint32_t tmp = LED_PORT;
    tmp++;
    LED_PORT = tmp;
    switch (tmp&0x3) {
        case 0:
            WS_REG = 0x050000;
            break;
        case 1:
            WS_REG = 0x000500;
            break;
        case 2:
            WS_REG = 0x000005;
            break;
        case 3:
            WS_REG = 0x050005;
            break;
    }
}

int main() {
    WS_REG = 0x000500;
    LED_PORT = 0x2;

    MTIME_REG = 0;
    MTIME_REGH = 0;
    MTIME_CMP = 500;
    // MTIME_CMP = 5;
    MTIME_CMPH = 0;
    MTIME_PSC = 24999;
    // MTIME_PSC = 2;
    sei();
    MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x1; /* enable the timer */

  while (1) {
      /* do nothing */
  }
}
