#include <stdint.h>

typedef uint32_t u32;

extern void ISR(void);
extern int main(void);

#define MAT_WIDTH (64)
#define MAT_HEIGHT (32)
#define MAT_CHAIN (1)
#define PIXEL_COUNT (MAT_WIDTH*MAT_HEIGHT*MAT_CHAIN)

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define MAT_BASE     (0x90000000)
#define MAT_TOP_REG      (*(volatile uint32_t *) (MAT_BASE + 0x00))
#define MAT_BOT_REG      (*(volatile uint32_t *) (MAT_BASE + 0x04))
#define MAT_REG      (*(volatile uint32_t *) (MAT_BASE + 0x08))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__("li a1, 128\n\t"
            "csrw mie, a1\n\t"
            "li a1, 8\n\t"
            "csrw mstatus, a1\n\t");
}

void ISR(void) {
    uint32_t tmp = LED_PORT;
    tmp++;
    LED_PORT = tmp;
}

void setPixel(uint16_t n, uint16_t clr) {
    uint32_t data = 0;

    data |= (n << 16); // set address
    data |= ((clr&0x7) << 8) | (clr&0x7); // set color
    MAT_REG = data;
}

void setPixelTop(uint16_t n, uint16_t clr) {
    uint32_t data = 0;

    data |= (n << 16); // set address
    data |= (clr&0x7); // set color
    MAT_TOP_REG = data;
}

void setPixelBot(uint16_t n, uint16_t clr) {
    uint32_t data = 0;
    data |= (clr&0x7); // set color
    data <<= 8;
    data |= (n << 16); // set address
    MAT_BOT_REG = data;
}

int main() {

    LED_PORT = 0x2;

    for (int i = 0; i < 64; i++) {
        // setPixelTop(128+i, (i&0x7));
        // setPixelBot(128+i, ((i+1)&0x7));
        setPixelTop(512+i, ((i+0)&0x7));
        setPixelTop(256+i, ((i+1)&0x7));
        setPixelTop(128+i, ((i+2)&0x7));

        setPixelBot(512+i, ((i+0)&0x3));
        setPixelBot(256+i, ((i+1)&0x3));
        setPixelBot(128+i, ((i+2)&0x3));
    }

    MTIME_REG = 0;
    MTIME_REGH = 0;
    MTIME_CMP = 500;
    // MTIME_CMP = 5;
    MTIME_CMPH = 0;
    MTIME_PSC = 24999;
    // MTIME_PSC = 2;
    sei();
    MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x1; /* enable the timer */

  while (1) {
      /* do nothing */
  }
}
