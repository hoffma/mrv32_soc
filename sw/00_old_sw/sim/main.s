# # zero-initialize register file
# addi x1, zero, 0
# # addi x2, zero, 0 # stack pointer is initialized somehwere else
# addi a0, zero, 0
# addi a1, zero, 0
# addi a2, zero, 0
# 
# 
# li t0, 0xff000000 # write signature address
# li t1, 10
# 
# 
# li ra, 0x3114 # signature start
# lui s5, 0x400
# 
# loop:
# addi t1, t1, -1
# sw t1, 0(t0)
# bne t1, x0, loop
# 
# 
# fin:
#     nop 
# j fin

start:
init_Mtvec:
la s0, trap_handler
csrrw s0, mtvec, s0

# init uart address
li t0, 0x01100000
li t1, 0x2
sw t1, 0x8(t0)
# send an 'a'
li t1, 61
sw t1, 0x0(t0)

# tmp_reg address
li t0, 0x01000000
# mtime base address
li t1, 0xfffff000
nop

# clear tmp_reg, mtime and mtimecmp
sw zero, 0(t0)
# mtime
sw zero, 0x0(t1)
sw zero, 0x4(t1)
# mtimecmp
sw zero, 0x8(t1)
sw zero, 0xc(t1)
# set mtimecmp
li a1, 10
sw a1, 0x8(t1)
# set prescaler
li a1, 4
sw a1, 0x10(t1)

# enable interrupts
li a1, 128
csrw mie, a1
li a1, 8
csrw mstatus, a1

# mtime ctrl, enable interrupt and free run mode
li a1, 0x3
sw a1, 0x18(t1)

#enable mtime
li a1, 1
sw a1, 0x14(t1)

loop:
    nop
    nop
    nop
    j loop

    nop
    nop

trap_handler:

# increment led
lw a1, 0(t0)
addi a1, a1, 1
sw a1, 0(t0)

sw zero, 20(t1)
sw zero, 4(t1)
sw zero, 0(t1)
li a1, 1
sw a1, 20(t1)
mret
