
start:
init_Mtvec:
la s0, trap_handler
csrrw s0, mtvec, s0

# tmp_reg address
li t0, 0x01000000
# mtime base address
li t1, 0xfffff000
nop

# clear tmp_reg, mtime and mtimecmp
sw zero, 0(t0)
# mtime
sw zero, 0x0(t1)
sw zero, 0x4(t1)
# mtimecmp
sw zero, 0x8(t1)
sw zero, 0xc(t1)
# set mtimecmp
li a1, 1000
sw a1, 0x8(t1)
# set prescaler
li a1, 24999 # 25 MHz / 25.000
#li a1, 99999 # 100 MHz / 100.000
sw a1, 0x10(t1)

# enable interrupts
li a1, 128
csrw mie, a1
li a1, 8
csrw mstatus, a1

# mtime ctrl, enable interrupt and free run mode
li a1, 0x3
sw a1, 0x18(t1)

#enable mtime
li a1, 1
sw a1, 0x14(t1)

loop:
    nop
    nop
    nop
    j loop

    nop
    nop

trap_handler:

# increment led
lw a1, 0(t0)
addi a1, a1, 1
sw a1, 0(t0)

sw zero, 20(t1)
sw zero, 4(t1)
sw zero, 0(t1)
li a1, 1
sw a1, 20(t1)
mret
