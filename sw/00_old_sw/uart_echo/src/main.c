#include <stdint.h>

typedef uint32_t u32;

extern void ISR(void);
extern int main(void);

// 25 MHz
#define F_CPU (25000000U)
#define BAUD_RATE (115200U)

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define UART_BASE (0x01100000)
#define UART_TX_PORT (*(volatile uint32_t *)(UART_BASE + 0x00))
#define UART_RX_PORT (*(volatile uint32_t *)(UART_BASE + 0x04))
#define UART_BRD_PORT (*(volatile uint32_t *)(UART_BASE + 0x08))

#define MTIME_BASE  (0xfffff000)
#define MTIME_REG   (*(volatile uint32_t *) (MTIME_BASE + 0x00))
#define MTIME_REGH  (*(volatile uint32_t *) (MTIME_BASE + 0x04))
#define MTIME_CMP   (*(volatile uint32_t *) (MTIME_BASE + 0x08))
#define MTIME_CMPH  (*(volatile uint32_t *) (MTIME_BASE + 0x0C))
#define MTIME_PSC   (*(volatile uint32_t *) (MTIME_BASE + 0x10))
#define MTIME_EN    (*(volatile uint32_t *) (MTIME_BASE + 0x14))
#define MTIME_CTRL  (*(volatile uint32_t *) (MTIME_BASE + 0x18))

void sei(void) {
    __asm__("li a1, 128\n\t"
            "csrw mie, a1\n\t"
            "li a1, 8\n\t"
            "csrw mstatus, a1\n\t");
}

void uart_set_brd(uint32_t brd);
void uart_putchar(char c);
void uart_putstr(char *s);

void ISR(void) {
    uint32_t tmp = LED_PORT;
    tmp++;
    LED_PORT = tmp;
}

void delay() {
    for (volatile int i = 0; i < 100000; i++)
        ;
}


void uart_set_brd(uint32_t brd) {
    UART_BRD_PORT = brd;
}

void uart_putchar(char c) {
    if (c == '\n')
        uart_putchar('\r');
    UART_TX_PORT = c;
}

void uart_putstr(char *s) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
}

int main() {

    LED_PORT = 0x2;
    uart_set_brd(F_CPU/BAUD_RATE);

    // MTIME_REG = 0;
    // MTIME_REGH = 0;
    // MTIME_CMP = 500;
    // MTIME_CMP = 5;
    // MTIME_CMPH = 0;
    // MTIME_PSC = 24999;
    // MTIME_PSC = 2;
    // sei();
    // MTIME_CTRL = 0x3; /* enable interrupt and free run mode */
    MTIME_EN = 0x0;
    LED_PORT++;

    while (1) {
        // delay();
        char rx = UART_RX_PORT;
        uart_putchar(rx == '\r' ? '\n' : rx);
        LED_PORT++;
    }
}
