
start:
init_Mtvec:
la s0, trap_handler
csrrw s0, mtvec, s0

# tmp_reg address
li t0, 0x01000000
nop

# clear tmp_reg, mtime and mtimecmp
sw zero, 0(t0)

# enable interrupts
li a1, 2048
csrw mie, a1
li a1, 8
csrw mstatus, a1

loop:
    nop
    nop
    nop
    j loop

    nop
    nop

trap_handler:
# increment led
lw a1, 0(t0)
addi a1, a1, 1
sw a1, 0(t0)
nop
mret
