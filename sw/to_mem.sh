#!/bin/bash
echo "library ieee;"
echo "use ieee.std_logic_1164.all;"
echo ""
echo "library work;"
echo "use work.misc.all;"
echo ""
echo "package mem_init is"
echo "    constant ROM_INIT_DATA : mem32_t(0 to (ROM_SIZE_BYTES/4-1)) := ("
sed --regexp-extended 's/([0-9a-f]*)/        x"\1",/' $1
echo "        others => (others => '0')"
echo "    );"
echo "end package mem_init;"
