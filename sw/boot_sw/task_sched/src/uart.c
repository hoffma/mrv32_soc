#include "uart.h"
#include "mystd.h"

void uart_set_brd(uint32_t brd) {
    UART0->brd = brd;
} 

void uart_send_byte(uint8_t b) {
    UART0->tx = b;
}

uint8_t uart_recv_byte(void) {
    return UART0->rx;
}

void uart_putchar(char c) {
    if (c == '\n')
        uart_putchar('\r');
    UART0->tx = c;
}

void uart_puts(char *s) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
}

[[nodiscard]] char uart_getchar(void) {
    return UART0->rx;
}
