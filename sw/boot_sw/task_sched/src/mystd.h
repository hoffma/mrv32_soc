#pragma once

#include <stdint.h>

#define F_CPU (25000000)

#define UART_BASE (0x01100000)
#define LED_BASE (0x01000000)
#define MTIME_BASE (0xfffff000)

struct uart_t {
    uint32_t tx;
    uint32_t rx;
    uint32_t brd;
};

struct led_t {
    uint32_t val;
};

struct mtime_t {
    uint64_t cnt;
    uint64_t cmp;
    uint32_t psc;
    uint32_t en;
    uint32_t ctrl;
};


extern struct uart_t * UART0;
extern struct led_t * LED0;
extern struct mtime_t * MTIME0;

void std_init(void);

