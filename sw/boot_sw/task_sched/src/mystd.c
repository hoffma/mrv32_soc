#include "mystd.h"

struct uart_t * UART0;
struct led_t * LED0;
struct mtime_t * MTIME0;

void std_init(void) {
    LED0 =  (struct led_t *) LED_BASE;
    UART0 =  (struct uart_t *) UART_BASE;
    MTIME0 =  (struct mtime_t *) MTIME_BASE;
}

