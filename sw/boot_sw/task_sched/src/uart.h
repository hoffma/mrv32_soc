#pragma once

#include <stdint.h>

void uart_set_brd(uint32_t brd);
void uart_send_byte(uint8_t b);
[[nodiscard]] uint8_t uart_recv_byte(void);
void uart_putchar(char c);
void uart_puts(char *s);
