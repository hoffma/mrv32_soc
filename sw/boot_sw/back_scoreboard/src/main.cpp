
#include <array>
#include <cstring>

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>
#include <led_matrix.h>

#include "bbc_master.h"

    extern void ISR(void);
    extern int main(void);
}

constexpr auto BAUD_RATE_U0{1000000};
constexpr auto BAUD_RATE_U1{115200};
// volatile bool print_state_flag = false;

constexpr auto FONT_WIDTH{8};
constexpr auto FONT_HEIGHT{8};
constexpr auto FONT_SCALE{2};

constexpr auto MAX_SECONDS{300};

struct state_t {
    uint32_t seconds;
    uint32_t scoreb;
    uint32_t scoreg;
    uint8_t enabled;
};

void ISR(void) {
    // print_state_flag = true;
}

template<typename T>
T abs(T &n) { return (n < 0) ? -n : n; }

char buf[16];
void print_int32(uart_t *dev, int32_t n) {
    int i = 0;
    if (0 == n) dev->tx = '0';
    int32_t num = abs(n);

    while (num > 0) {
        int32_t tmp = num%10;
        buf[i] = tmp;
        num = num/10;
        i++;
    }
    while (i > 0) {
        i--;
        uart_putchar(dev, '0' + buf[i]);
    }
}

void print_state(uart_t *dev, const state_t &state) {
    uart_puts(dev, (char *)"{\n\t\"seconds\": ");
    print_int32(dev, state.seconds);
    uart_puts(dev, (char *)",\n\t\"scoreb\": ");
    print_int32(dev, state.scoreb);
    uart_puts(dev, (char *)",\n\t\"scoreg\": ");
    print_int32(dev, state.scoreg);
    uart_puts(dev, (char *)",\n\t\"enabled\": ");
    uart_puts(dev, state.enabled ? (char *)"true" : (char *)"false");
    uart_puts(dev, (char *)"\n}\n");
}

uint8_t get_byte(uart_t *dev, uint8_t *byte_o) {
    if (uart_rx_valid(dev)) {
        *byte_o = uart_recv_nb(dev);
        return 0;
    }
    return 1;
}

enum class recv_state_t {
    START_BYTE,
    PAYLOAD,
    CHECKSUM,
    END_BYTE
};

inline bool is_start_byte(uint8_t b) {
    return b == 0xde;
}

int draw_ch(uint32_t x, uint32_t y, char c, uint32_t clr, uint32_t font_scale = 1) {
    int res = 0;
    uint32_t start = c - 32;

    for (auto j = 0; j < (FONT_HEIGHT*font_scale); ++j) {
        uint32_t tmp = start*FONT_HEIGHT+j/font_scale;
        char b = font_Acorn_BBC_Master[tmp];
        for (auto i = 0; i < (FONT_WIDTH*font_scale); ++i) {
            uint32_t tmpx = x+(FONT_WIDTH*font_scale-1-i);
            // uint32_t tmpy = y+(FONT_HEIGHT*FONT_SCALE-1-j);
            // uint32_t tmpx = x+i;
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> (i/font_scale))&0x1) ? clr : 0;
            res = matrix_pixel_xy(tmpx, tmpy, c_val);
            if (res < 0) return res; /* stops if there was an out of bounds pixel  */
        }
    }
    return 0;
}

int draw_dig(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    int res = 0;
    char dig = '0'+(n%10);

    draw_ch(x, y, dig, clr);
    if (res < 0) return res;

    return 0;
}

int draw_num(uint32_t x, uint32_t y, uint8_t n, uint32_t clr, uint32_t font_scale = 1) {
    int res = 0;
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    draw_ch(x, y, tenth, clr, font_scale);
    if (res < 0) return res;
    draw_ch(x+FONT_WIDTH*font_scale, y, dig, clr, font_scale);
    if (res < 0) return res;

    return 0;
}

void draw_score(const state_t &sb, uint32_t x, uint32_t y, uint32_t font_scale = 1) {
    // draw_num(12, 16, sb.scoreg, 0x2);
    // draw_ch(28, 16, ':', 0x1);
    // draw_num(36, 16, sb.scoreb, 0x4);
    uint32_t x_offset = (FONT_WIDTH-1)*font_scale;
    if ((sb.scoreb > 9) || (sb.scoreg > 9)) {
        draw_num(x, y, sb.scoreg, 0x2, font_scale);
        draw_ch(x + 2*x_offset, y, ':', 0x1, font_scale);
        draw_num(x + 3*x_offset, y, sb.scoreb, 0x4, font_scale);
    } else {
        draw_ch(x, y, '0'+sb.scoreg, 0x2, font_scale);
        draw_ch(x + 1*x_offset, y, ':', 0x1, font_scale);
        draw_ch(x + 2*x_offset, y, '0'+sb.scoreb, 0x4, font_scale);
    }
}

void draw_time(const state_t &sb, uint32_t x, uint32_t y, uint8_t clr, uint32_t font_scale = 1) {
    uint32_t minutes = (sb.seconds/60%10); /* only allow single digit minutes */
    uint32_t seconds = sb.seconds%60;

    uint32_t x_offset = (FONT_WIDTH-1)*font_scale;
    // draw_num(x, y, minutes, 0x6, font_scale);
    draw_ch(x, y, '0'+minutes, clr, font_scale);
    draw_ch(x + 1*x_offset, y, ':', clr, font_scale);
    draw_num(x + 2*x_offset, y, seconds, clr, font_scale);
}

void draw_line(const state_t &sb, uint32_t x1, uint32_t y, uint32_t height, uint32_t x2, uint8_t clr) {
    for (auto i = x1; i < x2; ++i) {
        for (auto j = 0; j < height; ++j) {
            matrix_pixel_xy(i, y+j, clr);
        }
    }
}

void draw_progress(const state_t &sb, uint32_t y, uint32_t h, uint8_t clr) {
        auto prog = (192*sb.seconds)/MAX_SECONDS;
        prog = prog > 192? 192: prog;
        int i = 0;
        draw_line(sb, 0, y, h, prog, clr);
        draw_line(sb, prog, y, h, 192, 0x7);
}

void draw_time_big(const state_t &sb) {
    // matrix_clear();
    draw_score(sb, 9, 40, 5);
    if (sb.enabled) {
        draw_time(sb, 4, 8, 0x6, 4);
        draw_progress(sb, 0, 4, 0x1);
    } else {
        draw_time(sb, 4, 8, 0x5, 4);
        draw_progress(sb, 0, 4, 0x3);
    }
}

void draw_score_big(const state_t &sb) {
    // matrix_clear();
    for(int i = 0; i < 192; ++i) {
        matrix_pixel_xy(i, 0, 0x3);
        matrix_pixel_xy(i, 1, 0x3);
        matrix_pixel_xy(i, 33, 0x3);
        matrix_pixel_xy(i, 34, 0x3);
    }
    draw_score(sb, 4, 6, 3);
    draw_time(sb, 24, 40, 2);
}

bool is_equal(const state_t &sb1, const state_t &sb2) {
    bool eq = true;

    eq &= (sb1.seconds == sb2.seconds);
    eq &= (sb1.scoreb == sb2.scoreg);
    eq &= (sb1.scoreg == sb2.scoreg);
    eq &= (sb1.enabled == sb2.enabled);

    return eq;
}

// uint16_t fletcher16(uint32_t *data_start, uint32_t *data_end)
uint16_t fletcher16(const std::array<uint8_t, sizeof(state_t)> &arr)
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    auto len = arr.size();

    for (uint32_t i = 0; i < len; ++i) {
        uint16_t tmp = arr[i]&0xff;
        sum1 = (sum1 + tmp) % 255;
        sum2 = (sum2 + sum1) % 255;
    }

    return (sum2 << 8) | sum1;
}


int main(void) {
    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    uart_set_brd(UART1, (F_CPU/BAUD_RATE_U1));
    matrix_init(64, 32, 12, 4);
    // matrix_init(64, 32, 4, 2);

    bool refresh_state = false;
    state_t scoreboard_state{0, 0, 0, false};
    state_t scoreboard_in{0, 0, 0, false};
    recv_state_t curr_state{recv_state_t::START_BYTE};
    
    matrix_clear();

    LED0->val= auto(1);

    // MTIME0->en = 0;
    // MTIME0->cnt = 0;
    // MTIME0->cmp = 999;
    // MTIME0->psc = 99999;

    // sei();
    // MTIME0->ctrl = 0x3;
    // MTIME0->en = 1;
    cli();

    std::array<uint8_t, sizeof(state_t)> payload_in{0};
    std::array<uint8_t, sizeof(uint16_t)> checksum_arr{0};
    std::array<uint8_t, sizeof(state_t)> payload_cpy{0};
    bool paket_valid{false};
    uint8_t byte_cnt{0};
    uint8_t checksum_cnt{0};
    // dont make them equal from the start
    uint16_t chksum_calc{0};
    uint16_t chksum_in{0};

    while (1) {
        if (refresh_state) {
            draw_time_big(scoreboard_state);
            // draw_progress(scoreboard_state);
            refresh_state = false;
        }
        uint8_t val = 0;
        auto v = get_byte(UART1, &val);
        LED0->val = 0;
        if (v == 0) {
            // TODO: send fletcher checksum too and verify
            switch(curr_state) {
                case recv_state_t::START_BYTE:
                    LED0->val = 1;
                    byte_cnt = 0;
                    checksum_cnt = 0;
                    curr_state = (val == 0xde) ? 
                        recv_state_t::PAYLOAD : recv_state_t::START_BYTE;
                    break;
                case recv_state_t::PAYLOAD:
                    LED0->val = 2;
                    payload_in[byte_cnt] = val;
                    byte_cnt++;

                    curr_state = (byte_cnt >= sizeof(state_t)) ?
                        recv_state_t::CHECKSUM : recv_state_t::PAYLOAD;
                    break;
                case recv_state_t::CHECKSUM:
                    LED0->val=3;
                    checksum_arr[checksum_cnt] = val;
                    checksum_cnt++;

                    curr_state = (checksum_cnt >= sizeof(uint16_t)) ?
                        recv_state_t::END_BYTE : recv_state_t::CHECKSUM;
                    break;
                case recv_state_t::END_BYTE:
                    LED0->val = 4;
                    paket_valid = (val == 0xad);
                    curr_state = recv_state_t::START_BYTE;
                    break;
                default:
                    curr_state = recv_state_t::START_BYTE;
            }
        }
        if (paket_valid) {
            chksum_calc = 123; /* dummy value */
            chksum_in = 0;
            // memcpy(&payload_cpy, &payload_in, sizeof(state_t));
            scoreboard_in = *reinterpret_cast<state_t *>(&payload_in);
            chksum_in = *reinterpret_cast<uint16_t *>(&checksum_arr);
            chksum_calc = fletcher16(payload_in);
            if (chksum_calc == chksum_in) {
                if (!is_equal(scoreboard_state, scoreboard_in)) {
                    scoreboard_state = state_t{scoreboard_in};
                    refresh_state = true;
                }
            }
            paket_valid = false;
        }
        __asm__("nop");
    }

    return 1;
}
