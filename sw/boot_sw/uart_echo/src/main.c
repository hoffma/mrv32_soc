#include <stdint.h>

#include "prog_loader.h"

extern void ISR(void);
extern int main(void);

#define F_CPU (25000000)
#define BAUD_RATE (19200)
// #define BAUD_RATE (1000000)

#define SRAM_BASE (0x81000000)
#define SRAM_BASE_ADDR ((uint32_t *) (SRAM_BASE))
#define SRAM_PORT (*(uint32_t *)(SRAM_BASE))

#define LED_BASE (0x01000000)
#define LED_PORT (*(volatile uint32_t *)(LED_BASE))

#define UART_BASE (0x01100000)
#define UART_TX_PORT (*(volatile uint32_t *)(UART_BASE + 0x00))
#define UART_RX_PORT (*(volatile uint32_t *)(UART_BASE + 0x04))
#define UART_BRD_PORT (*(volatile uint32_t *)(UART_BASE + 0x08))

void sei(void) {
    __asm__("li a1, 128\n\t"
            "csrw mie, a1\n\t"
            "li a1, 8\n\t"
            "csrw mstatus, a1\n\t");
}

void uart_set_brd(uint32_t brd);
void uart_putchar(char c);
void uart_putstr(char *s);
uint8_t recv_byte(void);
void write_byte(uint8_t b);
void write_mem(uint32_t w);

void ISR(void) {}

void short_delay() {
    for (volatile int i = 0; i < 5; i++)
        ;
}

void delay() {
    for (volatile int i = 0; i < 500000; i++)
        ;
}

uint8_t recv_byte(void) {
    return (uint8_t) UART_RX_PORT;
}

void write_byte(uint8_t b) {
    UART_TX_PORT = b;
}

uint32_t *write_addr = SRAM_BASE_ADDR;

void write_mem(uint32_t w) {
    *write_addr = w;
    write_addr++;
}

void uart_set_brd(uint32_t brd) {
    UART_BRD_PORT = brd;
}

void uart_putchar(char c) {
    if (c == '\n')
        uart_putchar('\r');
    UART_TX_PORT = c;
}

void uart_putstr(char *s) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
}

uint16_t fletcher16(uint32_t *data_start, uint32_t *data_end)
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    uint32_t *data = data_start;

    while (data < data_end) {
        uint32_t word = *data;
        for (uint32_t i = 0; i < 4; ++i) {
            uint16_t tmp = (word >> (i*8))&0xff;
            sum1 = (sum1 + tmp) % 255;
            sum2 = (sum2 + sum1) % 255;
        }
        data++;
    }

    return (sum2 << 8) | sum1;
}

int main() {
    uint16_t chksum_in = 0;
    uint16_t chksum_calc = 0;

    LED_PORT = 0x01;

    write_addr = SRAM_BASE_ADDR;
    uart_set_brd(F_CPU/BAUD_RATE - 1);


    while (1) {
        echo_word(recv_byte, write_byte);
    }
}
