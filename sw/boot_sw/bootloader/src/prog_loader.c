#include "prog_loader.h"

uint32_t get_word(uint8_t (recv_byte)(void));
uint16_t get_half_word(uint8_t (recv_byte)(void));

uint32_t get_word(uint8_t (recv_byte)(void)) {
    uint32_t tmp_word = 0;
    for (uint32_t i = 0; i < 4; ++i) {
        uint32_t b = ((uint32_t) recv_byte())&0xff;
        tmp_word |= (uint32_t)(b << (i*8));
    }
    return tmp_word;
}

uint16_t get_half_word(uint8_t (recv_byte)(void)) {
    uint16_t tmp_word = 0;
    for (uint32_t i = 0; i < 2; ++i) {
        uint16_t b = ((uint16_t) recv_byte())&0xff;
        tmp_word |= (uint16_t)(b << (i*8));
    }
    return tmp_word;
}

void send_word(uint32_t w, void (write_byte)(uint8_t)) {
    write_byte((w >> 0)&0xff);
    write_byte((w >> 8)&0xff);
    write_byte((w >> 16)&0xff);
    write_byte((w >> 24)&0xff);
}

void send_half_word(uint16_t hw, void (write_byte)(uint8_t)) {
    write_byte((hw >> 0)&0xff);
    write_byte((hw >> 8)&0xff);
}

/* echos all received word until a word with 'val' was received */
uint32_t echo_word(uint32_t val, uint8_t (recv_byte)(void), void (write_byte)(uint8_t)) {
    uint32_t w = 0;

    do {
        w = get_word(recv_byte);
        send_word(w, write_byte);
    } while (w != val);

    return w;
}

uint16_t loader_load(
        uint8_t (recv_byte)(void),
        void (write_byte)(uint8_t),
        void (write_mem)(uint32_t)
) {
    // uint32_t recvd_len = 0;
    // uint16_t recvd_checksum = 0;

    // get length
    uint32_t recvd_len = get_word(recv_byte);
    send_word(recvd_len, write_byte);
    uint16_t recvd_checksum = get_half_word(recv_byte);
    send_half_word(recvd_checksum, write_byte);

    while (recvd_len > 0) {
        uint32_t tmp_word = 0;
        uint32_t w = get_word(recv_byte);
        // send_word(w, write_byte);
        write_mem(w);
        recvd_len = recvd_len - 4;
    }
    return recvd_checksum;
}
