#include "bootloader.h"
extern "C" {
#include "prog_loader.h"
}

#define SRAM_BASE (0x81000000)
#define SRAM_BASE_ADDR ((uint32_t *) (SRAM_BASE))
#define SRAM_PORT (*(uint32_t *)(SRAM_BASE))

#define BAUD_RATE (19200)

uint32_t *write_addr = SRAM_BASE_ADDR;

void short_delay() {
    for (volatile int i = 0; i < 5; i++)
        ;
}

uint8_t recv_byte(void) {
    return uart_recv_byte(UART0);
}

void write_byte(uint8_t b) {
    uart_send_byte(UART0, b);
}

void write_mem(uint32_t w) {
    *write_addr = w;
    write_addr++;
}

uint16_t fletcher16(uint32_t *data_start, uint32_t *data_end)
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    uint32_t *data = data_start;

    while (data < data_end) {
        uint32_t word = *data;
        for (uint32_t i = 0; i < 4; ++i) {
            uint16_t tmp = (word >> (i*8))&0xff;
            sum1 = (sum1 + tmp) % 255;
            sum2 = (sum2 + sum1) % 255;
        }
        data++;
    }

    return (sum2 << 8) | sum1;
}

void bootloader_run() {
    uint16_t chksum_in = 0;
    uint16_t chksum_calc = 0;

    
    write_addr = SRAM_BASE_ADDR;
    uart_set_brd(UART0, F_CPU/BAUD_RATE);

    uint32_t val = 0xaa55aa55;
    uint32_t ret = echo_word(val, recv_byte, write_byte);

    LED0->val = ret;

    chksum_in = loader_load(recv_byte, write_byte, write_mem);
    LED0->val = 0x03;
    chksum_calc = fletcher16((uint32_t *) SRAM_BASE_ADDR, write_addr);
    // chksum_calc = chksum_in;
    LED0->val = 0x04;
    short_delay();
    write_byte(chksum_calc);
    write_byte((chksum_calc >> 8));
    LED0->val = 0x05;

    if (chksum_in == chksum_calc) {
        __asm__("la a1, 0x81000000\n\t"
                "jr a1\n\t");
    }
}
