#pragma once

#include <stdint.h>

uint16_t loader_load(uint8_t (recv_byte)(void), void (write_byte)(uint8_t), void (write_mem)(uint32_t));
uint32_t echo_word(uint32_t val, uint8_t (recv_byte)(void), void (write_byte)(uint8_t));
