#pragma once

#include <stdint.h>

class Player {
    public:
        Player(uint8_t x, uint8_t y) 
            : _pos_x(x), _pos_y(y), _score(0) {}
        uint8_t _pos_x;
        uint8_t _pos_y;
        uint8_t _score;
};
