
#include "pong.h"
extern "C" {
#include <mystd.h>
}

void Pong::drawBoard() const {
    for(auto i = 0; i < _BOARD_X; ++i) {
        // top
        matrix_pixel_xy(_board_x_off+i, _board_y_off-3, 0x7);
        matrix_pixel_xy(_board_x_off+i, _board_y_off-2, 0x7);
        matrix_pixel_xy(_board_x_off+i, _board_y_off-1, 0x7);
        // bot
        matrix_pixel_xy(_board_x_off+i, _BOARD_Y+_board_y_off+2, 0x7);
        matrix_pixel_xy(_board_x_off+i, _BOARD_Y+_board_y_off+1, 0x7);
        matrix_pixel_xy(_board_x_off+i, _BOARD_Y+_board_y_off+0, 0x7);
    }
    for(auto i = -3; i < _BOARD_Y+3; ++i) {
        // left
        matrix_pixel_xy(_board_x_off-1, _board_y_off+i, 0x7);
        matrix_pixel_xy(_board_x_off-2, _board_y_off+i, 0x7);
        matrix_pixel_xy(_board_x_off-3, _board_y_off+i, 0x7);
        // right
        matrix_pixel_xy(_BOARD_X+_board_x_off+1-1, _board_y_off+i, 0x7);
        matrix_pixel_xy(_BOARD_X+_board_x_off+2-1, _board_y_off+i, 0x7);
        matrix_pixel_xy(_BOARD_X+_board_x_off+3-1, _board_y_off+i, 0x7);
    }
}

void Pong::drawPaddle(const Player &pl, uint8_t clr) const {
    auto start_y = (pl._pos_y+_board_y_off);
    auto start_x = (pl._pos_x+_board_x_off);
    for (auto j = start_y; j < (start_y+_PADDLE_LEN); ++j) {
        for (auto i = start_x; i < (start_x+_PADDLE_WIDTH); ++i) {
            matrix_pixel_xy(i, j, clr);
        }
    }
}

void Pong::clearPaddle(const Player &pl) const {
    drawPaddle(pl, 0x0);
}

void Pong::updatePaddles() {
    static bool pl1_up = false, pl2_up = true;

    clearPaddle(_pl1);
    clearPaddle(_pl2);
    _pl1._pos_y = ENC_VAL;
    LED0->val = ENC_VAL;
    // if (pl1_up) {
    //     _pl1._pos_y--;
    //     if (_pl1._pos_y == 0) {
    //         pl1_up = false;
    //     }
    // } else {
    //     _pl1._pos_y++;
    //     if (_pl1._pos_y == (_BOARD_Y-_PADDLE_LEN-1)) {
    //         pl1_up = true;
    //     }
    // }
    if (pl2_up) {
        _pl2._pos_y--;
        if (_pl2._pos_y == 0) {
            pl2_up = false;
        }
    } else {
        _pl2._pos_y++;
        if (_pl2._pos_y == (_BOARD_Y-_PADDLE_LEN-1)) {
            pl2_up = true;
        }
    }
    drawPaddle(_pl1, 0x2);
    drawPaddle(_pl2, 0x4);
}
