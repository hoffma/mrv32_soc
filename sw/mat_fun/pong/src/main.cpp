#include <cstring>
#include "fixed.h"

#include "pong.h"

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>
#include <led_matrix.h>

    extern void ISR(void);
    extern int main(void);
}

using namespace numeric;
typedef fixed<24, 8> f16;

constexpr auto BAUD_RATE_U0{1000000};

constexpr auto MAT_WIDTH{192};
constexpr auto MAT_HEIGHT{128};

volatile bool tick{false};

void ISR(void) {
    tick = true;
}

template<typename T>
T abs(T n) { return (n < 0) ? -n : n; }

int main() {
    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    cli();

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 499;
    MTIME0->psc = 9999;

    sei();
    MTIME0->ctrl = 0x3;
    MTIME0->en = 1;

    Pong p{};
    p.drawBoard();
    p.updatePaddles();

    while(1) {
        if (tick) {
            p.updatePaddles();
            tick = false;
        }
        __asm__("nop");
    }

    return 1;
}
