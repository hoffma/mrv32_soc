#pragma once

#include <stdint.h>
#include "player.h"
extern "C" {
#include <led_matrix.h>
}
// #include "player.h"


#define ENC_BASE (0x01000200)
#define ENC_VAL (*(volatile uint32_t *)(ENC_BASE + 0x00))
#define ENC_LIMIT (*(volatile uint32_t *)(ENC_BASE + 0x04))
#define ENC_RST (*(volatile uint32_t *)(ENC_BASE + 0x08))

enum class paddle_t {
    PLAYER1,
    PLAYER2
};

class Pong {
    public:
        Pong() 
            : _pl1(5, 45), _pl2(135-_PADDLE_WIDTH, 45) 
        {
            matrix_init(64, 32, 12, 4);
            matrix_clear();
            _board_x_off = (192 - _BOARD_X)/2;
            _board_y_off = (128 - _BOARD_Y)/2;
            ENC_LIMIT = _BOARD_Y - _PADDLE_LEN;
        }

        void SetP1Rel(int y) {
        }

        void drawBoard() const;
        void drawPaddle(const Player &pl, uint8_t clr) const;
        void clearPaddle(const Player &pl) const;
        void updatePaddles();
    private:
        int _board_x_off;
        int _board_y_off;
        const uint8_t _BOARD_X = 140;
        const uint8_t _BOARD_Y = 105;
        const uint8_t _PADDLE_LEN = 15;
        const uint8_t _PADDLE_WIDTH = 3;
        const uint8_t _MAX_Y = (_BOARD_Y-_PADDLE_LEN+1);
        Player _pl1;
        Player _pl2;

};
