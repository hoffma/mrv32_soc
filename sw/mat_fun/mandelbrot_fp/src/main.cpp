#include <cstring>

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>
#include <led_matrix.h>

    extern void ISR(void);
    extern int main(void);
}

constexpr auto BAUD_RATE_U0{1000000};

constexpr auto MAT_WIDTH{192};
constexpr auto MAT_HEIGHT{128};
constexpr auto SHIFT_AMOUNT{28};
constexpr auto SHIFT_MASK{(1 << SHIFT_AMOUNT) - 1};

void ISR(void) {
    /* nothing */
}

void draw_mandelbrot() {
    int64_t i;
    int64_t re, im, nre, nim;
    double cre, cim;
    int64_t fp_cre, fp_cim;
    int64_t w = MAT_WIDTH;
    int64_t h = MAT_HEIGHT;
    int64_t mul = (1 << SHIFT_AMOUNT);
    int64_t tmp_re, tmp_im;
    int64_t sum;

    for(int j = 0; j < MAT_HEIGHT; j++) { /* y */
        for(int k = 0; k < MAT_WIDTH; k++) { /* x */
            // cre = (((double) k)/((double) w) - 0.5)*3.0 - 0.7;
            cre = (((double) k)/((double) w) - 0.5)*1.6875 - 0.7;
            cim = (((double) j)/((double) h) - 0.5)*1.2875;
            // cim = (((double) j)/((double) h) - 0.5)*1.6875;
            // cim = (((double) j)/((double) h) - 0.5)*3.0;

            fp_cre = (int64_t)(cre * mul);
            fp_cim = (int64_t)(cim * mul);

            re = (int64_t)(0.0 * mul);
            im = (int64_t)(0.0 * mul);
            i = 0;
            sum = 0;
            int iter = 1;


            while((i < 256) && (sum < (4 << SHIFT_AMOUNT))) {
                tmp_re = (re*re) >> SHIFT_AMOUNT;
                tmp_im = (im*im) >> SHIFT_AMOUNT;
                nre = (tmp_re - tmp_im);
                nre += fp_cre;
                nim = (re*im) >> SHIFT_AMOUNT;
                nim *= 2;
                nim += fp_cim;

                tmp_re = ((nre*nre) >> SHIFT_AMOUNT);
                tmp_im = ((nim*nim) >> SHIFT_AMOUNT);
                sum = (tmp_re + tmp_im);

                re = nre;
                im = nim;
                i++;
            }

            if(i < 10) {
                matrix_pixel_xy(k, j, 0x0);
            } else if(i < 20) {
                matrix_pixel_xy(k, j, 0x6);
            } else if(i < 40) {
                matrix_pixel_xy(k, j, 0x5);
            } else if(i < 80) {
                matrix_pixel_xy(k, j, 0x2);
            } else if(i < 160) {
                matrix_pixel_xy(k, j, 0x1);
            } else if(i < 240) {
                matrix_pixel_xy(k, j, 0x3);
            } else {
                matrix_pixel_xy(k, j, 0x4);
            }
        }
    }
}

int main() {
    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    matrix_init(64, 32, 12, 4);

    matrix_clear();
    cli();
    draw_mandelbrot();

    while(1) {
        __asm__("nop");
    }

    return 1;
}
