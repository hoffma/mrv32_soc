
#include <cstring>

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>
#include <led_matrix.h>

#include "bbc_master.h"

    extern void ISR(void);
    extern int main(void);
}

constexpr auto BAUD_RATE_U0{1000000};
constexpr auto BAUD_RATE_U1{115200};
// volatile bool print_state_flag = false;
constexpr auto FONT_WIDTH{8};
constexpr auto FONT_HEIGHT{8};
constexpr auto FONT_SCALE{3};

void ISR(void) {
    // print_state_flag = true;
}

template<typename T>
T abs(T &n) { return (n < 0) ? -n : n; }

char buf[16];
void print_int32(uart_t *dev, int32_t n) {
    int i = 0;
    if (0 == n) dev->tx = '0';
    int32_t num = abs(n);

    while (num > 0) {
        int32_t tmp = num%10;
        buf[i] = tmp;
        num = num/10;
        i++;
    }
    while (i > 0) {
        i--;
        uart_putchar(dev, '0' + buf[i]);
    }
}

uint8_t get_byte(uart_t *dev, uint8_t *byte_o) {
    if (uart_rx_valid(dev)) {
        *byte_o = uart_recv_nb(dev);
        return 0;
    }
    return 1;
}

int draw_ch(uint32_t x, uint32_t y, char c, uint32_t clr, uint32_t font_scale = 1) {
    int res = 0;
    uint32_t start = c - 32;

    for (auto j = 0; j < (FONT_HEIGHT*font_scale); ++j) {
        uint32_t tmp = start*FONT_HEIGHT+j/font_scale;
        char b = font_Acorn_BBC_Master[tmp];
        for (auto i = 0; i < (FONT_WIDTH*font_scale); ++i) {
            uint32_t tmpx = x+(FONT_WIDTH*font_scale-1-i);
            // uint32_t tmpy = y+(FONT_HEIGHT*FONT_SCALE-1-j);
            // uint32_t tmpx = x+i;
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> (i/font_scale))&0x1) ? clr : 0;
            res = matrix_pixel_xy(tmpx, tmpy, c_val);
            if (res < 0) return res; /* stops if there was an out of bounds pixel  */
        }
    }
    return 0;
}

int draw_dig(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    int res = 0;
    char dig = '0'+(n%10);

    draw_ch(x, y, dig, clr);
    if (res < 0) return res;

    return 0;
}

int draw_num(uint32_t x, uint32_t y, uint8_t n, uint32_t clr, uint32_t font_scale = 1) {
    int res = 0;
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    draw_ch(x, y, tenth, clr, font_scale);
    if (res < 0) return res;
    draw_ch(x+FONT_WIDTH*font_scale, y, dig, clr, font_scale);
    if (res < 0) return res;

    return 0;
}

int main(void) {
    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    // uart_set_brd(UART1, (F_CPU/BAUD_RATE_U1));
    matrix_init(64, 32, 6, 3);
    // matrix_init(64, 32, 4, 2);

    matrix_clear();

    LED0->val= auto(2);
    cli();

    // matrix_fill(0x3);
    // matrix_set(0, 0x1);
    // matrix_set(4, 0x2);
    // for (int i = 0; i < 7*128; i+=128) {
    //     matrix_set(256+i, 0x0506);
    //     matrix_set(256+i+2, 0x0605);
    // }

    for (int i = 0; i < 5; ++i) {
        draw_ch(i*(FONT_SCALE*FONT_WIDTH), 0, '0'+i, i+1, FONT_SCALE);
    }
    for (int i = 0; i < 5; ++i) {
        draw_ch(i*(FONT_SCALE*FONT_WIDTH), 32, '0'+i+5, 7-i, FONT_SCALE);
    }
    for (int i = 0; i < 5; ++i) {
        draw_ch(i*(FONT_SCALE*FONT_WIDTH), 64, '0'+i, 7-i, FONT_SCALE);
    }

    matrix_pixel_xy(0, 0, 0x1);
    matrix_pixel_xy(127, 0, 0x2);
    matrix_pixel_xy(64, 95, 0x4);
    while (1) {
        __asm__("nop");
    }

    return 1;
}
