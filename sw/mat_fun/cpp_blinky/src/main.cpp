

extern "C" {
#include <mystd.h>

    extern void ISR(void);
    extern int main(void);
}

#define ENC_BASE (0x01000200)
#define ENC_VAL (*(volatile uint32_t *)(ENC_BASE + 0x00))
#define ENC_LIMIT (*(volatile uint32_t *)(ENC_BASE + 0x04))

void ISR(void) {
    LED0->val++;
    // LED0->val = ENC_VAL;
}

int main(void) {
    std_init();

    LED0->val= auto(1);

    ENC_LIMIT = 100;
    ENC_VAL = 10;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 9999;
    MTIME0->psc = 9999;

    sei();
    MTIME0->ctrl = 0x3;
    MTIME0->en = 1;

    while (1) {
        // nothing
    }

    return 1;
}
