#pragma once
#include <array>
#include "mat.h"

constexpr auto PNL_W{64};
constexpr auto PNL_H{32};
constexpr auto PNL_C{12};
constexpr auto PNL_ROWS{4};
constexpr auto PNL_COLS{PNL_C/PNL_ROWS};
constexpr auto MAT_WIDTH{PNL_W*PNL_COLS};
constexpr auto MAT_HEIGHT{PNL_H*PNL_ROWS};


class Scoreboard {
struct state_t {
    uint32_t seconds;
    uint32_t scoreb;
    uint32_t scoreg;
    uint8_t enabled;
};
    public:
        Scoreboard() : _mat(_mat_buf, PNL_W, PNL_H, PNL_C, PNL_ROWS)
    {}
        int drawCh(uint32_t x, uint32_t y, char c, uint32_t clr, uint32_t font_scale = 1);
        int drawNum (uint32_t x, uint32_t y, uint8_t n, uint32_t clr, uint32_t font_scale = 1);

    private:
        std::array<uint8_t, (MAT_WIDTH*MAT_HEIGHT)> _mat_buf;
        Mat _mat;
        const uint32_t FONT_WIDTH{8};
        const uint32_t FONT_HEIGHT{8};
        state_t _curr_state;
};
