#include <cstring>
#include "mat.h"

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>
#include "bbc_master.h"

    extern void ISR(void);
    extern int main(void);
}

constexpr auto BAUD_RATE_U0{1000000};

constexpr auto MAT_WIDTH{192};
constexpr auto MAT_HEIGHT{128};

volatile bool updateMat{false};

void ISR(void) {
    updateMat = true;
    LED0->val++;
}

constexpr auto FONT_WIDTH{8};
constexpr auto FONT_HEIGHT{8};

template<typename T>
T abs(T n) { return (n < 0) ? -n : n; }

std::array<uint8_t, (MAT_WIDTH*MAT_HEIGHT)> mat_buf;
int main() {
    uint8_t clr{0};

    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    LED0->val = 1;
    updateMat = false;
    Mat myMat(mat_buf, 64, 32, 12, 4);

    cli();
    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 9999;
    MTIME0->psc = 9999;
    sei();
    MTIME0->ctrl = 0x3;
    MTIME0->en = 1;


    while(1) {
        if (updateMat) {
            myMat.clear();
            // for (auto i = 5; i < 128; i += 1) {
            //     myMat.drawLineH(i, 192-i, i, clr&0x7);
            // }
            draw_ch(myMat, 10, 10, 'A'+clr%26, clr&0x7);
            myMat.display();
            clr++;
            updateMat = false;
        }
    }

    return 1;
}
