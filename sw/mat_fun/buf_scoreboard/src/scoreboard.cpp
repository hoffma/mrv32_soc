#include "scoreboard.h"

extern "C" {
#include "bbc_master.h"
}

int Scoreboard::drawCh(uint32_t x, uint32_t y, char c, uint32_t clr, uint32_t font_scale) {
    int res = 0;
    uint32_t start = c - 32;

    for (auto j = 0; j < (FONT_HEIGHT*font_scale); ++j) {
        uint32_t tmp = start*FONT_HEIGHT+j/font_scale;
        char b = font_Acorn_BBC_Master[tmp];
        for (auto i = 0; i < (FONT_WIDTH*font_scale); ++i) {
            uint32_t tmpx = x+(FONT_WIDTH*font_scale-1-i);
            // uint32_t tmpy = y+(FONT_HEIGHT*FONT_SCALE-1-j);
            // uint32_t tmpx = x+i;
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> (i/font_scale))&0x1) ? clr : 0;
            res = this->_mat.setPixel(tmpx, tmpy, c_val);
            if (res < 0) return res; /* stops if there was an out of bounds pixel  */
        }
    }
    return 0;
}

int Scoreboard::drawNum (uint32_t x, uint32_t y, uint8_t n, uint32_t clr, uint32_t font_scale) {
    int res = 0;
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    this->drawCh(x, y, tenth, clr, font_scale);
    if (res < 0) return res;
    this->drawCh(x+FONT_WIDTH*font_scale, y, dig, clr, font_scale);
    if (res < 0) return res;

    return 0;
}

void drawScore(const state_t &sb, uint32_t x, uint32_t y, uint32_t font_scale) {
    // draw_num(12, 16, sb.scoreg, 0x2);
    // draw_ch(28, 16, ':', 0x1);
    // draw_num(36, 16, sb.scoreb, 0x4);
    uint32_t x_offset = (FONT_WIDTH-1)*font_scale;
    if ((sb.scoreb > 9) || (sb.scoreg > 9)) {
        draw_num(x, y, sb.scoreg, 0x2, font_scale);
        draw_ch(x + 2*x_offset, y, ':', 0x1, font_scale);
        draw_num(x + 3*x_offset, y, sb.scoreb, 0x4, font_scale);
    } else {
        draw_ch(x, y, '0'+sb.scoreg, 0x2, font_scale);
        draw_ch(x + 1*x_offset, y, ':', 0x1, font_scale);
        draw_ch(x + 2*x_offset, y, '0'+sb.scoreb, 0x4, font_scale);
    }
}
