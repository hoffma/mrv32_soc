
#include "mat.h"

int Mat::setPixel(uint32_t x, uint32_t y, uint8_t clr) {
    if ((x >= MAT_WIDTH) || (y >= MAT_HEIGHT)) return -1;

    auto n = (y*MAT_WIDTH) + x;
    _mat_buf[n] = clr;

    return 0;
}

int Mat::drawLineH(uint32_t x1, uint32_t x2, uint32_t y, uint8_t clr) {
    if ((x1 >= MAT_WIDTH) || (x2 >= MAT_WIDTH) || (y >= MAT_HEIGHT)) return -1;

    for (auto i = x1; i < x2; ++i) {
        this->setPixel(i, y, clr);
    }

    return 0;
}

void Mat::display() {
    for (auto j = 0; j < MAT_HEIGHT; ++j) {
        for (auto i = 0; i < MAT_WIDTH; ++i) {
                auto n = (j*MAT_WIDTH) + i;
                auto clr = _mat_buf[n];
                matrix_pixel_xy(i, j, clr);
        }
    }
}

void Mat::clear() {
    // for (auto j = 0; j < MAT_HEIGHT; ++j) {
    //     for (auto i = 0; i < MAT_WIDTH; ++i) {
    //         _mat_buf[i] = 0;
    //     }
    // }
    for (auto j = 0; j < _mat_buf.size(); ++j) {
        _mat_buf[j] = 0;
    }
}
