
#include <span>
#include <stdint.h>
extern "C" {
#include <led_matrix.h>
}

class Mat {
    public:
        Mat(const std::span<uint8_t>& buf, uint32_t w, uint32_t h, uint32_t c, uint32_t r) 
            : _mat_buf(buf), PNL_WIDTH(w), PNL_HEIGHT(h), PNL_COUNT(c), PNL_ROWS(r)
            // PNL_COLS(PNL_COUNT*PNL_ROWS), MAT_WIDTH(PNL_WIDTH*PNL_COLS), MAT_HEIGHT(PNL_HEIGHT*PNL_ROWS)
        {
            matrix_init(w, h, c, r);
        }

        int setPixel(uint32_t x, uint32_t y, uint8_t clr);
        int drawLineH(uint32_t x1, uint32_t x2, uint32_t y, uint8_t clr);
        void clear();
        void display();
    private:
        const uint32_t      PNL_WIDTH;
        const uint32_t      PNL_HEIGHT;
        const uint32_t      PNL_COUNT;
        const uint32_t      PNL_ROWS;
        const uint32_t  PNL_COLS{PNL_COUNT/PNL_ROWS};
        const uint32_t  MAT_WIDTH{PNL_WIDTH*PNL_COLS};
        const uint32_t  MAT_HEIGHT{PNL_HEIGHT*PNL_ROWS};

        const std::span<uint8_t> &_mat_buf;
};
