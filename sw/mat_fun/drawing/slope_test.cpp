#include <iostream>
#include <stdint.h>
#include "fixed.h"

using namespace numeric;
typedef fixed<16, 16> f16;

using namespace std;

constexpr int32_t SHIFT_AMOUNT{8};
constexpr int32_t MUL{1 << SHIFT_AMOUNT};

template<typename T>
T abs(T n) { return (n < 0) ? -n : n; }

int32_t fx(int32_t x, int32_t m, int32_t b) {
        auto res = ((m*x) >> SHIFT_AMOUNT) + b;
        return res;
}

void draw_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2) {
    f16 fx1{x1};
    f16 fx2{x2};
    f16 fy1{y1};
    f16 fy2{y2};

    f16 slope = (abs(fy2 - fy1)/abs(fx2 - fx1));
    f16 b = fy1 - slope*fx1;

    cout << "slope: " << slope << ", b: " << b << std::endl;

    int32_t start = (x1 < x2) ? x1 : x2;
    int32_t diff = abs(x2 - x1);
    for (auto i = start; i <= (start+diff); ++i) {
        f16 tmpx{i};
        auto y = (slope*tmpx) + b;
        std::cout << "(" << i << ", " << y.to_int() << ")" << std::endl;
    }
}

int main() {
    draw_line(1, 3, 40, 93);

    return 0;
}
