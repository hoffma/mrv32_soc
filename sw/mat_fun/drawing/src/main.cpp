#include <cstring>
#include "fixed.h"

#include <mystd.hpp>
#include <uart.hpp>
extern "C" {
#include <led_matrix.h>

    extern void ISR(void);
    extern int main(void);
}

using namespace numeric;
typedef fixed<24, 8> f16;

constexpr auto BAUD_RATE_U0{1000000};

constexpr auto MAT_WIDTH{192};
constexpr auto MAT_HEIGHT{128};

void ISR(void) {
    /* nothing */
}

struct Point {
    f16 x;
    f16 y;
};

template<typename T>
T abs(T n) { return (n < 0) ? -n : n; }

void draw_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t clr);
void draw_line(int32_t x1, int32_t x2, int32_t y, uint8_t clr);

void fill_triangle(const Point &p1, const Point &p2, const Point &p3, uint8_t clr=0x0f) {
    Point points[]{p1, p2, p3};

    for (auto i = 0; i < 2; ++i) {
        Point tmp{0,0};
        if(points[i].y < points[i+1].y) {
            tmp = points[i];
            points[i] = points[i+1];
            points[i+1] = tmp;
        }
    }

    f16 x_left{points[0].x};
    f16 x_right{x_left};
    f16 inv_slope_left;
    f16 inv_slope_right;
    if (points[1].x < points[2].x) {
        // mid is on the left side
        inv_slope_left = (points[1].x - points[0].x)/(points[1].y - points[0].y);
        inv_slope_right = (points[2].x - points[0].x)/(points[2].y - points[0].y);
        int32_t y = 0;
        for (y = points[0].y.to_int(); y < points[1].y.to_int(); ++y) {
            draw_line(x_left.to_int(), x_right.to_int(), y, clr);
            x_left += inv_slope_left;
            x_right += inv_slope_right;
        }
        inv_slope_left = (points[2].x - points[1].x)/(points[2].y - points[1].y);
        for (; y < points[2].y.to_int(); ++y) {
            draw_line(x_left.to_int(), x_right.to_int(), y, clr);
            x_left += inv_slope_left;
            x_right += inv_slope_right;
        }
    } else {
        inv_slope_right = (points[1].x - points[0].x)/(points[1].y - points[0].y);
        inv_slope_left = (points[2].x - points[0].x)/(points[2].y - points[0].y);
        int32_t y = 0;
        for (y = points[0].y.to_int(); y < points[1].y.to_int(); ++y) {
            draw_line(x_left.to_int(), x_right.to_int(), y, clr);
            x_left += inv_slope_left;
            x_right += inv_slope_right;
        }
        inv_slope_right = (points[2].x - points[1].x)/(points[2].y - points[1].y);
        for (; y < points[2].y.to_int(); ++y) {
            draw_line(x_left.to_int(), x_right.to_int(), y, clr);
            x_left += inv_slope_left;
            x_right += inv_slope_right;
        }
    }
}

void draw_line(int32_t x1, int32_t x2, int32_t y, uint8_t clr) {
    auto start = x1 < x2 ? x1 : x2;
    auto end = x1 > x2 ? x1 : x2;
    for (auto i = start; i <= end; ++i) {
        matrix_pixel_xy(i, y, clr);
    }
}
void draw_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t clr) {
    f16 fx1{x1};
    f16 fx2{x2};
    f16 fy1{y1};
    f16 fy2{y2};

    f16 slope = ((fy2 - fy1)/(fx2 - fx1));
    f16 b = fy1 - slope*fx1;

    auto start = (x1 < x2) ? x1 : x2;
    auto diff = abs(x1 - x2);
    for (auto i = start; i <= (start+diff); ++i) {
        f16 tmpx{i};
        auto y = (slope*tmpx) + b;
        matrix_pixel_xy(i, y.to_int(), clr);
    }
}

int main() {
    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    matrix_init(64, 32, 12, 4);

    matrix_clear();
    cli();
    matrix_pixel_xy(1, 1, 0x4);

    // draw_line(70, 10, 20, 40, 0xa2);
    // draw_line(120, 40, 50, 100, 0xf0);
    // draw_line(50, 100, 70, 10, 0x0f);

    // draw_line(5, 119, 187, 119, 0x2a);
    // draw_line(5, 119, 93, 12, 0x83);
    // draw_line(93, 12, 187, 119, 0x39);

    // draw_line(7, 118, 185, 118, 0x19);
    // draw_line(7, 118, 93, 14, 0x22);
    // draw_line(93, 14, 185, 118, 0x14);

    Point p1{70, 10};
    Point p2{40, 110};
    Point p3{100, 50};
    fill_triangle(p1, p2, p3, 0x14);
    while(1) {
        __asm__("nop");
    }

    return 1;
}
