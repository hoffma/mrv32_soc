#include <cstring>
#include "mat.h"

extern "C" {
#include <mystd.hpp>
#include <uart.hpp>

    extern void ISR(void);
    extern int main(void);
}

constexpr auto BAUD_RATE_U0{1000000};

constexpr auto MAT_WIDTH{192};
constexpr auto MAT_HEIGHT{128};

volatile bool updateMat{false};

void ISR(void) {
    updateMat = true;
    LED0->val++;
}

const uint8_t colors[] {
    0x12,
    0x52,
    0x41,
    0x32
};

template<typename T>
T abs(T n) { return (n < 0) ? -n : n; }

std::array<uint16_t, ((MAT_WIDTH*MAT_HEIGHT)/2)> mat_buf;
int main() {
    uint8_t clr{0};

    std_init();
    uart_set_brd(UART0, (F_CPU/BAUD_RATE_U0));
    LED0->val = 1;
    updateMat = false;
    Mat myMat(mat_buf, 64, 32, 12, 4);

    cli();
    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 9999;
    MTIME0->psc = 9999;
    sei();
    MTIME0->ctrl = 0x3;
    MTIME0->en = 1;

    myMat.clear();
    myMat.display2();

    myMat.setPixel(0, 0, colors[0]);
    myMat.setPixel(1, 0, colors[1]);
    myMat.setPixel(2, 0, colors[2]);
    myMat.setPixel(3, 0, colors[3]);
    myMat.setPixel(188, 127, colors[3]);
    myMat.setPixel(189, 127, colors[2]);
    myMat.setPixel(190, 127, colors[1]);
    myMat.setPixel(191, 127, colors[0]);
    // matrix_set(192, colors[3]);

    for (auto i = 127; i >= 0; --i) {
        myMat.setPixel(i, i, colors[clr&0x3]);
    }
    myMat.display2();
    while(1) {
        if (updateMat) {
            for (auto i = 30; i < 60; i += 1) {
                myMat.drawLineH(i, 192-i, i, colors[clr&0x3]);
            }
            myMat.display2();
            clr++;
            updateMat = false;
        }
    }

    return 1;
}
