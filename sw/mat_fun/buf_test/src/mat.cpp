
#include "mat.h"

int Mat::setPixel(uint32_t x, uint32_t y, uint8_t clr) {
    if ((x < 0) || (y < 0)) return -1;
    if (y >= (MAT_HEIGHT)) return -1;
    if (x >= (MAT_WIDTH)) return -1;

    uint32_t MAT_pxl_per_row{MAT_WIDTH};
    uint32_t MAT_pxl_per_col{MAT_HEIGHT};
    uint32_t MAT_pxl_row_chain{PNL_COUNT*PNL_WIDTH};
    bool is_top = false;

    x = MAT_pxl_per_row - x - 1;
    y = MAT_pxl_per_col - y - 1;

    int tmp_offset = 0;
    int start_addr = 0;

    int tmp_y = (y/PNL_HEIGHT);
    bool inverted = (tmp_y&1) == 1;
    int rel_y = inverted ? (PNL_HEIGHT - y%PNL_HEIGHT - 1) : (y%PNL_HEIGHT);

    if (rel_y >= (PNL_HEIGHT/2)) {
        rel_y -= (PNL_HEIGHT/2);
        is_top = false;
    }  else {
        is_top = true;
    }
    start_addr = MAT_pxl_row_chain*rel_y + tmp_y*MAT_pxl_per_row;
    if (inverted) {
        start_addr += x;
    } else {
        start_addr += (MAT_pxl_per_row - x - 1);
    }
    if (is_top) {
        _mat_buf[start_addr] &= ~(0xff); 
        _mat_buf[start_addr] |= clr; 
    } else {
       _mat_buf[start_addr]  &= ~(0xff00); 
       _mat_buf[start_addr]  |= (clr << 8); 
    }

    return start_addr;
}

// int Mat::setPixel(uint32_t x, uint32_t y, uint8_t clr) {
//     if ((x >= MAT_WIDTH) || (y >= MAT_HEIGHT)) return -1;
// 
//     auto n = (y*MAT_WIDTH) + x;
//     _mat_buf[n] = clr;
// 
//     return 0;
// }

int Mat::drawLineH(uint32_t x1, uint32_t x2, uint32_t y, uint8_t clr) {
    if ((x1 >= MAT_WIDTH) || (x2 >= MAT_WIDTH) || (y >= MAT_HEIGHT)) return -1;

    for (auto i = x1; i < x2; ++i) {
        this->setPixel(i, y, clr);
    }

    return 0;
}

void Mat::display() {
    for (auto j = 0; j < MAT_HEIGHT; ++j) {
        for (auto i = 0; i < MAT_WIDTH; ++i) {
                auto n = (j*MAT_WIDTH) + i;
                auto clr = _mat_buf[n];
                matrix_pixel_xy(i, j, clr);
        }
    }
}

void Mat::display2() {
    uint32_t PXL_CNT_HALF = PXL_CNT/2;
    for (auto i = 0; i < PXL_CNT_HALF; ++i) {
        // uint16_t val = _mat_buf[i] | (_mat_buf[i+PXL_CNT_HALF] << 8);
        // uint16_t val = _mat_buf[i];
        matrix_set(i, _mat_buf[i]);
    }
}

void Mat::clear() {
    // for (auto j = 0; j < MAT_HEIGHT; ++j) {
    //     for (auto i = 0; i < MAT_WIDTH; ++i) {
    //         _mat_buf[i] = 0;
    //     }
    // }
    for (auto j = 0; j < _mat_buf.size(); ++j) {
        _mat_buf[j] = 0;
    }
}
