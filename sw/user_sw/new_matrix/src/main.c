#include <mystd.h>
#include <uart.h>
#include "matrix.h"
#include "bbc_master.h"


extern void ISR(void);
extern int main(void);

#define BAUD (1000000)

#define FONT_WIDTH 8
#define FONT_HEIGHT 8
#define FONT_SCALE 2

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

typedef enum {
    CLR_OFF,
    CLR_RED,
    CLR_GREEN,
    CLR_YELLOW,
    CLR_BLUE,
    CLR_MAGENTA,
    CLR_TURQ,
    CLR_WHITE
// TODO: fill the rest
} color_t;

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

inline int abs(int a) {
    return a > 0 ? a : -1;
}

void draw_line(int x1, int y1, int x2, int y2, uint32_t val) {
    float m = (float)abs(y2-y1)/(float)abs(x2-x1);
    float b = (float)y1 - (m*(float)x1);

    int start = x1 > x2 ? x2 : x1;
    int end = x1 > x2 ? x1 : x2;

    for (int i = start; i <= end; i++) {
        float y = m*(float)i + b;
        matrix_pixel_xy(i, y, val);
    }
    // float y = m*x + b;
}

int draw_ch(uint32_t x, uint32_t y, char c, uint32_t clr) {
    int res = 0;
    uint32_t start = c - 32;

    for (auto j = 0; j < (FONT_HEIGHT*FONT_SCALE); ++j) {
        uint32_t tmp = start*FONT_HEIGHT+j/FONT_SCALE;
        char b = font_Acorn_BBC_Master[tmp];
        for (auto i = 0; i < (FONT_WIDTH*FONT_SCALE); ++i) {
            uint32_t tmpx = x+(FONT_WIDTH*FONT_SCALE-1-i);
            // uint32_t tmpy = y+(FONT_HEIGHT*FONT_SCALE-1-j);
            // uint32_t tmpx = x+i;
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> (i/FONT_SCALE))&0x1) ? clr : 0;
            res = matrix_pixel_xy(tmpx, tmpy, c_val);
            if (res < 0) return res; /* stops if there was an out of bounds pixel  */
        }
    }
    return 0;
}

int draw_dig(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    int res = 0;
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    draw_ch(x, y, tenth, clr);
    if (res < 0) return res;
    draw_ch(x+FONT_WIDTH*FONT_SCALE, y, dig, clr);
    if (res < 0) return res;

    return 0;
}

uint32_t strlen(const char *s) {
    uint32_t n = 0;
    while (*s) {
        s++;
        n++;
    }
    return n;
}

int draw_str(uint32_t x, uint32_t y, char *s, uint32_t clr) {
    int res = 0;
    uint32_t n = strlen(s);

    // while(*s) {
    //     res = draw_ch(x, y, *s, clr);
    //     x += FONT_WIDTH*FONT_SCALE;
    //     if (res < 0) return res;
    //     s++;
    // }

    for (int i = n-1; i >= 0; i--) {
        res = draw_ch(x, y, s[i], clr);
        x += FONT_WIDTH*FONT_SCALE;
        if (res < 0) return res;
    }
}

void cnt_updown() {
    static uint8_t down = 0;

    if (LED0->val >= 15) {
        down = 1;
    } else if (LED0->val == 0) {
        down = 0;
    }
    LED0->val += (down ? -1 : 1);
}

void draw_mat() {
    uint32_t tmp = LED0->val;

    matrix_clear();
    matrix_pixel_xy(1, 1, 0x1);
    matrix_pixel_xy(63, 0, 0x2);
    matrix_pixel_xy(0, 63, 0x4);
    matrix_pixel_xy(63, 63, 0x7);

    // matrix_set(2, 0x0104);
    // matrix_pixel_xy(63, 63, tmp&0x7);
    // matrix_pixel_xy(127, 63, tmp&0x7);
    // matrix_pixel_xy(127, 0, 0x1);
    // matrix_pixel_xy(0, 0, tmp&0x7);
    // matrix_pixel_xy(63, 0, tmp&0x7);
    // matrix_pixel_xy(0, 63, tmp&0x7);
    // matrix_pixel_xy(63, 63, tmp&0x7);
    draw_dig(0, 16, tmp, 0x1);
    draw_str(0, 24, "Hello!\0", 0x4);
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello!\n");
    // draw_str(1, 33, "Hello!", 0x2);
}

void fizzbuzz() {
    uint32_t tmp = LED0->val;

    matrix_clear();
    if ((tmp%3 == 0) && (tmp%5 == 0)) {
        draw_str(0, 0, "FizzBuzz\0", 0x1);
    } else if (tmp%3 == 0) {
        draw_str(0, 0, "Fizz\0", 0x2);
    } else if (tmp%5 == 0) {
        draw_str(0, 0, "Buzz\0", 0x4);
    } else {
        draw_dig(0, 0, tmp, 0x6);
    }
    LED0->val = (tmp >= 99) ? 0 : tmp+1;
}

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    {__COUNTER__, cnt_updown, 500, 500},
    {__COUNTER__, draw_mat, 500, 500},
    // {__COUNTER__, fizzbuzz, 1000, 1000},
    {__COUNTER__, hello, 500, 500},
};

void short_delay() {
  for (volatile uint32_t i = 0; i < 3; i++)
    ;
}
void long_delay() {
  for (volatile uint32_t i = 0; i < 10000; i++)
    ;
}

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));

    matrix_clear();
    LED0->val = 0x4;

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;

    LED0->val++;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 99;
    MTIME0->psc = 499;

    LED0->val = 0x1;
    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */

  while (1) {
      if (tick_flag) {
          cli();
          tick(tasks_start, task_cnt);
          tick_flag = 0;
          sei();
      }
    __asm__ volatile("nop\n\t");
  }
}
