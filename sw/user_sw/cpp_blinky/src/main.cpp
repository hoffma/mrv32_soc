
#define F_CPU (100000000)

extern "C" {
#include <mystd.h>

    extern void ISR(void);
    extern int main(void);
}

void ISR(void) {
    LED0->val++;
}

int main(void) {
    std_init();

    LED0->val= auto(1);

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 4999;
    MTIME0->psc = 9999;

    sei();
    MTIME0->ctrl = 0x3;
    MTIME0->en = 1;

    while (1) {
        // nothing
    }

    return 1;
}
