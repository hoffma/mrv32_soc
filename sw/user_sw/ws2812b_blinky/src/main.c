#include <mystd.h>
#include <uart.h>

#include "bbc_master.h"

#define FONT_WIDTH 8
#define FONT_HEIGHT 8

uint32_t clrs[16] = {
    0x010101,
    0x000005,
    0x000500,
    0x000505,
    0x050000,
    0x050005,
    0x050500,
    0x050505,
    0x010203,
    0x030201,
    0x010500,
    0x000205,
    0x050003,
    0x050105,
    0x050501,
    0x050301,
};

extern void ISR(void);
extern int main(void);

#define BAUD (1000000)

#define WS_BASE     (0x01300000)
#define WS_REG      (*(volatile uint32_t *) (WS_BASE + 0x00))
#define WS_CTRL_REG      (*(volatile uint32_t *) (WS_BASE + 0x4000))

#define MAT_W   (16)
#define MAT_H   (16)
#define MAT_CNT (2)
#define MAT_ROWS (1)
#define PXL_CNT (MAT_W*MAT_H*MAT_CNT)

inline int abs(int a) {
    return a > 0 ? a : -1;
}

void set_pxl(uint32_t n, uint32_t val) {
    uint32_t *ws_addr = (uint32_t *) WS_BASE;
    *(ws_addr + n) = val;
}

void set_xy(uint32_t x, uint32_t y, uint32_t val) {
    uint32_t n = 0;
    uint32_t tmp_x = (MAT_W - 1 - x);

    uint32_t add = (x/MAT_W)*(MAT_W*MAT_H);

    if (y%2) {
        n = y*MAT_H + (MAT_W - tmp_x - 1);
    } else {
        n = y*MAT_H + tmp_x;
    }

    set_pxl(n+add, val);
}

void draw_line(int x1, int y1, int x2, int y2, uint32_t val) {
    float m = (float)abs(y2-y1)/(float)abs(x2-x1);
    float b = (float)y1 - (m*(float)x1);

    int start = x1 > x2 ? x2 : x1;
    int end = x1 > x2 ? x1 : x2;

    for (int i = start; i <= end; i++) {
        float y = m*(float)i + b;
        set_xy(i, y, val);
    }
    // float y = m*x + b;
}

void draw_ch(uint32_t x, uint32_t y, char c, uint32_t clr) {
    uint32_t start = c - 32;

    for (auto j = 0; j < FONT_HEIGHT; ++j) {
        char b = font_Acorn_BBC_Master[start*FONT_HEIGHT+j];
        for (auto i = 0; i < FONT_WIDTH; ++i) {
            uint32_t tmpx = x+(FONT_WIDTH-1-i);
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> i)&0x1) ? clr : 0;
            set_xy(tmpx, tmpy, c_val);
        }
    }
}

void draw_dig(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    draw_ch(x, y, tenth, 0xff4400);
    draw_ch(x+FONT_WIDTH, y, dig, 0xff4400);
}

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

void cnt_updown() {
    static uint8_t down = 0;

    if (LED0->val >= 99)
        down = 1;
    else if (LED0->val == 0)
        down = 0;

    LED0->val += (down ? -1 : 1);
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello!\n");
}

void update_ws2812b() {
    uint32_t tmp = LED0->val;
    for (int i = 0; i < PXL_CNT; i++) {
        set_pxl(i, 0);
    }
    set_xy(0, 5, clrs[1]);
    set_xy(31, 15, clrs[tmp%16]);
    // set_xy(15, 0, clrs[tmp%16]);
    // set_xy(15, 16, clrs[tmp%16]);
    // draw_dig(0, 3, tmp, 0xff0044);
}

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    {__COUNTER__, cnt_updown, 200, 200},
    {__COUNTER__, update_ws2812b, 200, 200},
    {__COUNTER__, hello, 1000, 1000},
};

void short_delay() {
  for (volatile uint32_t i = 0; i < 3; i++)
    ;
}

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));
    WS_CTRL_REG = 256*2;

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;

    LED0->val++;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 99;
    MTIME0->psc = 999;

    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */

  while (1) {
      if (tick_flag) {
          cli();
          tick(tasks_start, task_cnt);
          tick_flag = 0;
          sei();
      }
    __asm__ volatile("nop\n\t");
  }
}
