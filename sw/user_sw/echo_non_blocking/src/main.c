#include <mystd.h>
#include <uart.h>


extern void ISR(void);
extern int main(void);

#define BAUD (1000000)

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

inline int abs(int a) {
    return a > 0 ? a : -1;
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello!\n");
    // draw_str(1, 33, "Hello!", 0x2);
}

void echo_uart() {
    if (uart_rx_valid(UART0)) {
        uint8_t b = uart_recv_nb(UART0);
        uart_putchar(UART0, (char) b);
        uart_putchar(UART1, (char) b);
    }
}

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    {__COUNTER__, blink, 999, 999},
    {__COUNTER__, echo_uart, 9, 9},
};

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));
    uart_set_brd(UART1, (F_CPU/BAUD));

    LED0->val = 0x4;

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;

    LED0->val++;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 999;
    MTIME0->psc = 99;

    LED0->val = 0x0;
    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */

  while (1) {
      if (tick_flag) {
          cli();
          tick(tasks_start, task_cnt);
          tick_flag = 0;
          sei();
      }
    __asm__ volatile("nop\n\t");
  }
}
