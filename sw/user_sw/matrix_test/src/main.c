#include <mystd.h>
#include <uart.h>


extern void ISR(void);
extern int main(void);
void setPixel(uint32_t n, uint32_t clr1, uint32_t clr2);
void setPixelTop(uint16_t n, uint32_t clr);
void setPixelBot(uint16_t n, uint32_t clr);
void setPixelXY(uint32_t x, uint32_t y, uint16_t clr);
void clearMat();

#define BAUD (1000000)

#define MAT_BASE    (0x01200000)
#define MAT_WIDTH   (64)
#define MAT_HEIGHT  (64)
#define MAT_CHAIN   (1)
#define PIXEL_COUNT (MAT_WIDTH*MAT_HEIGHT*MAT_CHAIN)
#define PIXEL_HALF  (PIXEL_COUNT/2)

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

typedef enum {
    CLR_OFF,
    CLR_RED,
    CLR_GREEN,
    CLR_YELLOW,
    CLR_BLUE,
    CLR_MAGENTA,
    CLR_TURQ,
    CLR_WHITE
// TODO: fill the rest
} color_t;

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

void cnt_updown() {
    static uint8_t down = 0;

    clearMat();
    if (LED0->val >= 15) {
        down = 1;
    } else if (LED0->val == 0) {
        down = 0;
    }
    LED0->val += (down ? -1 : 1);
    // setPixel((1 << LED0->val), 0x1, 0x2);
    //
    uint32_t start = LED0->val;
    for (int i = 0; i < (MAT_HEIGHT)/2; ++i) {
        setPixelTop(start + i + (i*MAT_WIDTH), 0x1);
        setPixelBot((MAT_WIDTH-i-1-start) + (i*MAT_WIDTH), 0x2);
    }

    int pos1 = (MAT_WIDTH-31-start);
    int pos2 = (31-8+start);
    setPixelXY(pos1, 7, 0x3);
    setPixelXY(pos2, 7+16, 0x4);

    for (int i = 0; i < 32; i++) {
        setPixelXY(1, i, 0x6);
    }
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello!\n");
}

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    {__COUNTER__, cnt_updown, 500, 500},
    {__COUNTER__, hello, 1000, 1000},
};

void short_delay() {
  for (volatile uint32_t i = 0; i < 3; i++)
    ;
}

void setPixelXY(uint32_t x, uint32_t y, uint16_t clr) {
    uint32_t tmp_n = (y*MAT_WIDTH) + x;

    if (tmp_n < PIXEL_HALF) {
        setPixelTop(tmp_n, clr);
    } else {
        setPixelBot((tmp_n - PIXEL_HALF), clr);
    }
}

void setPixel(uint32_t n, uint32_t clr1, uint32_t clr2) {
    volatile uint32_t *tmp = (volatile uint32_t *) MAT_BASE;
    clr1 = clr1&0x7;
    clr2 = clr2&0x7;

    *(tmp + n) = (clr2 << 8) | clr1;
}

void setPixelTop(uint16_t n, uint32_t clr) {
    volatile uint32_t *tmp = (volatile uint32_t *) MAT_BASE;
    clr = clr &0x7;

    *(tmp + n) &= ~(0xff);
    *(tmp + n) |=  clr;
}

void setPixelBot(uint16_t n, uint32_t clr) {
    volatile uint32_t *tmp = (volatile uint32_t *) MAT_BASE;
    clr = clr &0x7;

    *(tmp + n) &= ~(0xff00);
    *(tmp + n) |=  (clr << 8);
}

void clearMat() {
    for (int i = 0; i < (PIXEL_COUNT/2); ++i) {
        setPixel(i, 0x0, 0x0);
    }
}

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));

    volatile uint32_t *MAT_START = (volatile uint32_t *) MAT_BASE;

    LED0->val = 0x4;

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;

    LED0->val++;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 99;
    MTIME0->psc = 499;

    LED0->val = 0x0;
    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */

  while (1) {
      if (tick_flag) {
          cli();
          tick(tasks_start, task_cnt);
          tick_flag = 0;
          sei();
      }
    __asm__ volatile("nop\n\t");
  }
}
