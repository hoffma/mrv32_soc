start:
/* Main program
 **********************************/
.section .text
.globl _start

la sp, _ram_end # initialize stack pointer
# word align
andi sp, sp, 0xfffffffc

# zero-initialize register file
addi x1, zero, 0
# addi x2, zero, 0 # stack pointer is initialized somehwere else
addi x3, zero, 0
addi x4, zero, 0
addi x5, zero, 0
addi x6, zero, 0
addi x7, zero, 0
addi x8, zero, 0
addi x9, zero, 0
addi x10, zero, 0
addi x11, zero, 0
addi x12, zero, 0
addi x13, zero, 0
addi x14, zero, 0
addi x15, zero, 0
addi x16, zero, 0
addi x17, zero, 0
addi x18, zero, 0
addi x19, zero, 0
addi x20, zero, 0
addi x21, zero, 0
addi x22, zero, 0
addi x23, zero, 0
addi x24, zero, 0
addi x25, zero, 0
addi x26, zero, 0
addi x27, zero, 0
addi x28, zero, 0
addi x29, zero, 0
addi x30, zero, 0
addi x31, zero, 0

# copy ROM to RAM
__copy_data_init:
  la   t0, __data_src_begin
  la   t1, __data_dst_begin
  la   t2, __data_dst_end
  beq  t0, t1, __copy_data_loop_end // src == dst, nothing to do

__copy_data_loop:
  bge  t1, t2,  __copy_data_loop_end
  lw   s0, 0(t0)
  sw   s0, 0(t1)
  addi t0, t0, 4
  addi t1, t1, 4
  j    __copy_data_loop
__copy_data_loop_end:

addi t0, zero, 0
addi t1, zero, 0
addi t2, zero, 0
addi s0, zero, 0

# clear BSS
__clear_bss_init:
  la   t0, __bss_start
  la   t1, __bss_end
__clear_bss_loop:
  bge  t0, t1,  __clear_bss_loop_end
  sw   zero, 0(t0)
  addi t0, t0, 4
  j    __clear_bss_loop
__clear_bss_loop_end:

addi t0, zero, 0
addi t1, zero, 0

# Update LEDs
li a0, 0x01000000
li a1, 0x99
sw a1, 0(a0)

la s0, trap_handler
csrw mtvec, s0
# csrrw t0, mtvec, t0

# enable interrupts
# li a1, 128
# csrw mie, a1
# li a1, 8
# csrw mstatus, a1

li a1, 0x67
sw a1, 0(a0)
nop
nop
li a1, 0x68
sw a1, 0(a0)

call main

loop:
j loop


trap_handler:
# save  s-registers
isr_prologue:
    addi sp, sp, -16
    sw s0, 0(sp)
    sw s1, 4(sp)
    sw s2, 8(sp)
    sw ra, 12(sp)
isr_body:
    # increment led
    # mtime base address
    li t1, 0xfffff000
    sw zero, 20(t1)
    sw zero, 4(t1)
    sw zero, 0(t1)

    call ISR

    li t1, 0xfffff000
    li a1, 1
    sw a1, 20(t1)
    # restore s-registers
isr_epilogue:
    lw s0, 0(sp)
    lw s1, 4(sp)
    lw s2, 8(sp)
    lw ra, 12(sp)
    addi sp, sp, 16
    mret
