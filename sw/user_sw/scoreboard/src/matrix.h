#pragma once

#include <stdint.h>

constexpr auto MAT_W = 64;
constexpr auto MAT_H = 64;
constexpr auto MAT_cnt = 1;
constexpr auto MAT_rows = 1;
constexpr auto MAT_cols = MAT_cnt/MAT_rows;
constexpr auto MAT_pxl_per_row = MAT_W*MAT_cols;
constexpr auto MAT_pxl_row_chain = MAT_cnt*MAT_W;

void matrix_clear();
void matrix_fill(uint8_t clr);
int matrix_pixel_xy(uint32_t x, uint32_t y, uint8_t clr);
void matrix_set(uint16_t n, uint16_t val);
