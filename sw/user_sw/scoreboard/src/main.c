#include <mystd.h>
#include <uart.h>
#include "matrix.h"
#include "bbc_master.h"


extern void ISR(void);
extern int main(void);

#define BAUD (1000000)

#define FONT_WIDTH 8
#define FONT_HEIGHT 8
#define FONT_SCALE 1

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

struct score_t {
    uint8_t score_b;
    uint8_t score_g;
    uint32_t seconds;
};

struct score_t scoreboard;

typedef enum {
    CLR_OFF,
    CLR_RED,
    CLR_GREEN,
    CLR_YELLOW,
    CLR_BLUE,
    CLR_MAGENTA,
    CLR_TURQ,
    CLR_WHITE
// TODO: fill the rest
} color_t;

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

inline int abs(int a) {
    return a > 0 ? a : -1;
}

void draw_line(int x1, int y1, int x2, int y2, uint32_t val) {
    float m = (float)abs(y2-y1)/(float)abs(x2-x1);
    float b = (float)y1 - (m*(float)x1);

    int start = x1 > x2 ? x2 : x1;
    int end = x1 > x2 ? x1 : x2;

    for (int i = start; i <= end; i++) {
        float y = m*(float)i + b;
        matrix_pixel_xy(i, y, val);
    }
    // float y = m*x + b;
}

int draw_ch(uint32_t x, uint32_t y, char c, uint32_t clr) {
    int res = 0;
    uint32_t start = c - 32;

    for (auto j = 0; j < (FONT_HEIGHT*FONT_SCALE); ++j) {
        uint32_t tmp = start*FONT_HEIGHT+j/FONT_SCALE;
        char b = font_Acorn_BBC_Master[tmp];
        for (auto i = 0; i < (FONT_WIDTH*FONT_SCALE); ++i) {
            uint32_t tmpx = x+(FONT_WIDTH*FONT_SCALE-1-i);
            // uint32_t tmpy = y+(FONT_HEIGHT*FONT_SCALE-1-j);
            // uint32_t tmpx = x+i;
            uint32_t tmpy = y+j;
            // 0x5c - 0101 1100
            uint32_t c_val = ((b >> (i/FONT_SCALE))&0x1) ? clr : 0;
            res = matrix_pixel_xy(tmpx, tmpy, c_val);
            if (res < 0) return res; /* stops if there was an out of bounds pixel  */
        }
    }
    return 0;
}

int draw_dig(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    int res = 0;
    char dig = '0'+(n%10);

    draw_ch(x, y, dig, clr);
    if (res < 0) return res;

    return 0;
}

int draw_num(uint32_t x, uint32_t y, uint8_t n, uint32_t clr) {
    int res = 0;
    char tenth = '0'+(n/10);
    char dig = '0'+(n%10);

    draw_ch(x, y, tenth, clr);
    if (res < 0) return res;
    draw_ch(x+FONT_WIDTH*FONT_SCALE, y, dig, clr);
    if (res < 0) return res;

    return 0;
}

uint32_t strlen(const char *s) {
    uint32_t n = 0;
    while (*s) {
        s++;
        n++;
    }
    return n;
}

// TODO: Make this better
int draw_str(uint32_t x, uint32_t y, char *s, uint32_t clr) {
    int res = 0;
    uint32_t n = strlen(s);

    // while(*s) {
    //     res = draw_ch(x, y, *s, clr);
    //     x += FONT_WIDTH*FONT_SCALE;
    //     if (res < 0) return res;
    //     s++;
    // }

    for (int i = n-1; i >= 0; i--) {
        res = draw_ch(x, y, s[i], clr);
        x += FONT_WIDTH*FONT_SCALE;
        if (res < 0) return res;
    }

    return res;
}

void draw_score() {
    draw_num(12, 16, scoreboard.score_g, 0x2);
    draw_ch(28, 16, ':', 0x1);
    draw_num(36, 16, scoreboard.score_b, 0x4);
}

void time_tick() {
    uint32_t minutes = scoreboard.seconds/60;
    uint32_t seconds = scoreboard.seconds%60;

    draw_num(12, 44, minutes, 0x5);
    draw_ch(28, 44, ':', 0x5);
    draw_num(36, 44, seconds, 0x5);

    if (scoreboard.seconds > 0) {
        scoreboard.seconds--;
    }
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello!\n");
    // draw_str(1, 33, "Hello!", 0x2);
}

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    {__COUNTER__, time_tick, 999, 999},
    {__COUNTER__, draw_score, 999, 999},
    {__COUNTER__, hello, 2999, 2999},
};

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));
    scoreboard.score_b = 0;
    scoreboard.score_g = 0;
    scoreboard.seconds = 300;

    matrix_clear();
    LED0->val = 0x4;

    for(int i = 0; i < 64; ++i) {
        matrix_pixel_xy(i, 30, 0x3);
        matrix_pixel_xy(i, 31, 0x3);
        matrix_pixel_xy(i, 32, 0x3);
        matrix_pixel_xy(i, 33, 0x3);
    }

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;

    LED0->val++;

    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 999;
    MTIME0->psc = 99;

    LED0->val = 0x1;
    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */

  while (1) {
      if (tick_flag) {
          cli();
          tick(tasks_start, task_cnt);
          tick_flag = 0;
          sei();
      }
    __asm__ volatile("nop\n\t");
  }
}
