#include <mystd.h>
#include <uart.h>


extern void ISR(void);
extern int main(void);

#define BAUD (1000000)
#define BAUD_UART1 (115200)

struct task_t {
    const uint8_t t_id;
    void (*const fn)(void);
    uint32_t delay;
    uint32_t period;
};

struct state_t {
    uint32_t seconds;
    uint32_t scoreb;
    uint32_t scoreg;
    uint8_t enabled;
};

volatile struct state_t curr_state;
volatile struct state_t state_in;
bool paket_received;
constexpr uint8_t START_BYTE = 0xde;
constexpr uint8_t END_BYTE = 0xad;
constexpr uint8_t PAKET_SIZE = sizeof(struct state_t);

void tick(struct task_t *tasks, uint32_t cnt);

volatile uint8_t tick_flag = 0;

void ISR(void) {
    tick_flag = 1;
}

// int sprintf(char *restrict str, const char *restrict format, ...);
// 
// int sprintf(char *restrict str, const char *restrict format, ...) {
//     while(*format) {
//     }
// }

inline int abs(int a) {
    return a > 0 ? a : -1;
}

uint8_t buf[255];

void tmp_print_int32(struct uart_t *dev, int32_t n) {
    int i = 0;
    if (0 == n) dev->tx = '0';
    int32_t num = abs(n);

    while (num > 0) {
        int32_t tmp = num%10;
        buf[i] = tmp;
        num = num/10;
        i++;
    }
    while (i > 0) {
        i--;
        uart_putchar(dev, '0' + buf[i]);
    }
}

void tmp_print_state(struct uart_t *dev, volatile struct state_t *state) {
    uart_puts(dev, "{\n\t\"seconds\": ");
    tmp_print_int32(dev, state->seconds);
    uart_puts(dev, ",\n\t\"scoreb\": ");
    tmp_print_int32(dev, state->scoreb);
    uart_puts(dev, ",\n\t\"scoreg\": ");
    tmp_print_int32(dev, state->scoreg);
    uart_puts(dev, ",\n\t\"enabled\": ");
    uart_puts(dev, state->enabled ? "true" : "false");
    uart_puts(dev, "\n}\n");
}

void echo_uart() {
    if (uart_rx_valid(UART0)) {
        uint8_t b = uart_recv_nb(UART0);
        uart_putchar(UART0, (char) b);
        uart_putchar(UART1, (char) b);
    }
}

bool is_start_byte(uint8_t b) {
    return b == START_BYTE;
}

void recv_paket() {
    static uint32_t byte_cnt = 0;
    static bool is_receiving = false;
    uint8_t *pkg_start;

    if (uart_rx_valid(UART1)) {
        uint8_t b = uart_recv_nb(UART1);
        if (!is_receiving) {
            is_receiving = is_start_byte(b);
            byte_cnt = 0;
            pkg_start = (uint8_t *) &state_in;
        } else {
            if (byte_cnt < PAKET_SIZE) {
                pkg_start[byte_cnt] = b;
                byte_cnt++;
            } else {
                byte_cnt = 0;
                if (b == END_BYTE) {
                    LED0->val++;
                    state_in = *((struct state_t *) pkg_start);
                    paket_received = true;
                } else {
                    is_receiving = false;
                }
            }
        }
    }
}

void show_state() {
    if (curr_state.seconds > 0) {
        curr_state.enabled = true;
        curr_state.seconds--;
    } else {
        curr_state.enabled = false;
    }
    tmp_print_state(UART0, &curr_state);
}

void update_state() {
    if (paket_received) {
        curr_state.seconds = state_in.seconds;
        curr_state.scoreb = state_in.scoreb;
        curr_state.scoreg = state_in.scoreg;
        curr_state.enabled = state_in.enabled;
        paket_received = false;
        tmp_print_state(UART0, &curr_state);
    }
}

void blink() {
    LED0->val++;
}

void hello() {
    uart_puts(UART0, "Hello, World this is an epic long message (based af)!\n");
}


// void recv_pkt() {
//     static uint32_t byte_cnt = 0;
// }

void tick(struct task_t *tasks, uint32_t cnt) {
    struct task_t *tmp_t = tasks;
    for (uint32_t i = 0; i < cnt; ++i) {
        if (tmp_t->delay > 0) {
            tmp_t->delay--;
        } else {
            tmp_t->fn();
            tmp_t->delay = tmp_t->period;
        }
        tmp_t++;
    }
}

struct task_t tasks[] = {
    // {__COUNTER__, recv_paket, 9, 9},
    // {__COUNTER__, update_state, 9, 9},
    {__COUNTER__, show_state, 999, 999}
    // {__COUNTER__, show_state, 999, 999},
};

int main() {
    std_init();
    LED0->val = 0x2;
    uart_set_brd(UART0, (F_CPU/BAUD));
    uart_set_brd(UART1, (F_CPU/BAUD_UART1));

    curr_state.enabled = 1;
    curr_state.seconds = 300;
    curr_state.scoreb = 1;
    curr_state.scoreg = PAKET_SIZE;
    paket_received = false;

    LED0->val = 0x4;

    struct task_t *tasks_start = &tasks[0];
    const uint32_t task_cnt = __COUNTER__;
    tick_flag = 0;


    MTIME0->en = 0;
    MTIME0->cnt = 0;
    MTIME0->cmp = 999;
    MTIME0->psc = 99;

    sei();
    MTIME0->ctrl = 0x3; /* enable interrupt and free run mode */
    MTIME0->en = 0x1; /* enable the timer */


    while (1) {
        recv_paket();
        if (tick_flag) {
            cli();
            tick(tasks_start, task_cnt);
            tick_flag = 0;
            sei();
        }
      __asm__ volatile("nop\n\t");
    }
}
