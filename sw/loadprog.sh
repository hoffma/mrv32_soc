#!/bin/bash

BIN=$1

socat pty,link=/home/user/dev/vmodem0 tcp:raspberrypi.local:2012 &
ssh pi@raspberrypi.local 'python3 reset_mrv32.py'
mrv32_loader ~/dev/vmodem0 ${BIN}
killall socat
