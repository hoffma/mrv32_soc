-- ######################################################################################
-- ###################################  UART TX #########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_tx is
    Port (
        clk : in STD_LOGIC;
        rst : in STD_LOGIC;
        txd : out STD_LOGIC;
        brd : in STD_LOGIC_VECTOR(31 downto 0);
        we  : in STD_LOGIC;
        ack : out STD_LOGIC;
        din : in STD_LOGIC_VECTOR(7 downto 0)
    );
end uart_tx;

architecture Behave of uart_tx is
    signal max_cnt : integer;
    signal tx_counter : unsigned(31 downto 0);
    signal tx_buf : std_logic_vector(7 downto 0);
    signal tx_bit_cnt : unsigned(7 downto 0) := x"00";
    
    signal  tx_state : std_logic_vector(1 downto 0) := "01";
    constant TX_WAIT : std_logic_vector(1 downto 0) := "01";
    constant TX_SEND : std_logic_vector(1 downto 0) := "10";
    constant TX_END  : std_logic_vector(1 downto 0) := "11";
begin
    process(clk)
    begin
        if rising_edge(clk) then
            ack <= '0';
            
            case tx_state is
                when TX_WAIT =>
                    txd <= '1';
                    if we = '1' then
                        txd <= '0'; -- start bit
                        tx_buf <= din;
                        tx_bit_cnt <= x"08";
                        tx_counter <= (others => '0');
                        tx_state <= TX_SEND;
                        max_cnt <= to_integer(unsigned(brd));
                    end if;
                when TX_SEND =>
                    if tx_counter < max_cnt then
                        tx_counter <= tx_counter + 1;
                        tx_state <= TX_SEND;
                    else
                        tx_counter <= (others => '0');
                        if tx_bit_cnt > 0 then
                            txd <= tx_buf(0);
                            tx_bit_cnt <= tx_bit_cnt - 1;
                            tx_buf <= '0' & tx_buf(7 downto 1);
                        else
                            txd <= '1';
                            tx_state <= TX_END;
                        end if;
                    end if;
                when TX_END =>
                    -- stop bit
                    if tx_counter < max_cnt then
                        tx_counter <= tx_counter + 1;
                    else
                        tx_counter <= (others => '0');
                        txd <= '1';
                        ack <= '1';
                        tx_state <= TX_WAIT;
                    end if;
                when others =>
                    tx_state <= TX_WAIT;
                    tx_counter <= (others => '0');
                    ack <= '0';
            end case;
            
            if rst = '1' then
                tx_counter <= (others => '0');
                tx_state <= TX_WAIT;
            end if;
        end if;
    end process;
end Behave;

-- ######################################################################################
-- ###################################  UART RX #########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_rx is
    Port (
        clk : in STD_LOGIC;
        rst : in STD_LOGIC;
        rxd : in STD_LOGIC;
        brd : in STD_LOGIC_VECTOR(31 downto 0);
        rdy : out STD_LOGIC; -- data ready
        dout : out STD_LOGIC_VECTOR(7 downto 0)
    );
end uart_rx;

architecture behave of uart_rx is
    signal max_cnt : integer;
    signal rx_counter : unsigned(31 downto 0) := (others => '0');
    signal rx_buf : std_logic_vector(7 downto 0) := (others => '0');
    signal rx_bit_cnt : unsigned(7 downto 0) := (others => '0');
    
    type rx_state_t is (RX_WAIT, RX_START, RX_RECV, RX_END);
    signal rx_state : rx_state_t;
begin

    process(clk)
    begin
        if rising_Edge(clk) then
            rdy <= '0';
            
            case rx_state is
                when RX_WAIT =>
                    rx_buf <= (others => '0');
                    rx_state <= RX_WAIT;
                    if rxd = '0' then
                        rx_state <= RX_START;
                        rx_counter <= (others => '0');
                        rx_bit_cnt <= x"07";
                        max_cnt <= to_integer(unsigned(brd));
                    end if;
                -- when RX_WAIT =>
                when RX_START =>
                    if rx_counter < (max_cnt/2) then
                        rx_counter <= rx_counter + 1;
                        rx_state <= RX_START;
                    else
                        if rxd = '0' then -- check if still start bit
                            rx_state <= RX_RECV;
                            rx_counter <= (others => '0');
                        else
                            rx_state <= RX_WAIT;
                        end if;
                    end if;
                -- RX_START
                when RX_RECV =>
                    if rx_counter < max_cnt then
                        rx_counter <= rx_counter + 1;
                        rx_state <= RX_RECV;
                    else
                        rx_counter <= (others => '0');
                        if rx_bit_cnt > 0 then
                            rx_buf <= rxd & rx_buf(7 downto 1);
                            rx_bit_cnt <= rx_bit_cnt - 1;
                            rx_state <= RX_RECV;
                        else
                            rx_buf <= rxd & rx_buf(7 downto 1);
                            rx_state <= RX_END;
                        end if;
                    end if;
                -- when RX_RECV =>
                when RX_END =>
                    if rx_counter < max_cnt then
                        rx_counter <= rx_counter + 1;
                        rx_state <= RX_END;
                    else
                        rx_state <= RX_END;
                        if rxd = '1' then
                            rx_counter <= (others => '0');
                            rx_state <= RX_WAIT;
                            dout <= rx_buf;
                            rdy <= '1';
                        end if;
                    end if;
                -- when RX_END =>
                when others =>
                    rx_state <= RX_WAIT;
                    rx_buf <= (others => '0');
                    rx_counter <= (others => '0');
            end case;
            
            if rst = '1' then
                rx_state <= RX_WAIT;
            end if;
        end if;
    end process;
end behave;

-- ######################################################################################
-- ###################################  UART TOP ########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;

entity uart is
  Port (
    clk : in std_logic;
    rst : in std_logic;
    brd : in std_logic_vector(31 downto 0);
    -- tx
    we : in std_logic;
    ack : out std_logic;
    din : in std_logic_vector(7 downto 0);
    -- rx
    rdy : out std_logic;
    dout : out std_logic_vector(7 downto 0);
    -- hardware ports
    txd : out std_logic;
    rxd : in std_logic
  );
end uart;

architecture behavioral of uart is
begin
  inst_tx : entity work.uart_tx
  port map (
    clk => clk,
    rst => rst,
    txd => txd,
    brd => brd,
    we => we,
    ack => ack,
    din => din
  );

  inst_rx : entity work.uart_rx
  port map (
    clk => clk,
    rst => rst,
    rxd => rxd,
    brd => brd,
    rdy => rdy,
    dout => dout
  );
end behavioral;
