library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity top_tb is
end top_tb;

architecture behave of top_tb is
    signal clk, nrst : std_logic := '0';
    signal btns : std_logic_vector(3 downto 0);
    signal led : std_logic_vector(7 downto 0);

    signal txd, rxd : std_logic;
    signal wifi_gpio0 : std_logic;

    signal uart_we : std_logic;
    signal uart_ack : std_logic;
    signal uart_din : std_logic_vector(7 downto 0);
    constant uart_brd : std_logic_vector(31 downto 0) := x"0000_00d9";
begin
    clk <= not clk after 20 ns;
    -- nrst <= '0', '1' after 200 ns;

    btns(0) <= nrst;
    btns(3 downto 1) <= "000";

    uart_din <= x"00";
    uart_we <= '0';

    stim : process
    begin
        nrst <= '0';
        wait for 200 ns;
        nrst <= '1';
        report "simulation started ...";
        wait until led = x"aa";
        wait until rising_edge(clk);
        wait for 10 us;
        report "simulation finished";
        wait for 1 us;
        finish;
    end process;

    inst_top : entity work.top
    port map (
        clk_25mhz => clk,
        btn => btns,
        led => led,
        ftdi_rxd => txd,
        ftdi_txd => rxd,
        wifi_gpio0 => wifi_gpio0
    );

    inst_uart_tx : entity work.uart_tx
    port map (
        clk => clk,
        txd => rxd,
        brd => uart_brd,
        we => uart_we,
        ack => uart_ack,
        din => uart_din
    );
end behave;
