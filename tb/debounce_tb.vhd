library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity debounce_tb is
end debounce_tb;

architecture behave of debounce_tb is
    signal clk, rst : std_logic := '0';
    signal din, dout : std_logic := '0';
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 200 ns;

    stim : process
        procedure delay_cycles(cnt : in natural) is
        begin
            for I in 0 to cnt loop
                wait until rising_edge(clk);
            end loop;
        end procedure delay_cycles;
    begin
        din <= '0';
        wait until rst = '0';

        din <= '1';
        wait for 1200 ns;
        din <= '0';
        delay_cycles(10);

        report "finished simulation";
        finish;
    end process;

    inst_db : entity work.debounce
    generic map (
        CLK_T_NS => 10,
        DEBOUNCE_T_NS => 100
    ) port map (
        clk => clk,
        rst => rst,
        din => din,
        dout => dout
    );
end behave;
