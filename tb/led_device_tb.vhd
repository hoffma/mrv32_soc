library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.finish;

entity led_device_tb is
end led_device_tb;

architecture behave of led_device_tb is
    signal clk : std_logic := '0';
    signal rst : std_logic := '0';

    signal en, ack : std_logic;
    signal we : std_logic_vector(3 downto 0);
    signal addr, din, dout : std_logic_vector(31 downto 0);

    signal led_o : std_logic_vector(7 downto 0);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 45 ns;

    addr <= x"01000000";

    stim: process
        variable tmp : std_logic_vector(31 downto 0);
    begin
        wait until rst = '0';
        report "simulation started";
        en <= '1';
        we <= x"f";
        din <= x"00000001";
        wait until ack = '1';
        en <= '0';
        we <= x"0";
        wait until rising_edge(clk);

        for I in 0 to 9 loop
            en <= '1';
            wait until ack = '1';
            tmp := dout;
            en <= '0';
            wait until rising_edge(clk);
            en <= '1';
            we <= x"f";
            din <= std_logic_vector(unsigned(tmp) + 1);
            wait until ack = '1';
            en <= '0';
            we <= x"0";
            wait until rising_edge(clk);
        end loop;

        report "simulation finished";
        finish;
    end process;

    inst_led_dev : entity work.led_device
    port map (
        clk => clk,
        rst => rst,

        en => en,
        we => we,
        addr => addr,
        din => din,
        dout => dout,
        ack => ack,

        led_o => led_o
    );
end behave;
