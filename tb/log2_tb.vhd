library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

library work;
use work.misc.all;

entity log2_tb is
end log2_tb;

architecture behave of log2_tb is
    signal clk, rst :  std_logic := '0';
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    stim : process
        variable tmp : natural;
    begin
        wait until rst = '0';

        for i in 0 to 10 loop
            tmp := log2_ceil_f(2**i);
            report "i: " & integer'image(i) & ", 2**i: " & integer'image(2**i) severity note;
            assert tmp = i report "Got: " & integer'image(i) & " for i=" & integer'image(i) severity failure;
            tmp := log2_ceil_f(2**i + 1);
            assert tmp = (i+1) report "Got: " & integer'image(tmp) & " for i=" & integer'image(i) severity failure;
        end loop;
        
        report "simulation finished";
        finish;
    end process;
end behave;
