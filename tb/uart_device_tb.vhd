library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.finish;

entity uart_device_tb is
end uart_device_tb;

architecture behave of uart_device_tb is
    constant CLK_HZ : natural := 25_000_000;
    constant BAUDRATE : natural := 115200;
    -- constant BAUDRATE : natural := 1_000_000;
    -- constant BRD : natural := (CLK_HZ/BAUDRATE);
    constant BRD : natural := 217;

    signal clk, rst : std_logic := '0';

    signal en, ack : std_logic;
    signal we : std_logic_vector(3 downto 0);
    signal addr, din, dout : std_logic_vector(31 downto 0);

    signal uart_rxd, uart_txd : std_logic;

    signal fin : std_logic := '0';
    signal uart_dout : std_logic_vector(7 downto 0);
    signal uart_rdy : std_logic;
    
    signal running : std_logic;
begin
    clk <= not clk after 20 ns;
    rst <= '1', '0' after 180 ns;

    uart_rxd <= '1';

    stim : process
        variable tmp_brd : unsigned(31 downto 0);
        variable tmp_out : unsigned(31 downto 0);
    begin
        running <= '0';
        wait until rst = '0';
        report "simulation started";
        tmp_brd := to_unsigned(BRD, tmp_brd'length);
        addr <= x"01100008";
        din <= std_logic_vector(tmp_brd);
        en <= '1';
        we <= x"f";
        wait until ack = '1';
        en <= '0';
        we <= x"0";
        wait until rising_edge(clk);
        en <= '1';
        wait until ack = '1';
        tmp_out := unsigned(dout);
        en <= '0';
        wait until rising_edge(clk);
        assert tmp_out = tmp_brd report "BRD not equal!" severity failure;
        report "BRD fine. Continue writing ...";
        addr <= x"01100000";
        din <= x"00001234";
        we <= x"f";
        en <= '1';
        running <= '1';
        wait until rising_edge(clk);
        we <= x"0";
        wait until ack = '1';
        en <= '0';
        running <= '0';
        wait until rising_edge(clk);

        -- wait until uart_rdy = '1';
        wait for 5 us;
        report "simulation finished";
        finish;
    end process;

    inst_uartdev_tx : entity work.uart_device
    generic map (
        g_CLKS_PER_BIT => 217
    ) port map (
        clk => clk,
        rst => rst,
        en => en,
        we => we,
        addr => addr,
        din => din,
        dout => dout,
        ack => ack,
        txd => uart_txd,
        rxd => uart_rxd
    );

    inst_rx : entity work.UART_RX
    generic map (
        g_CLKS_PER_BIT => 217
        -- g_CLKS_PER_BIT => 1302
    ) port map (
        i_CLK => clk,
        i_RX_Serial => uart_txd,
        o_RX_DV => uart_rdy,
        o_RX_Byte =>uart_dout 
    );
    -- inst_uart_rx : entity work.uart_rx
    -- port map (
    --     clk => clk,
    --     rst => rst,
    --     rxd => uart_txd,
    --     brd => std_logic_vector(to_unsigned(BRD, 32)),
    --     rdy => uart_rdy,
    --     dout => uart_dout
    -- );
end behave;
