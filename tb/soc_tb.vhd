library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity soc_tb is
end soc_tb;

architecture behave of soc_tb is
    signal clk, rst : std_logic := '0';
    signal led_o : std_logic_vector(7 downto 0);

    signal irq : std_logic := '0';
    signal uart_rxd : std_logic;
    signal uart_txd : std_logic;
begin
    rst <= '1', '0' after 50 ns;
    clk <= not clk after 5 ns;

    uart_rxd <= '1';

--    irq_stim : process
--    begin
--        irq <= '1';
--        wait until rising_edge(clk);
--        wait for 200 ns;
--        irq <= '0';
--        wait for 1 us;
--    end process;

    process
    begin
        wait until rst = '0';
        report "simulation started ...";
        wait until led_o = x"10";
        wait for  20 us;
        report "simulation finished";
        finish;
    end process;

    -- inst_soc : entity work.soc(new_behave)
    inst_soc : entity work.soc
    generic map (
        MEM_FILE => "firmware.hex",
        MEM_SIZE_BYTES => 16#8000#
    )
    port map (
        clk => clk,
        rst => rst,
        irq_i => '0',
        led => led_o,
        
        uart_rxd => uart_rxd,
        uart_txd => uart_txd
    );

end behave;

