library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

library work;
use work.devices.all;

entity csg_tb is
end csg_tb;

architecture behave of csg_tb is
    constant BUS_DEVICES : csg_devices := CSG_ENTRIES;

    signal clk, rst : std_logic := '0';
    signal addr_i : std_logic_vector(31 downto 0);
    signal cs_o : std_logic_vector(BUS_DEVICES'length-1 downto 0);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 45 ns;

    process
    begin
        addr_i <= (others => '0');
        wait until rst = '0';
        wait until rising_edge(clk);
        addr_i <= x"8000_0012";
        wait until rising_edge(clk);
        addr_i <= x"8000_0123";
        wait until rising_edge(clk);
        addr_i <= x"8000_0243";
        wait until rising_edge(clk);
        addr_i <= x"8000_1012";
        wait until rising_edge(clk);
        addr_i <= x"8000_9012";
        wait until rising_edge(clk);
        addr_i <= x"8000_0812";
        wait until rising_edge(clk);
        addr_i <= x"8000_0712";
        wait until rising_edge(clk);
        addr_i <= x"0100_0004";
        wait until rising_edge(clk);
        addr_i <= x"0100_0104";
        wait until rising_edge(clk);
        addr_i <= x"0110_0004";
        wait until rising_edge(clk);
        addr_i <= x"0110_0104";

        report "simulation finished";
        finish;
    end process;

    inst_csg : entity work.csg
    generic map (
        BUS_DEVICES => BUS_DEVICES
    ) port map (
        addr => addr_i,
        cs_signal => cs_o
    );
end behave;
