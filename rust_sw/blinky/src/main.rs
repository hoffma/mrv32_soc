#![no_std]
#![no_main]


use core::arch::global_asm;
use core::ptr;
use core::panic::PanicInfo;
use core::hint::spin_loop;

global_asm!(include_str!("entry.s"));


fn write_led(val : u8) {
    const LED: *mut u8 = 0x01000000 as *mut u8;

    unsafe {
        ptr::write_volatile(LED, val);
    }
}

fn delay() {
    for _ in 0..100000 {
        spin_loop();
    }
}

#[no_mangle]
pub extern "C" fn main() -> !{
    let mut i: u8 = 0;
    loop {
        write_led(i);
        delay();
        i += 1;
    }
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
