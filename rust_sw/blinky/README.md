
# requirements 

- jq tool ```sudo apt install jq```
- risc-v target ```rustup target add riscv32i-unknown-none-elf```
- cargo binutils https://github.com/rust-embedded/cargo-binutils
    - ```cargo install cargo-binutils``
    - ```rustup component add llvm-tools-preview```
