use std::env;

fn main() {
    let name = env!("CARGO_PKG_NAME");
    println!("cargo:rustc-link-arg-bin={name}=-Tlink_script.ld");
}
