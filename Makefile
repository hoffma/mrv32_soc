PHONY: all
.DELETE_ON_ERROR:
TOPMOD  := top
TOPFILE := hdl/$(TOPMOD).vhd

PLLFILE := hdl/ecp5pll.vhd

VHD_STD = 08

BUILD_DIR = build

FIRMWARE := firmware.hex

SIMMOD := $(TOPMOD)_tb
TBFILE := tb/$(SIMMOD).vhd
SIMFILE := $(TOPMOD)_sim.ghw

TARGET := ulx3s
BITFILE := $(TARGET).bit
CONFFILE := $(TARGET)_out.config

CPUFILES :=
CPUFILES += mrv32/hdl/core/constants.vhd
CPUFILES += mrv32/hdl/core/types.vhd
CPUFILES += mrv32/hdl/core/reg.vhd
CPUFILES += mrv32/hdl/core/barrel_shifter.vhd
CPUFILES += mrv32/hdl/core/alu.vhd
CPUFILES += mrv32/hdl/core/csr.vhd

CPUFILES += mrv32/hdl/core/control_unit.vhd
CPUFILES += mrv32/hdl/core/idecode.vhd
CPUFILES += mrv32/hdl/core/regfile.vhd
CPUFILES += mrv32/hdl/core/mrv32_core.vhd

CPUFILES += mrv32/hdl/mrv32.vhd

SOCFILES :=
SOCFILES += mem_init.vhd
SOCFILES += hdl/misc.vhd

SOCFILES += hdl/mtime.vhd
SOCFILES += hdl/sram.vhd
SOCFILES += hdl/UART_RX.vhd
SOCFILES += hdl/UART_TX.vhd

SOCFILES += hdl/devices/led_device.vhd
SOCFILES += hdl/devices/uart_device.vhd
SOCFILES += hdl/devices/rom_device.vhd
SOCFILES += hdl/devices/sram_device.vhd

SOCFILES += hdl/soc_types.vhd
SOCFILES += hdl/devices.vhd
SOCFILES += hdl/csg.vhd
SOCFILES += hdl/soc.vhd

# SOCFILES += hdl/top.vhd

VHDFILES := $(CPUFILES) $(SOCFILES)


COREOBJ := $(VHDFILES:mrv32/hdl/core/%.vhd=$(BUILD_DIR)/%.o)
PERIPHOBJ := $(VHDFILES:mrv32/hdl/peripherals/%.vhd=$(BUILD_DIR)/%.o)
CPUOBJ := $(VHDFILES:mrv32/hdl/%.vhd=$(BUILD_DIR)/%.o)
SOCOBJ := $(VHDFILES:hdl/%.vhd=$(BUILD_DIR)/%.o)
TOPOBJ := $(TOPFILE:hdl/%.vhd=$(BUILD_DIR)/%.o)
PLLOBJ := $(PLLFILE:hdl/%.vhd=$(BUILD_DIR)/%.o)
VHDOBJ := $(COREOBJ) $(PERIPHOBJ) $(CPUOBJ) $(SOCOBJ)

all: $(BITFILE)

sim: $(BUILD_DIR) $(SIMFILE)

## 
.PHONY: clean
clean:
	rm -rf $(BINFILE)
	rm -rf $(TOPMOD).json $(CONFFILE) $(BITFILE) *.cf
	rm -rf $(SIMFILE)
	rm -rf build/
	rm -rf *.log
	rm -f *.o
	rm -f $(SIMMOD)
	rm -f *.ghk


$(BUILD_DIR):
	mkdir -p build

$(BUILD_DIR)/%.o: mrv32/hdl/core/%.vhd
	ghdl -a --std=$(VHD_STD) $<

$(BUILD_DIR)/%.o: mrv32/hdl/peripherals/%.vhd
	ghdl -a --std=$(VHD_STD) $<

$(BUILD_DIR)/%.o: mrv32/hdl/%.vhd
	ghdl -a --std=$(VHD_STD) $<

$(BUILD_DIR)/%.o: hdl/%.vhd
	ghdl -a --std=$(VHD_STD) $<

$(BITFILE): $(CONFFILE)
	ecppack -v --compress --freq 62.0 $(CONFFILE) $(BITFILE)

$(CONFFILE): $(TOPMOD).json
	nextpnr-ecp5 -l "pnr.log" -v --45k --json $(TOPMOD).json --package CABGA381 --lpf ulx3s_v20.lpf --textcfg $(CONFFILE) 

$(TOPMOD).json: $(VHDFILES) $(TOPFILE) $(PLLFILE) $(FIRMWARE)
	yosys -m ghdl -l "sys.log" -p 'ghdl --std=$(VHD_STD) -fexplicit -frelaxed $(VHDFILES) $(PLLFILE) $(TOPFILE) -e $(TOPMOD); hierarchy -top $(TOPMOD); synth_ecp5 -json $(TOPMOD).json'

$(SIMFILE): $(VHDOBJ) $(TBFILE) $(FIRMWARE)
	ghdl -a --std=$(VHD_STD) $(TBFILE)
	ghdl -e --std=$(VHD_STD) $(SIMMOD)
	ghdl -r --std=$(VHD_STD) $(SIMMOD) --stop-time=1000us --max-stack-alloc=0 --wave=$(SIMFILE)

prog: $(BITFILE)
	fujprog $^
